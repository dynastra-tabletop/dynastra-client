import { FULL_SYNC_RESPONSE_OBJECT } from 'spec/Chat.mocks';

import { DynastraMatrixDataTransformer } from './Chat.utils';
import { MatrixEventTypes } from './enums/Chat.enums';
import {
  IChatMatrixSyncResponse,
  IChatMatrixSyncRoomsEvent,
  IDynastraCompactChatEvent,
  IDynastraCompactReaction,
} from './interfaces/Chat.interfaces';

describe('Utilities - Chat utilities', () => {
  let transformer: DynastraMatrixDataTransformer;
  beforeEach(() => {
    transformer = new DynastraMatrixDataTransformer();
  })
  it('should correctly convert a full response', () => {
    const convertedResponse: {
      messageEvents: Array<IDynastraCompactChatEvent>,
      reactEvents: Array<IDynastraCompactReaction>
    } = transformer.convertSyncResponseToChatEvents(FULL_SYNC_RESPONSE_OBJECT, []);
    expect(convertedResponse.messageEvents.length).toBeGreaterThan(0);
    expect(convertedResponse.reactEvents.length).toBeGreaterThan(0);
  });
  it ('should skip conversion if no rooms event is present', () => {
    const partialResponse: IChatMatrixSyncResponse = {} as any;
    const convertedResponse: {
      messageEvents: Array<IDynastraCompactChatEvent>,
      reactEvents: Array<IDynastraCompactReaction>
    } = transformer.convertSyncResponseToChatEvents(partialResponse, []);

    expect(convertedResponse.messageEvents.length).toEqual(0);
    expect(convertedResponse.reactEvents.length).toEqual(0);
  });
  it('should find the event', () => {
    const event: IChatMatrixSyncRoomsEvent = {
      type: MatrixEventTypes.RoomMessage
    } as any;
    const events: Array<IChatMatrixSyncRoomsEvent> = [event];
    const found = transformer.getEvent(events, MatrixEventTypes.RoomMessage);
    expect(found).not.toBeUndefined();
  });
  it('should find invite events if they exist', () => {
    const fakeInviteResponse: IChatMatrixSyncResponse = {
      rooms: {
        invite: [{}]
      }
    } as any;
    const hasEvents: boolean = transformer.hasInviteEvents(fakeInviteResponse);
  });
  it('should fail to retrieve an event if the invite events are empty', () => {
    const fakeEmptySyncResponse: IChatMatrixSyncResponse = {} as any;
    const hasEvents: boolean = transformer.hasInviteEvents(fakeEmptySyncResponse);
    expect(hasEvents).toBeFalsy();
    const fakePartiallyEmptySync: IChatMatrixSyncResponse = {
      rooms: {}
    } as any;
    const hasEvents2: boolean = transformer.hasInviteEvents(fakePartiallyEmptySync);
    expect(hasEvents2).toBeFalsy();
  })
  it('should return when an event is found or not', () => {
    const event: IChatMatrixSyncRoomsEvent = {
      type: MatrixEventTypes.RoomMessage
    } as any;
    const events: Array<IChatMatrixSyncRoomsEvent> = [event];
    const hasEvent: boolean = transformer.hasEvent(events, MatrixEventTypes.RoomMessage);
    expect(hasEvent).toEqual(true);
  });
  it('should skip retrieving an event if the eventsArray is empty', () => {
    const event = transformer.getEvent([], MatrixEventTypes.RoomMessage);
    expect(event).toBeUndefined();
  });
});
