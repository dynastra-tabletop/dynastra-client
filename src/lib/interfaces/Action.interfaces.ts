export interface IGameActionResult<TResultType = any>
{
  gameAction: string;
  gameActionResult: TResultType;
}

export interface IGameActionRollResultIndividual {
  appliesToResultCalculation: boolean;
  rollDiceType: string;
  rollValue: number;
}

export interface IGameActionRollResult {
  id: string;
  title: string;
  subTitle: string;
  dateRolled: string;
  requestedRoll: string,
  rollResults: Array<IGameActionRollResultIndividual>;
  humanReadableRollResult: string;
  rollResult: number;
}

export interface IGameActionDisplayResult {
  diceValue: string;
  diceValueClassName: string;
}
