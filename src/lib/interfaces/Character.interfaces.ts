export interface ICharacter {
  Id: string;
  UserId: string;
  CharacterName: string;
  CharacterAbout: string;
  CharacterImgUrl: string;
  IsPublic: boolean;
}
