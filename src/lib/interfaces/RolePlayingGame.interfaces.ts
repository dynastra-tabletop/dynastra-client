import { RolePlayingGameAssetType } from '../enums/role-playing-games.enums';
import { ICharacter } from './Character.interfaces';
import { IUser } from './User.interfaces';

/**
 * The session contained
 *
 * @export
 * @interface IRolePlayingGameSession
 */
export interface IRolePlayingGameSession {
  SessionId: string;
  RolePlayingGameId: string;
  state: number;
  DateStarted: string;
  DateEnded: string;
  ChatRoomIdentifier: string;
  CreatedBy: string;
}

/**
 * The role playing game.
 *
 * @export
 * @interface IRolePlayingGame
 */
export interface IRolePlayingGame {
    CreatedByUser: string;
    Description: string;
    Id?: string;
    ImageUrl: string;
    Name: string;
    Characters?: Array<ICharacter>;
    Users?: Array<IUser>;
    Sessions?: Array<IRolePlayingGameSession>;
}

/**
 * An asset which has been provided in this game.
 *
 * @export
 * @interface IRolePlayingGameAsset
 */
export interface IRolePlayingGameAsset {
  AssetReferenceId: string;
  RolePlayingGameId: string;
  AssetType: RolePlayingGameAssetType;
  DateAdded: string;
  FileName: string;
  CreatedBy: string;
  Title: string;
  Description: string;
}

/**
 * A request to upload a game asset.
 *
 * @export
 * @interface IRolePlayingGameAssetRequest
 */
export interface IRolePlayingGameAssetRequest {
  FileName: string;
  Title: string;
  Description: string;
  AssetType: RolePlayingGameAssetType;
}

/**
 * The settings for the grid.
 */
export interface IGameBoardGridSettings {
  maxZoom: number;
  minZoom: number;
  gridLineStyle: string;
  gridLineColor: string;
  gridLineWidth: number;
}

/**
 * Overall settings for the game board.
 */
export interface IGameBoardSettings {
  gridSettings?: Partial<IGameBoardGridSettings>;
}
