import { MatrixEventTypes } from "../enums/Chat.enums";

/**
 * A temporary holdover for early development.
 *
 * @export
 * @interface IChatLoginRequest
 */
export interface IChatMatrixLoginRequest {
  type: "m.login.password";
  user: string;
  password: string;
}

/**
 * A request to leave a room.
 *
 * @export
 * @interface IChatMatrixLeaveRoomRequest
 */
export interface IChatMatrixLeaveRoomRequest {
  reason: string;
}


/**
 * The login response received from the login server.
 *
 * @export
 * @interface IChatMatrixLoginResponse
 */
export interface IChatMatrixLoginResponse {
  access_token: string;
  device_id: string;
  home_server: string;
  user_id: string;
  well_known: object;
}

/**
 * The content of the event.
 *
 * @export
 * @interface IMatrixRoomEventContent
 */
export interface IMatrixRoomEventContent {
  msgtype: string;
  body: any;
  format: any;
  formatted_body?: any;
  info: any;
}

export interface IMatrixReactionEventContent {
  "m.relates_to": any;
  event_id: string;
  key: string;
  rel_type: "m.annotation"
}

/**
 * A reaction-based event.
 *
 * @export
 * @interface IChatMatrixSyncRoomsReactionEvent
 * @extends {IChatMatrixSyncRoomsEvent<IMatrixReactionEventContent>}
 */
export interface IChatMatrixSyncRoomsReactionEvent extends IChatMatrixSyncRoomsEvent<IMatrixReactionEventContent> {

}

/**
 * An event wrapper which contains an event sent in a matrix room.
 *
 * @export
 * @interface IChatMatrixSyncRoomsEvent
 */
export interface IChatMatrixSyncRoomsEvent<TContentType = IMatrixRoomEventContent> {
  content: TContentType;
  event_id: string;
  origin_server_ts: number;
  sender: string;
  type: MatrixEventTypes;
  unsigned: {
    redacted_by?: string;
    age: number;
  };
}

export interface IChatMatrixGenericEvent {
  event_id: string;
}


/**
 * The timeline container which has the events
 *
 * @export
 * @interface IChatMatrixSyncResponseRoomsTimelineContainer
 */
export interface IChatMatrixSyncResponseRoomsTimelineContainer {
  events: Array<IChatMatrixSyncRoomsEvent>;
  prev_batch?: string;
  limited?: boolean;
}

/**
 * The container for the rooms included in the sync request.
 *
 * @export
 * @interface IChatMatrixSyncResponseRoomsContainer
 */
export interface IChatMatrixSyncResponseRoomsContainer {
  account_data: IChatMatrixSyncResponseRoomsTimelineContainer;
  ephemeral: IChatMatrixSyncResponseRoomsTimelineContainer;
  'org.matrix.msc2654.unread_count': number;
  state: IChatMatrixSyncResponseRoomsTimelineContainer;
  summary: any;
  timeline: IChatMatrixSyncResponseRoomsTimelineContainer;
  unread_notifications: {
    notification_count: number;
    highlight_count: number;
  }
}

/**
 * The response that comes when an invite is provided.
 *
 * @export
 * @interface IChatMatrixSyncResponseInviteContainer
 */
export interface IChatMatrixSyncResponseInviteContainer {
  invite_state: {
    events: Array<IChatMatrixSyncRoomsEvent>;
  }
}

/**
 * The response for individual rooms.
 *
 * @export
 * @interface IChatMatrixSyncResponseRooms
 */
export interface IChatMatrixSyncResponseRooms {
  invite?: { [key: string]: IChatMatrixSyncResponseInviteContainer };
  join?: { [key: string]: IChatMatrixSyncResponseRoomsContainer };
}

/**
 * The response from the matrix server when requesting to sync.
 *
 * @export
 * @interface IChatMatrixSyncResponse
 */
export interface IChatMatrixSyncResponse {
  account_data: {events: Array<any>; }
  device_one_time_keys_count: {signed_curve25519: number};
  device_unused_fallback_key_types:  Array<any>;
  next_batch: string;
  'org.matrix.msc2732.device_unused_fallback_key_types': Array<any>;
  presence: Array<any>;
  rooms: IChatMatrixSyncResponseRooms;
}


/**
 * A request to join a room.
 *
 * @export
 * @interface IChatMatrixJoinRoomRequest
 */
export interface IChatMatrixJoinRoomRequest {
  reason: string;
  // TODO (post-beta): Encryption enabled and signing keys, etc.
  third_party_signed?: any;
}

/**
 * A response returned when the user joins the room.
 *
 * @export
 * @interface IChatMatrixJoinRoomResponse
 */
export interface IChatMatrixJoinRoomResponse {
  room_id: string;
}

/**
 * A chat event which is received by Dynastra.
 *
 * @export
 * @interface IDynastraChatEvent
 */
export interface IDynastraChatEvent {
  roomId: string;
  originalMessage: IChatMatrixSyncRoomsEvent;
  relatedMessages: Array<IDynastraChatEvent>;
}

/**
 * A list of reactions for
 *
 * @export
 * @interface IDynastraCompactReaction
 */
export interface IDynastraCompactReaction {
  roomId: string;
  reactionId: string;
  originalMessageId: string;
  userNames: Array<string>;
  targetEmoji: string;
}

/**
 * A chat event which is compacted down to include as little info as possible.
 *
 * @export
 * @interface IDynastraCompactChatEvent
 * @template TBodyType
 */
export interface IDynastraCompactChatEvent<TBodyType = string> {
  roomId: string;
  messageId: string;
  messageBody: TBodyType;
  sender: string;
  timestamp: number;
  isEmojiOnlyMessage: boolean;
  emojiReactions: Array<IDynastraCompactReaction>;
}
