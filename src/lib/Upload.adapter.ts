import { HttpClient, HttpEvent, HttpEventType, HttpRequest } from '@angular/common/http';
import { FilePickerAdapter, FilePreviewModel, UploadResponse, UploadStatus } from 'ngx-awesome-uploader';
import { catchError, map, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

export class DynastraUploadAdapter extends FilePickerAdapter {
  /**
   * Gets the upload
   *
   * @private
   * @static
   * @return {*}
   * @memberof DynastraUploadAdapter
   */
  private get UploadPath() {
    return `${environment.asset_upload}`;
  }


  uploadFile(fileItem: FilePreviewModel): Observable<UploadResponse> {
    const form = new FormData();
    form.append('file', fileItem.file);
    const req = new HttpRequest('POST', this.UploadPath, form, { reportProgress: true });
    return this.HttpClient.request(req).pipe(
      map((res: HttpEvent<any>) => {
        if (res.type === HttpEventType.Response) {
          const responseFromBackend = res.body;
          return {
            body: responseFromBackend,
            status: UploadStatus.UPLOADED
          };
        } else if (res.type === HttpEventType.UploadProgress) {
          /** Compute and show the % done: */
          const uploadProgress = +Math.round((100 * res.loaded) / res.total!);
          return {
            status: UploadStatus.IN_PROGRESS,
            progress: uploadProgress
          };
        } else {
          return {
            status: UploadStatus.IN_PROGRESS,
            progress: 0,
          }
        }
      }),
      catchError(er => {
        console.log(er);
        return of({ status: UploadStatus.ERROR, body: er });
      })
    );
  }


  removeFile(fileItem: FilePreviewModel): Observable<any> {
    throw new Error('Method not implemented.');
  }

  /**
   * Creates an instance of DynastraUploadAdapter.
   * @param {HttpClient} HttpClient
   * @memberof DynastraUploadAdapter
   */
  constructor(protected readonly HttpClient: HttpClient) {
    super();
  }
}
