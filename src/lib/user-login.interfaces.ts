/**
 * A user which is logged into the interface.
 *
 * @export
 * @interface ILoggedInUser
 */
export interface ILoggedInUser {
  /**
   * The user's ID.
   *
   * @type {string}
   * @member ILoggedInUser
   */
    userID: string;

    /**
     * The user's username.
     *
     * @type {string}
     * @member ILoggedInUser
     */
    username: string;

    /**
     * The user's e-mail address (used for avatar generation).
     *
     * @type {string}
     * @member ILoggedInUser
     */
    email: string;

    /**
     * The URL for the user's avatar.
     *
     * @type {string}
     * @member ILoggedInUser
     */
    avatarURL: string;
}
