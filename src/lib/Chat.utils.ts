import emojiRegex from 'emoji-regex';
import { groupBy } from 'lodash';

import { MatrixEventTypes } from './enums/Chat.enums';
import {
  IChatMatrixSyncResponse,
  IChatMatrixSyncResponseRoomsContainer,
  IChatMatrixSyncRoomsEvent,
  IDynastraCompactChatEvent,
  IDynastraCompactReaction,
} from './interfaces/Chat.interfaces';

const _ = { groupBy }
/**
 * A handler which takes Matrix data and parses it into
 * dynastra events.
 *
 * @export
 * @class DynastraMatrixRoomHandler
 */
export class DynastraMatrixDataTransformer {
  /**
   * Store the emoji regex as a singleton to maximise reuse.
   *
   * @private
   * @type {*}
   * @memberof DynastraMatrixDataTransformer
   */
  private RegexInst: any = emojiRegex();

  /**
   * Does the room have chat data?
   *
   * @private
   * @param {IChatMatrixSyncResponseRoomsContainer} roomContainer
   * @memberof DynastraMatrixDataTransformer
   */
  private roomHasChatData(events: Array<IChatMatrixSyncRoomsEvent>) {
    return events.find((item: IChatMatrixSyncRoomsEvent) => item.type === MatrixEventTypes.RoomMessage || item.type === MatrixEventTypes.Reaction) !== undefined;
  }

  /**
   * Checks if the message is emoji only.
   *
   * @private
   * @param {string} body
   * @return {*}  {boolean}
   * @memberof DynastraMatrixDataTransformer
   */
  private checkIsEmojiOnly(body: string): boolean {
    if (body.length === 0) {
      return false;
    }

    const matches = body.match(this.RegexInst);
    return matches !== null && (matches!.length * 2) === body.length;
  }

  /**
   * Converts the state info from the room into actual usable objects.
   *
   * @private
   * @param {*} roomId
   * @param {*} roomContainer
   * @memberof DynastraMatrixDataTransformer
   */
  private convertRoomContainerToDynastraEvents(
    roomId: string,
    roomContainer: IChatMatrixSyncResponseRoomsContainer,
    chatHistory: Array<IDynastraCompactChatEvent>,
  ): { messageEvents: Array<IDynastraCompactChatEvent>, reactEvents: Array<IDynastraCompactReaction> } {
    if (!this.roomHasChatData(roomContainer.timeline.events)) {
      return {
        messageEvents: [],
        reactEvents: [],
      };
    }

    const useEvents = roomContainer.timeline.events.length > 0 ?
      roomContainer.timeline.events :
      roomContainer.state.events;

    const messageEvents: Array<IDynastraCompactChatEvent> = useEvents
      .filter((event: IChatMatrixSyncRoomsEvent) => event.type === MatrixEventTypes.RoomMessage)
      .map((event: IChatMatrixSyncRoomsEvent) => {
        return {
          roomId: roomId,
          messageId: event.event_id,
          messageBody: event.content!.body!,
          sender: event.sender,
          timestamp: event.origin_server_ts,
          isEmojiOnlyMessage: this.checkIsEmojiOnly(event.content!.body!),
          emojiReactions: [],
        }
      });


      const filteredEmoji = useEvents
        .filter((event: IChatMatrixSyncRoomsEvent) => event.type === MatrixEventTypes.Reaction && event.unsigned.redacted_by === undefined);

      const eventsGrouped = _.groupBy(
        filteredEmoji,
        (item: IChatMatrixSyncRoomsEvent) => item!.content!['m.relates_to']!.event_id!
      );

      const reactEvents = Object.keys(eventsGrouped)
        .reduce((iter: any, eventId: string) => {
          const items = eventsGrouped[eventId];
          const grouped = _.groupBy(items, (item: IChatMatrixSyncRoomsEvent) => item!.content!['m.relates_to']!.key!)
          const groupedAsCompact = Object.keys(grouped)
            .reduce((subIter: any, emoji: string) => {
              const emojiReacts = grouped[emoji];
              const convertedEmojiReact = emojiReacts.reduce((subSubIter: any, reactItem) => {
                subSubIter.reactionIds.push(reactItem.event_id);
                subSubIter.userNames.push(reactItem.sender);
                return subSubIter;
              }, {
                roomId,
                reactionIds: [],
                originalMessageId: eventId,
                userNames: [],
                targetEmoji: emoji
              });
              subIter.push(convertedEmojiReact);
              return subIter;
            }, []);
          iter = iter.concat(groupedAsCompact);
          return iter;
        }, []);

    // Now get the rest of the events and convert them to Dynastra events.
    return {
      messageEvents,
      reactEvents,
    };
  }

  /**
   * Checks to see if the sync response has invite events.
   * (e.g. rooms.invite?.x?.etc.)
   *
   * @param {IChatMatrixSyncResponse} syncResponse
   * @return {*}
   * @memberof DynastraMatrixDataTransformer
   */
  public hasInviteEvents(syncResponse: IChatMatrixSyncResponse) {
    if (!syncResponse.rooms?.invite) {
      return false;
    }
    return Object.keys(syncResponse.rooms.invite).length > 0;
  }

  /**
   * Gets an event from a list of events in a room.
   *
   * @param {Array<IChatMatrixSyncRoomsEvent>} eventsArray
   * @param {MatrixEventTypes} eventType
   * @return {*}
   * @memberof DynastraMatrixDataTransformer
   */
  public getEvent(eventsArray: Array<IChatMatrixSyncRoomsEvent>, eventType: MatrixEventTypes) {
    if (eventsArray.length === 0) {
      return undefined;
    }

    return eventsArray.find((event: IChatMatrixSyncRoomsEvent) => event.type === eventType);
  }

  /**
   * Does the events for
   *
   * @param {Array<IChatMatrixSyncRoomsEvent>} eventsArray
   * @return {*}
   * @memberof DynastraMatrixDataTransformer
   */
  public hasEvent(eventsArray: Array<IChatMatrixSyncRoomsEvent>, eventType: MatrixEventTypes) {
    return this.getEvent(eventsArray, eventType) !== undefined;
  }


  /**
   * Takes that big boi sync response and converts it to
   * sexy objects.
   *
   * @param {IChatMatrixSyncResponse} syncResponse
   * @memberof DynastraMatrixDataTransformer
   */
  public convertSyncResponseToChatEvents(
    syncResponse: IChatMatrixSyncResponse,
    roomHistory: Array<IDynastraCompactChatEvent>
  ): { messageEvents: Array<IDynastraCompactChatEvent>, reactEvents: Array<IDynastraCompactReaction> } {
    if (!syncResponse.rooms) {
      return {
        messageEvents: [],
        reactEvents: []
      };
    }

    const convertedObjects = Object
      .keys((syncResponse.rooms as { [key: string]: IChatMatrixSyncResponseRoomsContainer; })['join'])
      .reduce((iter: any, roomId: any) => {
        const roomContainer: IChatMatrixSyncResponseRoomsContainer = syncResponse.rooms.join![roomId];
        const convertedEvents: { messageEvents: Array<IDynastraCompactChatEvent>, reactEvents: Array<IDynastraCompactReaction> } = this.convertRoomContainerToDynastraEvents(
          roomId,
          roomContainer,
          roomHistory,
        );
        iter.messageEvents = iter.messageEvents.concat(convertedEvents.messageEvents);
        iter.reactEvents = iter.reactEvents.concat(convertedEvents.reactEvents);
        return iter;
      }, {
        messageEvents: [],
        reactEvents: []
      });
    return convertedObjects;
  }
}
