export enum MatrixEventTypes {
  RoomMessage = "m.room.message",
  RoomPowerLevels = "m.room.power_levels",
  RoomMember = "m.room.member",
  RoomHistoryVisibility = "m.room.member",
  RoomCreate = "m.room.create",
  RoomName = "m.room.name",
  RoomCanonicalAlias = "m.room.canonical_alias",
  RoomTopic = "m.room.topic",
  RoomJoinRules = "m.room.join_rules",
  Emote = "m.emote",
  RoomEncryptedContent = "m.room.encrypted",
  Reaction = "m.reaction"
}

/**
 * The state for the role-playing session.
 *
 * @export
 * @enum {number}
 */
export enum RolePlayingGameSessionState {
  Open = 0,
  Closed = 1,
  Expired = 2
};
