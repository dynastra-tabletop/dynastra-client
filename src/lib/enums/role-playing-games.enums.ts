/**
 * The type of display selected by the user.
 *
 * @export
 * @enum {number}
 */
export enum RolePlayingGameDisplayType {
  Grid,
}

/**
 * The types of game assets provided.
 *
 * @export
 * @enum {number}
 */
export enum RolePlayingGameAssetType {
  Token,
  GameObject
}
