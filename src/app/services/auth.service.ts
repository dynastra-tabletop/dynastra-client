import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { filter, Subject, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ILoggedInUser } from 'src/lib/user-login.interfaces';

/**
 * The interface defining the authentication service.
 *
 * @export
 * @interface IAuthService
 */
export interface IAuthService {
  isAwaitingAuthentication: boolean;
  failedToContactAuthentication: boolean;
  profile?: any;
  loggedInUser: Partial<ILoggedInUser>;
  loggedInUserContext: any;
  initAuth(initialURL?: string): Promise<void>;
  logout(): void;
  getIssuerAccountRedirect(): string;
  isLoggedIn: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService implements IAuthService {
  /**
   * Are we still waiting for the authentication service?
   *
   * @type {boolean}
   * @memberof AuthService
   */
  public isAwaitingAuthentication: boolean = true;

  /**
   * Did we try to connect to authentication but fail?
   *
   * @type {boolean}
   * @memberof AuthService
   */
  public failedToContactAuthentication: boolean = false;

  /**
   * When the authentication endpoint fails to respond, the auth service gives up.
   *
   * @protected
   * @type {Subject<boolean>}
   * @memberof AuthService
   */
  protected onAuthContactFailure$: Subject<boolean> = new Subject<boolean>();

  /**
   * The subscription for the authentication event.
   *
   * @protected
   * @type {Subscription}
   * @memberof AuthService
   */
  protected autheEventSubscription!: Subscription;

  /**
   * The profile for the user.
   *
   * @type {*}
   * @memberof AuthService
   */
  public profile!: any;

  /**
   * The environment as loaded by angular.
   *
   * @readonly
   * @protected
   * @memberof AuthService
   */
  protected get environment() {
    return environment as any;
  }

  /**
   * Gets the identity claims. Only called when logged in.
   *
   * @readonly
   * @protected
   * @memberof AuthService
   */
  protected get identityClaims() {
    return this.OAuthService.getIdentityClaims();
  }

  /**
   * Error events which when we receive them, we redirect to the error page.
   *
   * @protected
   * @type {Array<string>}
   * @memberof AuthService
   */
  protected redirectableErrorEvents: Array<string> = [
    "session_error",
    "invalid_nonce_in_state"
  ];

  /**
   * Is the user ID provided me?
   * @param userId
   */
  public isMe(userId: string) {
    return userId === this.loggedInUser.userID;
  }

  /**
   * The current logged in user.
   *
   * @readonly
   * @type {Partial<ILoggedInUser>}
   * @memberof AuthService
   */
  public get loggedInUser(): Partial<ILoggedInUser> {
    return {
      userID: (this.identityClaims as any)?.sub || "",
      username: (this.identityClaims as any)?.name || "",
      email: (this.identityClaims as any)?.preferred_username || "",
    };
  }

  /**
   * The redirect URL for the OIDC issuer passed as "iss" in the claims.
   *
   * @readonly
   * @type {string}
   * @memberof AuthService
   */
  public get issuerRedirect(): string {
    return (this.identityClaims as any)?.iss;
  }

  /**
   * Gets the redirect URL for the account console.
   *
   * @readonly
   * @type {string}
   * @memberof AuthService
   */
  public getIssuerAccountRedirect(): string {
    if(this.issuerRedirect === undefined) {
      return "#";
    }

    return `${this.issuerRedirect}/account`;
  }

  /**
   * Is the user logged in?
   *
   * @return {*}  {boolean}
   * @memberof AuthService
   */
  public get isLoggedIn(): boolean {
    return  this.OAuthService.hasValidAccessToken();
  }

  /**
   * Gets the logged in user's context.
   *
   * @readonly
   * @type {ILoggedInUser}
   * @memberof AuthService
   */
  public get loggedInUserContext(): any {
    return this.OAuthService.getIdentityClaims();
  }

  /**
   * Checks for the login token and loads into oauth service.
   *
   * @memberof AuthService
   */
  public checkForAuth(): void {
    this.OAuthService.tryLogin({});
  }

  /**
   * Initialises the auth process.
   *
   * @return {*}  {Observable<boolean>}
   * @memberof AuthService
   */
  public async initAuth(initialURL?: string): Promise<void>{
    return await this.doAuthFlow(initialURL);
  }

  /**
   * Logs the user out.
   *
   * @memberof AuthService
   */
  public logout(): void {
    this.OAuthService.logOut();
    this.router.navigate(["/logged-out"]);
  }

  /**
   * Does the logic for the oAuth flow.
   *
   * @protected
   * @return {*}  {Promise<void>}
   * @memberof AuthService
   */
  protected async doAuthFlow(initialURL?: string): Promise<void> {
    // Already authed?
    this.checkForAuth();

    if (this.isLoggedIn) {
      return;
    }

    try {
      await this.OAuthService.loadDiscoveryDocument(
        this.environment.oidc_discovery,
      );
      this.OAuthService.setupAutomaticSilentRefresh();
      this.profile = await this.OAuthService.loadUserProfile();
    } catch(err) {
      const errorInst = err as any;
      if (errorInst.name && errorInst.name === "HttpErrorResponse") {
        this.failedToContactAuthentication = true;
      }
    }

    this.OAuthService.initCodeFlow(initialURL);
    return;
  }

  /**
   * Initialises the authentication.
   *
   * @memberof AuthService
   */
  protected initAuthentication() {
    this.OAuthService.setStorage(sessionStorage);
    this.OAuthService.configure({
      issuer: this.environment.oidc_issuer,
      redirectUri: `${this.environment.oidc_baseURL}/post-login`,
      clientId: this.environment.oidc_clientID,
      scope: "openid profile email",
    });
  }

  /**
   * Reacts to when an authentication error was received.
   *
   * @protected
   * @memberof AuthService
   */
  protected handleAuthErrorEvent() {
    this.router.navigate(["/login-error"]);
  }


  /**
   * Handles changes in the authentication when there are errors or events.
   *
   * @protected
   * @memberof AuthService
   */
  protected handleAuthChanges(): void {
    this.OAuthService.events!
      .pipe(
        filter(p => this.redirectableErrorEvents.includes(p.type)),
      ).subscribe(() => this.handleAuthErrorEvent());
  }

  /**
   * Creates an instance of AuthService.
   * @param {OAuthService} OAuthService
   * @memberof AuthService
   */
  constructor(
    private readonly OAuthService: OAuthService,
    private readonly router: Router,
  ) {
    this.initAuthentication();
    this.handleAuthChanges();
  }
}
