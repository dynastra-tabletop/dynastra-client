import {inject, TestBed} from '@angular/core/testing';

import { RolePlayingGameService } from './role-playing-game.service';
import {MockModule} from "ng-mocks";
import {OAuthModule} from "angular-oauth2-oidc";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthService} from "./auth.service";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {IRolePlayingGame} from "../../lib/interfaces/RolePlayingGame.interfaces";
import {CharactersService} from "./characters.service";
import {mock} from "sinon";

describe('RolePlayingGameService', () => {
  let service: RolePlayingGameService;
  let mockGame: IRolePlayingGame = {
    Characters: [],
    CreatedByUser: "",
    Description: "",
    Id: "",
    ImageUrl: "",
    Name: "",
    Users: []
  }

  beforeEach(() => {
    TestBed.overrideProvider(AuthService, {
      useValue: {
        loggedInUser: {
          userID: "",
        },
        loggedInUserContext: {
          id: "",
        }
      }
    })
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MockModule(OAuthModule.forRoot()),
        RouterTestingModule
      ]
    });
    service = TestBed.inject(RolePlayingGameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should get a role playing game', inject(
    [HttpTestingController, RolePlayingGameService],
    (httpMock: HttpTestingController, gameService: RolePlayingGameService) => {

      gameService.GetRolePlayingGame('').subscribe((game :IRolePlayingGame) => {
        expect(game).toEqual(mockGame);
      });
      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush(mockGame);
    }
  ));
  it('should add the current user to the game', inject(
    [HttpTestingController, RolePlayingGameService],
    (httpMock: HttpTestingController, gameService: RolePlayingGameService) => {

      gameService.AddCurrentUserToRolePlayingGame('').subscribe((addUser: any) => {
        expect(addUser).toEqual({});
      });
      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush({});
    }
  ));
  it('should add a character to the game', inject(
    [HttpTestingController, RolePlayingGameService],
    (httpMock: HttpTestingController, gameService: RolePlayingGameService) => {

      gameService.AddCharacterToRolePlayingGame('', '').subscribe((addUser: any) => {
        expect(addUser).toEqual({});
      });
      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush({});
    }
  ));
  it('should add characters to the game', inject(
    [HttpTestingController, RolePlayingGameService],
    (httpMock: HttpTestingController, gameService: RolePlayingGameService) => {

      gameService.AddCharactersToRolePlayingGame('', ['']).subscribe((addUser: any) => {
        expect(addUser.length).toEqual(1);
      });
      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush({});
    }
  ));
  it('should remove characters to the game', inject(
    [HttpTestingController, RolePlayingGameService],
    (httpMock: HttpTestingController, gameService: RolePlayingGameService) => {

      gameService.RemoveCharactersFromRolePlayingGame('', ['']).subscribe((addUser: any) => {
        expect(addUser.length).toEqual(1);
      });
      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush({});
    }
  ));
  it('should remove a character to the game', inject(
    [HttpTestingController, RolePlayingGameService],
    (httpMock: HttpTestingController, gameService: RolePlayingGameService) => {

      gameService.RemoveCharacterFromRolePlayingGame('', '').subscribe((remUser: any) => {
        expect(remUser).toEqual([{}]);
      });
      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush([{}]);
    }
  ));
  it('should list characters in the game', inject(
    [HttpTestingController, RolePlayingGameService],
    (httpMock: HttpTestingController, gameService: RolePlayingGameService) => {

      gameService.ListCharactersInRolePlayingGame('').subscribe((chars: any) => {
        expect(chars).toEqual([mockGame]);
      });
      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush([mockGame]);
    }
  ));
  it('should update a role-playing game', inject(
    [HttpTestingController, RolePlayingGameService],
    (httpMock: HttpTestingController, gameService: RolePlayingGameService) => {

      gameService.UpdateRolePlayingGame('', mockGame).subscribe((chars: any) => {
        expect(chars).toEqual({});
      });
      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush({});
    }
  ));
  it('should create a role-playing game', inject(
    [HttpTestingController, RolePlayingGameService],
    (httpMock: HttpTestingController, gameService: RolePlayingGameService) => {

      gameService.CreateRolePlayingGame(mockGame).subscribe((chars: any) => {
        expect(chars).toEqual(true);
      });
      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush(true);
    }
  ));
  it('should create a role-playing game', inject(
    [HttpTestingController, RolePlayingGameService],
    (httpMock: HttpTestingController, gameService: RolePlayingGameService) => {

      gameService.DeleteRolePlayingGame('').subscribe((res: any) => {
        expect(res).toEqual(true);
      });
      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush(true);
    }
  ));
  it('should get role-playing games', inject(
    [HttpTestingController, RolePlayingGameService],
    (httpMock: HttpTestingController, gameService: RolePlayingGameService) => {

      gameService.GetRolePlayingGames().subscribe((chars: Array<IRolePlayingGame>) => {
        expect(chars).toEqual([mockGame]);
      });
      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush([mockGame]);
    }
  ));
});
