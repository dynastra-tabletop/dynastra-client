import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { OAuthEvent, OAuthService } from 'angular-oauth2-oidc';
import { Observable, of } from 'rxjs';
import sinon from "sinon";
import { AuthService } from './auth.service';

describe('Services - Authentication Service', () => {
	let service: AuthService;
	const mockOauthService: OAuthService = sinon.mock(OAuthService) as unknown as OAuthService;
	const mockRouter: Router = sinon.mock(Router) as unknown as Router;
	const mockIdentityClaims: any = {
		"family_name": "Dickhead",
		"given_name": "Jimmy",
		"locale": "en-US",
		"name": "Jimmy Dickhead",
		"preferred_username": "jimbob.dicketh.head@lolz.coomer",
		"sub": "00u9vme99nxudvxZA0h7",
		"updated_at": 1490198843,
		"zoneinfo": "America/Los_Angeles"
	};
	const setStorageSpy = sinon.spy();
	const configureSpy = sinon.spy();
	const loadDiscoverySpy = sinon.spy();
	const silentRefreshSpy = sinon.spy();
	const profileSpy = sinon.spy();
	const accessTokenSpy = sinon.spy();
	const tryLoginSpy = sinon.spy();
	const codeFlowSpy = sinon.spy();
	const logOutSpy = sinon.spy();
	const routerEasierSpy = sinon.spy(); // We're not using testbed

	beforeEach(() => {
		mockOauthService.setStorage = setStorageSpy;
		mockOauthService.configure = configureSpy;
		mockOauthService.loadDiscoveryDocument = loadDiscoverySpy;
		mockOauthService.setupAutomaticSilentRefresh = silentRefreshSpy;
		mockOauthService.loadUserProfile = profileSpy;
		mockOauthService.hasValidAccessToken = () => {
			accessTokenSpy();
			return false;
		};
		mockOauthService.hasValidIdToken = () => true;
		mockOauthService.tryLogin = tryLoginSpy;
		mockOauthService.initCodeFlow = codeFlowSpy;
		mockOauthService.logOut = logOutSpy;
		mockOauthService.getIdentityClaims = () => mockIdentityClaims;
		mockOauthService.events = new Observable();
		mockRouter.navigate = routerEasierSpy;
		service = new AuthService(mockOauthService, mockRouter);
	});
	afterEach(() => {
		setStorageSpy.resetHistory();
		configureSpy.resetHistory();
		loadDiscoverySpy.resetHistory();
		silentRefreshSpy.resetHistory();
		profileSpy.resetHistory();
		accessTokenSpy.resetHistory();
		tryLoginSpy.resetHistory();
	});

	it('should be created', () => {
		expect(service).not.toBeUndefined();
	});

	it('should correctly set storage, and then configure auth on construct', () => {
		expect(() => new AuthService(mockOauthService, mockRouter)).not.toThrowError();
		expect(setStorageSpy.called).toBeTrue();
		expect(configureSpy.called).toBeTrue();
	});

	it('should correctly initialise auth if requested', async () => {
		await service.initAuth();
		expect(accessTokenSpy.called).toBeTrue();
		expect(loadDiscoverySpy.called).toBeTrue();
		expect(silentRefreshSpy.called).toBeTrue();
		expect(profileSpy.called).toBeTrue();
		expect(service.loggedInUser).not.toBeNull();
		expect(service.loggedInUserContext).not.toBeNull();
	});

	it('should correctly handle a HTTP error response if received from the server', async () => {
		mockOauthService.loadDiscoveryDocument = () => {
			loadDiscoverySpy();
			throw new HttpErrorResponse({});
		};
		await expect(async () => await service.initAuth()).not.toThrowError();
	});

	it('should log out correctly', () => {
		service.logout();
		expect(logOutSpy.called).toBeTrue();
	});

	it('should correctly skip auth flow if the user is logged in', async () => {
		mockOauthService.hasValidAccessToken = () =>  true;
		const auth = new AuthService(mockOauthService, mockRouter);
		await auth.initAuth();
		expect(loadDiscoverySpy.called).toBeFalse();
	});

	it('should correctly handle an error event from the authentication library', () => {
		mockOauthService.events = of({ type: "session_error" }, { type: "invalid_nonce_in_state"} ,{ type: "johnny_dipshit_in_the_corner" }) as Observable<OAuthEvent>;
		new AuthService(mockOauthService, mockRouter);
		expect(routerEasierSpy.called).toBeTrue();
	});
});
