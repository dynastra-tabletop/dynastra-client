import { TestBed } from '@angular/core/testing';
import { ActionService } from './action.service';
import {MockModule, MockProvider} from "ng-mocks";
import {OAuthModule} from "angular-oauth2-oidc";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthService} from "./auth.service";

describe('ActionService', () => {
  let service: ActionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MockModule(OAuthModule.forRoot()),
        RouterTestingModule
      ],
      providers: [
        MockProvider(AuthService)
      ]
    });
    service = TestBed.inject(ActionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
