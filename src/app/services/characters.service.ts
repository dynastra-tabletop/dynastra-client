import { Injectable } from '@angular/core';
import { AuthService} from "./auth.service";
import {HttpClient} from "@angular/common/http";
import {ICharacter} from "../../lib/interfaces/Character.interfaces";
import {IRolePlayingGame} from "../../lib/interfaces/RolePlayingGame.interfaces";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CharactersService {
  /**
   * Endpoint path for listing characters.
   * @protected
   */
  protected static ListCharactersPath: string = 'characters/list';

  /**
   *
   * @protected
   */
  protected static CreateCharactersPath: string = 'characters/create';

  /**
   * The path for getting a character.
   * @protected
   */
  protected static GetCharacterPath: string = 'character'

  public CreateCharacter(character: ICharacter): Observable<any> {
    return this.HttpClient
      .post<ICharacter>(`https://localhost:5001/${CharactersService.CreateCharactersPath}`, character);
  }

  /**
   * Gets an individual character.
   * @param characterId
   * @constructor
   */
  public GetCharacter(characterId: string) : Observable<ICharacter> {
    return this.HttpClient.get<ICharacter>(`https://localhost:5001/${CharactersService.GetCharacterPath}/${characterId}`)
  }

  /**
   * Gets all the characters a user has.
   * @param userId
   * @constructor
   */
  public GetCharactersForUser(): Observable<Array<ICharacter>> {
    return this.HttpClient
      .get<Array<ICharacter>>(`https://localhost:5001/${CharactersService.ListCharactersPath}`);
  }

  /**
   * Deletes a character
   * @constructor
   */
  public DeleteCharacter(characterId: string): Observable<object> {
    return this.HttpClient
      .delete(`https://localhost:5001/${CharactersService.GetCharacterPath}/${characterId}`);
  }

  /**
   * Updates the character.
   * @param characterId
   * @param character
   * @constructor
   */
  public UpdateCharacter(characterId: string, character: ICharacter): Observable<object> {
    return this.HttpClient
      .put(`https://localhost:5001/${CharactersService.GetCharacterPath}/${characterId}`, character);
  }

  /**
   * Creates a new instance of the CharactersService
   * @param HttpClient
   */
  constructor(
    private HttpClient: HttpClient,
    private AuthService: AuthService
  ) { }
}
