import { Injectable } from '@angular/core';
import * as signalR from '@microsoft/signalr';
import {environment} from "../../environments/environment";
import {filter, from, map, Observable, Subject} from "rxjs";
import {IGameActionResult} from "../../lib/interfaces/Action.interfaces";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root',
})
export class ActionService {

  /**
   * The connection to the SignalR backend.
   * @protected
   */
  protected Connection: any;

  /**
   * Subject for routing actions.
   * @protected
   */
  protected OnActionSubject: Subject<IGameActionResult> = new Subject<IGameActionResult>();

  /**
   * For when the connection is awaiting.
   * @protected
   */
  protected OnConnected: Subject<boolean> = new Subject<boolean>();

  /**
   * Is the socket instance connected?
   */
  public IsConnected: boolean | null = null;


  /**
   * Listens to the action
   * @constructor
   */
  public ListenToActions(): Observable<IGameActionResult> {
    return this.OnActionSubject;
  }

  /**
   * Listens to a specific action.
   * @param actionType
   * @constructor
   */
  public ListenToAction(actionType: string) {
    return this.OnActionSubject.pipe(
        filter((action: IGameActionResult) => action.gameAction === actionType)
    );
  }

  /**
   * Initialises the action service.
   * @protected
   */
  protected init() {
    this.Connection = new signalR.HubConnectionBuilder()
      .withUrl(environment.socket_url)
      .configureLogging(signalR.LogLevel.Debug)
      .build();

    // Start on startup.
    this.startConnection();

    this.Connection.onclose(() => {
      // Re-start on disconnection.
      this.startConnection();
    });

    const self = this;
    this.Connection.on('GameActionResult', function(response: IGameActionResult) {
      self.OnActionSubject.next(response);
    });
  }

  /**
   * Joins a game session.
   * @param GameSessionId
   */
  public joinGameSession(GameSessionId: string) {
    if(!this.IsConnected || this.IsConnected === null) {
      return this.OnConnected.pipe(
        map(() => {
          return from(this.Connection.invoke('JoinGameSession', GameSessionId));
        })
      );
    }
    return from(this.Connection.invoke('JoinGameSession', GameSessionId));
  }

  /**
   * Requests a dice roll.
   * @param RollNotation
   */
  public requestDiceRoll(GameSessionId: string, title: string, subTitle: string, RollNotation: string) {
    const UserId = this.AuthService.loggedInUser.userID;
    return from(this.Connection.invoke('RollDiceAction', GameSessionId, UserId, title, subTitle, RollNotation ));
  }

  /**
   * Starts the connection anew.
   * @protected
   */
  protected startConnection() {
    this.Connection.start()
      .then(() => {
        this.IsConnected = true;
        this.OnConnected.next(true);
      })
      .catch((err) => {
        this.IsConnected = false;
        this.OnConnected.next(false);
      });
  }

  /**
   * Creates a new instance of the action service.
   */
  constructor(
    protected readonly AuthService: AuthService,
  ) {
    this.init();

  }
}
