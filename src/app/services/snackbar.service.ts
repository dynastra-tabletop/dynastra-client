import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DynastraSnackBarComponent } from '../components/dynastra-snack-bar/dynastra-snack-bar.component';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {
  /**
   * Sends a message via the snackbar.
   *
   * @param {string} header
   * @param {string} message
   * @memberof SnackbarService
   */
  public Message(icon: string, header: string, message: string) {
    this.SnackBar.openFromComponent(DynastraSnackBarComponent, {
      announcementMessage: header,
      verticalPosition: 'bottom',
      horizontalPosition: 'end',
      data: {
        icon,
        header,
        message,
      }
    });
  }

  /**
   * Creates an instance of SnackbarService.
   * @param {MatSnackBar} SnackBar
   * @memberof SnackbarService
   */
  constructor(protected SnackBar: MatSnackBar) { }
}
