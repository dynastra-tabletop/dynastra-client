import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { RolePlayingGameAssetType } from 'src/lib/enums/role-playing-games.enums';

import { IGameActionRollResult } from '../../lib/interfaces/Action.interfaces';
import { ICharacter } from '../../lib/interfaces/Character.interfaces';
import {
  IRolePlayingGame,
  IRolePlayingGameAsset,
  IRolePlayingGameAssetRequest,
  IRolePlayingGameSession,
} from '../../lib/interfaces/RolePlayingGame.interfaces';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class RolePlayingGameService {

  /**
   * Temporary URL for alpha. After alpha, these are auto-clients.
   * @protected
   */
  protected static ListRolePlayingGamesPath: string = 'role-playing-games/list';

  /**
   * Temporary URL for alpha. After alpha, these are auto-clients.
   * @protected
   */
  protected static CreateRolePlayingGamesPath: string = 'role-playing-games/create';

  /**
   * The role-playing game session path.
   * @protected
   */
  protected static RolePlayingGameSessionPath: string = 'role-playing-game-session';

  /**
   * The path for the individual path.
   *
   * @protected
   * @static
   * @type {string}
   * @memberof RolePlayingGameService
   */
  protected static RolePlayingGameSingularPath: string = 'role-playing-game';

  /**
   * Generates the path for adding a character to the role-playing game.
   * @param gameId
   * @param characterGuid
   * @constructor
   * @protected
   */
  protected static AddCharacterToRolePlayingGamePath(gameId: string, characterGuid: string) {
    return `${environment.api_prefix}/role-playing-game/${gameId}/characters/add/${characterGuid}`;
  }

  /**
   * Generates a path for removing a character from a role-playing game.
   * @param gameId
   * @param characterGuid
   * @constructor
   * @protected
   */
  protected static RemoveCharacterFromRolePlayingGamePath(gameId: string, characterGuid: string) {
    return `${environment.api_prefix}/role-playing-game/${gameId}/characters/remove/${characterGuid}`;
  }

  /**
   * The path for listing assets for a role playing game.
   *
   * @protected
   * @static
   * @param {string} gameId
   * @param {RolePlayingGameAssetType} assetType
   * @return {*}
   * @memberof RolePlayingGameService
   */
  protected static ListAssetsForRolePlayingGamePath(gameId: string, assetType: RolePlayingGameAssetType) {
    return `${environment.api_prefix}/role-playing-game/${gameId}/assets/${assetType}`;
  }

  /**
   * Gets the path for the "get characters" API call.
   * @param gameId
   * @constructor
   * @protected
   */
  protected static GetCharactersFromRolePlayingGamePath(gameId: string) {
    return `${environment.api_prefix}/${RolePlayingGameService.IndividualRolePlayingGamePath}/${gameId}/characters`;
  }

  /**
   * Gets a role playing game individually by ID
   * @param gameId
   * @constructor
   * @protected
   */
  protected static GetRolePlayingGameByIdPath(gameId: string) {
    return `${environment.api_prefix}/${RolePlayingGameService.IndividualRolePlayingGamePath}/${gameId}`;
  }

  /**
   * Gets the path for adding a user to a game.
   * @param gameId
   * @param userId
   * @constructor
   * @protected
   */
  protected static GetAddRolePlayingUserAddPath(gameId: string, userId: string) {
    return `${environment.api_prefix}/${RolePlayingGameService.IndividualRolePlayingGamePath}/${gameId}/users/add/${userId}`;
  }

  /**
   * Path for getting dice rolls in session.
   * @param sessionId
   * @param numberRolls
   * @constructor
   * @protected
   */
  protected static GetRolePlayingGameDiceRollsPath(sessionId: string, numberRolls: number = 10) {
    return `${environment.api_prefix}/${RolePlayingGameService.RolePlayingGameSessionPath}/${sessionId}/rolls`;
  }

  /**
   * Path for creating a role playing game
   *
   * @protected
   * @static
   * @param {string} sessionId
   * @memberof RolePlayingGameService
   */
  protected static CreateRolePlayingGameAssetPath(sessionId: string) {
    return `${environment.api_prefix}/${RolePlayingGameService.RolePlayingGameSingularPath}/${sessionId}/assets/create`;
  }

  /**
   * Temporary URL for alpha. After alpha, these are auto-clients.
   * @protected
   */
  protected static IndividualRolePlayingGamePath: string = 'role-playing-game';

  /**
   * Gets the dice rolls for the role-playing game.
   * This is limited to the last 10.
   * TODO: Paginate and deduplicate.
   * @param gameSessionId
   * @constructor
   */
  public GetDiceRollsForRolePlayingGame(gameSessionId: string): Observable<Array<IGameActionRollResult>> {
    const path = RolePlayingGameService.GetRolePlayingGameDiceRollsPath(gameSessionId);
    return this.HttpClient.get<Array<IGameActionRollResult>>(path);
  }

  /**
   * Gets an individual role-playing game.
   * @param gameId
   * @constructor
   */
  public GetRolePlayingGame(gameId: string): Observable<IRolePlayingGame>{
    const path = RolePlayingGameService.GetRolePlayingGameByIdPath(gameId);
    return this.HttpClient.get<IRolePlayingGame>(path);
  }

  /**
   * Gets the current userId
   * @param gameId
   * @constructor
   */
  public AddCurrentUserToRolePlayingGame(gameId: string) {
    const userId: string = this.AuthService.loggedInUser.userID as string;
    const path = RolePlayingGameService.GetAddRolePlayingUserAddPath(gameId, userId);
    return this.HttpClient.get(path);
  }

  /**
   * Adds characters to a role-playing game.
   * @param gameId
   * @param characterGuids
   * @constructor
   */
  public AddCharactersToRolePlayingGame(gameId:string, characterGuids: Array<string>): Observable<any> {
    const observables = characterGuids.map((guid: string) => this.AddCharacterToRolePlayingGame(gameId, guid));
    return forkJoin(observables);
  }

  /**
   * Removes the characters from the role-playing game.
   * @param gameId
   * @param characterGuids
   * @constructor
   */
  public RemoveCharactersFromRolePlayingGame(gameId: string, characterGuids: Array<string>) {
    const observables = characterGuids.map((guid: string) => this.RemoveCharacterFromRolePlayingGame(gameId, guid));
    return forkJoin(observables);
  }

  /**
   * Removes a character from a role-playing game.
   * @param gameId
   * @param characterId
   * @constructor
   */
  public RemoveCharacterFromRolePlayingGame(gameId: string, characterId: string) {
    const path = RolePlayingGameService.RemoveCharacterFromRolePlayingGamePath(gameId, characterId);
    return this.HttpClient.get(path);
  }

  /**
   * Adds a given character to the role-playing game.
   * @param gameId
   * @param characterGuid
   * @constructor
   */
  public AddCharacterToRolePlayingGame(gameId: string, characterGuid: string) {
    const path = RolePlayingGameService.AddCharacterToRolePlayingGamePath(gameId, characterGuid);
    return this.HttpClient
      .get(path);
  }

  /**
   * Lists the characters in the role-playing game.
   * @param gameId
   * @constructor
   */
  public ListCharactersInRolePlayingGame(gameId: string): Observable<Array<ICharacter>> {
    const path = RolePlayingGameService.GetCharactersFromRolePlayingGamePath(gameId);
    return this.HttpClient.get<Array<ICharacter>>(path);
  }

  /**
   * Updates a role playing game with the entire object.
   * This will be discontinued after more elements appear in the RolePlayingGame model.
   * @param game
   * @constructor
   */
  public UpdateRolePlayingGame(id: string, game: IRolePlayingGame) {
    return this.HttpClient
      .put<IRolePlayingGame>(`${environment.api_prefix}/${RolePlayingGameService.IndividualRolePlayingGamePath}/${id}`, game);
  }

  /**
   * Creates a role-playing game.
   * @param game The game object to create a new game
   * @constructor
   */
  public CreateRolePlayingGame(game: IRolePlayingGame) {
    game.CreatedByUser = this.AuthService.loggedInUser.userID as string;
    return this.HttpClient
      .post<boolean>(`${environment.api_prefix}/${RolePlayingGameService.CreateRolePlayingGamesPath}`,game);
  }

  /**
   * Gets role playing games from the API.
   *
   * @memberof RolePlayingGameServiceService
   */
  public GetRolePlayingGames(): Observable<any> {
    return this.HttpClient
      .get(`${environment.api_prefix}/${RolePlayingGameService.ListRolePlayingGamesPath}`);
  }

  /**
   * Deletes a role-playing game.
   * @param id
   * @constructor
   */
  public DeleteRolePlayingGame(id: string): Observable<object> {
    return this.HttpClient
      .delete(`${environment.api_prefix}/${RolePlayingGameService.IndividualRolePlayingGamePath}/${id}`);
  }

  /**
   * Does the role-playing game exist?
   *
   * @param {string} id
   * @return {*}  {Observable<boolean>}
   * @memberof RolePlayingGameService
   */
  public DoesRolePlayingGameExist(id: string): Observable<boolean> {
    return this.HttpClient
      .get<boolean>(`${environment.api_prefix}/${RolePlayingGameService.IndividualRolePlayingGamePath}/${id}/exists`);
  }

  /**
   * Gets the latest session from the role playing game.
   *
   * @param {IRolePlayingGame} game
   * @return {*}  {IRolePlayingGameSession}
   * @memberof RolePlayingGameService
   */
  public GetLatestSessionForGame(game: IRolePlayingGame): IRolePlayingGameSession | undefined {
    return game.Sessions!.find((session: IRolePlayingGameSession) => session.state === 0);
  }

  /**
   * Gets the role-playing game assets for the
   *
   * @param {string} gameId
   * @param {RolePlayingGameAssetType} assetType
   * @return {*}
   * @memberof RolePlayingGameService
   */
  public GetAssetsForGame(gameId: string, assetType: RolePlayingGameAssetType): Observable<Array<IRolePlayingGameAsset>> {
    return this.HttpClient
      .get<Array<IRolePlayingGameAsset>>(
        RolePlayingGameService.ListAssetsForRolePlayingGamePath(gameId, assetType)
      );
  }

  /**
   * Creates a game asset.
   *
   * @param {string} fileName
   * @param {string} title
   * @param {string} desc
   * @param {RolePlayingGameAssetType} assetType
   * @memberof RolePlayingGameService
   */
  public CreateGameAsset(
    sessionId: string,
    fileName: string,
    title: string,
    desc: string,
    assetType: RolePlayingGameAssetType
  ) {
    return this.HttpClient
      .post<IRolePlayingGameAssetRequest>(
        RolePlayingGameService.CreateRolePlayingGameAssetPath(sessionId),
        {
          FileName: fileName,
          Title: title,
          Description: desc,
          AssetType: assetType
        },
        {
          headers: {
            "X-Dynastra-User-Id": this.AuthService.loggedInUser.userID || ""
          }
        }
      );
  }

  /**
   * Creates an instance of RolePlayingGameServiceService.
   * @param {HttpClient} HttpClient
   * @memberof RolePlayingGameServiceService
   */
  constructor(private HttpClient: HttpClient, private AuthService: AuthService) { }
}
