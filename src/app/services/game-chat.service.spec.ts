import { GameChatService } from './game-chat.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

describe('GameChatService', () => {
  let service: GameChatService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ]
    });
    service = TestBed.inject(GameChatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
