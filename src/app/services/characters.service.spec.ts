import {inject, TestBed} from '@angular/core/testing';

import { CharactersService } from './characters.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {MockModule} from "ng-mocks";
import {OAuthModule} from "angular-oauth2-oidc";
import {RouterTestingModule} from "@angular/router/testing";
import {MockAuthService} from "../../../spec/MockAuthService";
import {AuthService, IAuthService} from "./auth.service";
import {ICharacter} from "../../lib/interfaces/Character.interfaces";

describe('CharactersService', () => {
  let service: CharactersService;
  let mockAuthService: IAuthService;
  let mockCharacter: ICharacter = {
    CharacterName: "",
    CharacterAbout: "",
    CharacterImgUrl: "",
    IsPublic: true,
    Id: "",
    UserId: ""
  };
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    mockAuthService = new MockAuthService();
    TestBed.overrideProvider(AuthService, { useValue : mockAuthService });
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MockModule(OAuthModule.forRoot()),
      ],
      providers: [MockAuthService]
    });
    service = TestBed.inject(CharactersService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should correctly get a character', inject(
    [HttpTestingController, CharactersService],
    (httpMock: HttpTestingController, charService: CharactersService) => {

      charService.GetCharacter('').subscribe((char: ICharacter) => {
        expect(char).not.toBeNull();
        expect(char).toEqual(mockCharacter);
      });

      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush(mockCharacter);
    }
  ));
  it('should correctly create a character', inject(
    [HttpTestingController, CharactersService],
    (httpMock: HttpTestingController, charService: CharactersService) => {

      charService.CreateCharacter(mockCharacter).subscribe((char: boolean) => {
        expect(char).not.toBeNull();
        expect(char).toEqual(true);
      });

      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush(true);
    }
  ));
  it('should correctly get a user\'s characters', inject(
    [HttpTestingController, CharactersService],
    (httpMock: HttpTestingController, charService: CharactersService) => {

      charService.GetCharactersForUser().subscribe((char: Array<ICharacter>) => {
        expect(char).not.toBeNull();
        expect(char).toEqual([mockCharacter]);
      });

      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush([mockCharacter]);
    }
  ));
  it('should correctly delete a character', inject(
    [HttpTestingController, CharactersService],
    (httpMock: HttpTestingController, charService: CharactersService) => {

      charService.DeleteCharacter('').subscribe((char: any) => {
        expect(char).not.toBeNull();
        expect(char).toEqual({});
      });

      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush({});
    }
  ));
  it('should correctly update a character', inject(
    [HttpTestingController, CharactersService],
    (httpMock: HttpTestingController, charService: CharactersService) => {

      charService.UpdateCharacter('', mockCharacter).subscribe((char: any) => {
        expect(char).not.toBeNull();
        expect(char).toEqual({});
      });

      const mockReq = httpMock.expectOne(() => true);
      mockReq.flush({});
    }
  ));
});
