import { Injectable } from '@angular/core';
import {IGameBoardSettings} from "../../lib/interfaces/RolePlayingGame.interfaces";
import {BehaviorSubject, distinctUntilChanged, Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class GameBoardService {
  /**
   * The settings stored for the session.
   * @protected
   */
  protected StoredSettings: IGameBoardSettings = {
    gridSettings: {
      maxZoom: 1,
      minZoom: 0.025,
      gridLineColor: '#000000',
      gridLineStyle: 'solid',
      gridLineWidth: 3
    }
  };

  /**
   * The game board settings were changed.
   * @protected
   */
  protected gameBoardSettingsChanged$: BehaviorSubject<IGameBoardSettings> = new BehaviorSubject<IGameBoardSettings>(this.StoredSettings);

  /**
   * Subscriber for when settings are changed.
   * TODO: Subscribe on property
   */
  public onSettingsChanged(): Observable<IGameBoardSettings> {
    return this.gameBoardSettingsChanged$;
  }

  /**
   * Updates the settings for the game board.
   * @param settings
   */
  public updateSettings(settings: IGameBoardSettings) {
    this.StoredSettings = Object.assign(this.StoredSettings, settings);
    this.gameBoardSettingsChanged$.next(this.StoredSettings);
  }

  /**
   * Gets the settings for the game board.
   */
  public getSettings(): IGameBoardSettings {
    return this.StoredSettings;
  }
}
