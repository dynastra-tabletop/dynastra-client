import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IChatMatrixGenericEvent, IChatMatrixJoinRoomResponse, IChatMatrixLeaveRoomRequest, IChatMatrixLoginRequest, IChatMatrixLoginResponse, IChatMatrixSyncResponse, IChatMatrixSyncResponseInviteContainer, IChatMatrixSyncRoomsEvent, IDynastraChatEvent, IDynastraCompactChatEvent, IDynastraCompactReaction } from 'src/lib/interfaces/Chat.interfaces';
import { concatMap, filter, firstValueFrom, interval, map, merge, Observable, ReplaySubject, Subject } from 'rxjs';
import { DynastraMatrixDataTransformer } from 'src/lib/Chat.utils';
import { noop } from 'lodash';
import { MatrixEventTypes } from 'src/lib/enums/Chat.enums';
import { environment } from 'src/environments/environment';
const _ = { noop };

@Injectable({
  providedIn: 'root'
})
export class GameChatService {
  /**
   * Poll chat every n ms.
   *
   * @private
   * @static
   * @type {number}
   * @memberof GameChatService
   */
  private static POLL_INTERVAL: number = 1000;

  /**
   * The base URL for the chat server.
   *
   * @private
   * @static
   * @memberof GameChatService
   */
  private static BASE_URL = "http://localhost:8008/_matrix/client/";
  private static SERVER_URL = "http://localhost:8010"

  /**
   * The ID of the user that has logged into the matrix instance.
   *
   * @readonly
   * @type {string}
   * @memberof GameChatService
   */
  public get LoggedInUserId(): string {
    return this.LoggedInUser?.user_id || '';
  }

  /**
   * The user that is currently logged into the Matrix server.
   *
   * @private
   * @type {IChatMatrixLoginResponse}
   * @memberof GameChatService
   */
  private LoggedInUser?: IChatMatrixLoginResponse;

  /**
   * Is the user currently logged into Matrix?
   *
   * @private
   * @type {boolean}
   * @memberof GameChatService
   */
  private OnLoginCompleted$: ReplaySubject<boolean> = new ReplaySubject<boolean>();

  /**
   * The previous sync response.
   *
   * @private
   * @type {IChatMatrixSyncResponse}
   * @memberof GameChatService
   */
  private PreviousSyncResult: IChatMatrixSyncResponse | null = null;

  /**
   * An instance of the transformer to change the data from one format to the other.
   *
   * @private
   * @type {DynastraMatrixDataTransformer}
   * @memberof GameChatService
   */
  private Transformer: DynastraMatrixDataTransformer = new DynastraMatrixDataTransformer();

  /**
   * The subject for broadcasting the changes for chat as they are received.
   *
   * @private
   * @type {Subject<IDynastraCompactChatEvent>}
   * @memberof GameChatService
   */
  private Chat$: Subject<IDynastraCompactChatEvent> = new Subject<IDynastraCompactChatEvent>();

  /**
   * The subject for when reactions are being emitted.
   *
   * @private
   * @type {Subject<IDynastraCompactReaction>}
   * @memberof GameChatService
   */
  private Reactions$: Subject<IDynastraCompactReaction> = new Subject<IDynastraCompactReaction>();

  /**
   * Outputs when a room is joined.
   *
   * @private
   * @type {Subject<string>}
   * @memberof GameChatService
   */
  private Join$: Subject<string> = new Subject<string>();

  /**
   * The chat historyu
   *
   * @type {Array<IDynastraChatEvent>}
   * @memberof GameChatService
   */
  public ChatHistory: Array<IDynastraCompactChatEvent> = [];

  /**
   * All the rooms this current client is a part of.
   *
   * @private
   * @type {Array<string>}
   * @memberof GameChatService
   */
  public JoinedRooms: Array<string> = [];

  /**
   * List of rooms invites should be accepted for (e.g. This current session)
   *
   * @private
   * @type {Array<string>}
   * @memberof GameChatService
   */
  private AcceptRoomInvitesFrom: Array<string> = [];

  /**
   * Waits for login.
   *
   * @return {*}  {Observable<boolean>}
   * @memberof GameChatService
   */
  public waitForLogin(): Promise<boolean> {
    return firstValueFrom(this.OnLoginCompleted$);
  }

  /**
   * Requests that the client be invited to the room.
   * We can't knock on a room, because it's not private.
   * And, to make it restful, the client needs to say "I'm here, let me in"
   *
   * @param {string} roomId
   * @param {string} userId
   * @memberof GameChatService
   */
  public requestInviteToRoom(
    sessionId: string,
    roomIdentifier: string,
    userIdentifier: string
  ): Promise<any> {
    if (this.JoinedRooms.includes(roomIdentifier)) {
      return Promise.resolve();
    }

    return firstValueFrom(this.http
    .post(
      `${environment.api_prefix}/role-playing-game-session/${sessionId}/chatInvite`,
      {
        RoomIdentifier: roomIdentifier,
        UserIdentifier: userIdentifier,
      }
    ));
  }

  /**
   * Listens for chat events specified.
   * TODO: Wait for gcs bootup before piping
   * @param {string} roomTopic
   * @memberof GameChatService
   */
  public ListenForChatEvents(roomId: string) {
    return this.Chat$.pipe(
      filter((chatItem: IDynastraCompactChatEvent) => {
        return roomId === chatItem.roomId;
      })
    );
  }

  /**
   * Listens for reaction events (e.g. emoji reacts)
   *
   * @param {string} roomId
   * @return {*}
   * @memberof GameChatService
   */
  public ListenForReactionEvents(roomId: string) {
    return this.Reactions$.pipe(
      filter((reactEvent: IDynastraCompactReaction) => reactEvent.roomId === roomId)
    );
  }

  /**
   * Listens for when a room is joined.
   *
   * @param {string} roomId
   * @return {*}
   * @memberof GameChatService
   */
  public ListenForRoomJoin(roomId: string) {
    return this.Join$.pipe(
      filter((room: string) => room === roomId)
    );
  }

  /**
   * Sends a text-only chat message.
   *
   * @param {string} roomIdentifier
   * @param {string} messageText
   * @memberof GameChatService
   */
  public sendTextChatMessage(roomIdentifier: string, messageText: string) {
    return firstValueFrom(this.http
      .post<IChatMatrixGenericEvent>(`${GameChatService.BASE_URL}r0/rooms/${roomIdentifier}/send/m.room.message`, {
        msgtype: 'm.text',
        body: messageText
      }, {
        headers: {
          Authorization: `Bearer ${this.LoggedInUser?.access_token || ''}`
        },
      }));
  }

  /**
   * Instructs the service to accept invites with a given topic.
   *
   * @param {string} roomIdentifier
   * @memberof GameChatService
   */
  public acceptInvitesFrom(roomIdentifier: string) {
    this.AcceptRoomInvitesFrom.push(roomIdentifier);
  }

  /**
   * Knocks on the room
   *
   * @protected
   * @param {string} roomAlias
   * @memberof GameChatService
   */
  public async knockOnRoom(roomAlias: string) {
    await firstValueFrom(this.OnLoginCompleted$);
    return await firstValueFrom(this.http
      .post(`${GameChatService.BASE_URL}v3/knock/${roomAlias}`, {}, {
        headers: {
          Authorization: `Bearer ${this.LoggedInUser?.access_token || ''}`
        },
      }));
  }

  /**
   * Leaves a given room.
   *
   * @protected
   * @param {string} roomIdentifier
   * @memberof GameChatService
   */
  protected async leaveChatRoom(roomIdentifier: string): Promise<any> {
    return await firstValueFrom(this.http
        .post<IChatMatrixLeaveRoomRequest>(`${GameChatService.BASE_URL}v3/rooms/${roomIdentifier}/leave`, {
          reason: 'Chat Client Initialising, resetting Dynastra Client state..',
        }, {
          headers: {
            Authorization: `Bearer ${this.LoggedInUser?.access_token || ''}`
          },
          params: new HttpParams({
            fromObject: {
              // NOTE: Err, Matrix, why? It's in the path...? (Still love you BTW.)
              roomId: roomIdentifier
            }
          })
        })
    )
  }

  /**
   * Gets the joined rooms for this given user.
   *
   * @protected
   * @return {*}
   * @memberof GameChatService
   */
  protected async getJoinedRooms(): Promise<Array<string>> {
    return await firstValueFrom(this.http.get<{ joined_rooms: Array<string> }>(`${GameChatService.BASE_URL}v3/joined_rooms`,{
      headers: {
        Authorization: `Bearer ${this.LoggedInUser?.access_token || ''}`
      },
    }).pipe(map((val: { joined_rooms: Array<string> }) => val.joined_rooms)));
  }

  /**
   * Joins a given room with a reason.
   * NOTE: Stuff like encryption and keys, post-beta.
   *
   * @protected
   * @param {string} roomIdentifier
   * @param {string} [reason='']
   * @return {*}  {Promise<IChatMatrixJoinRoomResponse>}
   * @memberof GameChatService
   */
  protected async joinRoom(roomIdentifier: string, reason: string = ''): Promise<IChatMatrixJoinRoomResponse> {
    return await firstValueFrom(
      this.http.post<IChatMatrixJoinRoomResponse>(
        `${GameChatService.BASE_URL}v3/join/${roomIdentifier}`,
        {
          reason,
        },
        {
          headers: {
            Authorization: `Bearer ${this.LoggedInUser?.access_token || ''}`
          },
          params: new HttpParams({
            fromObject: {
              roomIdOrAlias: roomIdentifier,
            }
          })
        }
      )
    )
  }

  /**
   * A temporary holdover for the client to use to log into
   * the matrix chat server.
   *
   * @protected
   * @return {*}
   * @memberof GameChatService
   */
  protected async holdoverLogin(): Promise<IChatMatrixLoginResponse> {
    return await firstValueFrom(this.http
      .post<IChatMatrixLoginResponse>(
        `${GameChatService.BASE_URL}r0/login`,
        {
          type: 'm.login.password',
          user: 'dynastra_service',
          password: 'Dynastra'
        }
      ));
  }

  /**
   * Starts to sync with the server.
   *
   * @protected
   * @return {*}  {Promise<any>}
   * @memberof GameChatService
   */
  protected sync(): Observable<IChatMatrixSyncResponse> {
    const hasSyncResult = this.PreviousSyncResult !== null;
    const params = {};

    if (!hasSyncResult) {
      params['full_state'] = true;
    } else {
      params['since'] = this.PreviousSyncResult?.next_batch || '';
    }

    return this.http.get<IChatMatrixSyncResponse>(`${GameChatService.BASE_URL}v3/sync`, {
      headers: {
        Authorization: `Bearer ${this.LoggedInUser?.access_token || ''}`
      },
      params: new HttpParams({
        fromObject: params
      })
    });
  }


  /**
   * Handles an individual invite, which, on the face of it, the user
   *
   * @protected
   * @param {IChatMatrixSyncResponseInviteContainer} container
   * @memberof GameChatService
   */
  protected async handleIndividualInvite(roomIdentifier: string, container: IChatMatrixSyncResponseInviteContainer) {
    const topic: any = this.Transformer.getEvent(container.invite_state.events, MatrixEventTypes.RoomTopic);

    if (!topic) {
      await this.leaveChatRoom(roomIdentifier);
      return;
    }

    // Separate here because includes() is more expensive, don't wanna waste it if it has no topic.
    if (!this.AcceptRoomInvitesFrom.includes(topic.content.topic)) {
      await this.leaveChatRoom(roomIdentifier);
      return;
    }

    try {
      await this.joinRoom(roomIdentifier);
      this.Join$.next(roomIdentifier);
    } catch  {}

  }

  /**
   * Handles any invites which have been sent to the client.
   *
   * @protected
   * @param {IChatMatrixSyncResponse} update
   * @memberof GameChatService
   */
  protected async handleInvite(update: IChatMatrixSyncResponse) {
    if (!update.rooms.invite) {
      return;
    }

    await Object.keys(update.rooms.invite)
      .forEach(async (roomName: string) => {
        const roomItem = update!.rooms!.invite![roomName];

        if (roomItem) {
          const inviteStates = roomItem.invite_state.events;
          const hasTopic = this.Transformer.hasEvent(
            inviteStates,
            MatrixEventTypes.RoomTopic
          );

          if (!hasTopic) {
            // We don't care, this isn't managed by Dynastra,
            // and we're not gonna bother with it.
            await this.leaveChatRoom(roomName);
          } else {
            await this.handleIndividualInvite(roomName, roomItem);
          }
        }
      });
  }

  /**
   * When the sync response is received, re-broadcasts it as seperate
   *
   * @protected
   * @param {IChatMatrixSyncResponse} update
   * @memberof GameChatService
   */
  protected rebroadcastSync(update: IChatMatrixSyncResponse): void {
    // Allow to fall through, because multiple things can happen in one update.
    if (this.Transformer.hasInviteEvents(update)) {
      this.handleInvite(update).then(_.noop)
    }

    this.PreviousSyncResult = update;

    const convertedToDynastraEvents = this.Transformer.convertSyncResponseToChatEvents(
      update,
      this.ChatHistory
    );

    convertedToDynastraEvents.messageEvents.forEach((chatEvent: IDynastraCompactChatEvent) => {
      this.ChatHistory.push(chatEvent);
      this.Chat$.next(chatEvent);
    });

    convertedToDynastraEvents.reactEvents.forEach((reactEvent: IDynastraCompactReaction) => {
      const foundItemIndex = this.ChatHistory.findIndex((item: IDynastraCompactChatEvent) => item.messageId === reactEvent.originalMessageId)

      // The react is from a message we didn't have any history of.
      if(foundItemIndex === -1) {
        return;
      }

      this.ChatHistory[foundItemIndex].emojiReactions.push(reactEvent);
      this.Reactions$.next(reactEvent);
    });

  }

  /**
   * Starts the loop to sync with the client.
   *
   * @protected
   * @memberof GameChatService
   */
  protected doSyncLoop(): void {
    interval(GameChatService.POLL_INTERVAL).pipe(
      concatMap((interval) => this.sync()),
    ).subscribe((update: IChatMatrixSyncResponse) => this.rebroadcastSync(update));
  }

  /**
   * Initialises the game chat service.
   *
   * @protected
   * @memberof GameChatService
   */
  protected async init(): Promise<void> {
    // Do the login
    this.LoggedInUser = await this.holdoverLogin();
    this.JoinedRooms = await this.getJoinedRooms();
    this.OnLoginCompleted$.next(true);

    // Then we want to sync repeatedly.
    this.doSyncLoop();
  }

  /**
   * Creates an instance of GameChatService.
   * @memberof GameChatService
   */
  constructor(protected readonly http: HttpClient) {
    this.init().then(() => {});
  }
}
