import {Component, OnInit, ViewChild} from '@angular/core';
import {RolePlayingGameService} from "../../services/role-playing-game.service";
import {IRolePlayingGame} from "../../../lib/interfaces/RolePlayingGame.interfaces";
import {ParamMap, ActivatedRoute} from "@angular/router";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {
  SelectUserCharacterModalComponent
} from "../../components/select-user-character-modal/select-user-character-modal.component";
import {ICharacter} from "../../../lib/interfaces/Character.interfaces";
import {MatSelectionList} from "@angular/material/list";
import {
  RolePlayingGameCharactersComponent
} from "../../components/role-playing-game-characters/role-playing-game-characters.component";

@Component({
  selector: 'app-role-playing-game',
  templateUrl: './role-playing-game.component.html',
  styleUrls: ['./role-playing-game.component.scss']
})
export class RolePlayingGameComponent implements OnInit {

  /**
   * Outlines if an error was encountered.
   * @protected
   */
  protected EncounteredError: boolean | null = null;

  /**
   * The ID received from the page URL.
   */
  public GameId: string = '';

  /**
   * The game that has been loaded.
   * @protected
   */
  public Game: IRolePlayingGame | null = null;

  /**
   * Is the REST resource loading?
   * @protected
   */
  protected IsRESTLoading: boolean = false;

  /**
   * The reference to the dialogue.
   * @protected
   */
  protected DialogRef!: MatDialogRef<any>;

  /**
   * The component for the characters.
   */
  @ViewChild('charactersComponent')
  public CharactersComponent!: RolePlayingGameCharactersComponent;

  /**
   * Is this component currently loading?
   * @constructor
   * @protected
   */
  public get IsLoading(): boolean {
    return this.IsRESTLoading;
  }

  /**
   * Selects characters to add into the game.
   * @param $event
   */
  public selectCharacters(remove: boolean) {
    if (this.Game === null) {
      return;
    }

    this.DialogRef = this.Dialog.open(SelectUserCharacterModalComponent, {
      data : {
        remove,
        existingCharacters: this.Game.Characters,
        rolePlayingGameId: this.GameId,
      }
    });
    this.DialogRef!.afterClosed().subscribe(() => {
      this.GetRolePlayingGame(this.GameId);
      this.CharactersComponent.reloadCharacters();
    });
  }

  /**
   * Single-use function for when the load is finished.
   * @param error
   * @protected
   */
  protected onLoadFinished(error: boolean = false) {
      if (error) {
        this.EncounteredError = true;
      }
      this.IsRESTLoading = false;
  }

  /**
   * Gets the role-playing game from the service.
   * @param gameId
   * @constructor
   * @protected
   */
  protected GetRolePlayingGame(gameId: string) {
    this.IsRESTLoading = true;
    this.RolePlayingGamesService.GetRolePlayingGame(gameId)
      .subscribe({
        next: (game: IRolePlayingGame) => {
          this.Game = game;
          this.onLoadFinished();
        },
        error: () => {
          this.onLoadFinished(true);
        },
        complete: () => {
          this.onLoadFinished();
        }
      });
  }

  /**
   * When the component is loaded, begin updating with properties.
   */
  ngOnInit(): void {
    this.ActivatedRoute.paramMap
      .subscribe((params: ParamMap) => {
        const id = params.get('role-playing-game-id') as string;
        this.GameId = id;
        this.GetRolePlayingGame(id);
      });
  }

  /**
   * Creates a new instance of the role-playing game component.
   * @param RolePlayingGamesService
   */
  constructor(
    protected readonly Dialog: MatDialog,
    protected readonly RolePlayingGamesService: RolePlayingGameService,
    protected readonly ActivatedRoute: ActivatedRoute
  ) {

  }

}
