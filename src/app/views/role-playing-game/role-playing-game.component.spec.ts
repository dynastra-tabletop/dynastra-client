import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { OAuthModule } from 'angular-oauth2-oidc';
import { MockComponent, MockModule } from 'ng-mocks';
import { from, Observable } from 'rxjs';

import { MockAuthService } from '../../../../spec/MockAuthService';
import { IRolePlayingGame } from '../../../lib/interfaces/RolePlayingGame.interfaces';
import {
  RolePlayingGameCharactersComponent,
} from '../../components/role-playing-game-characters/role-playing-game-characters.component';
import { AuthService } from '../../services/auth.service';
import { RolePlayingGameService } from '../../services/role-playing-game.service';
import { RolePlayingGameComponent } from './role-playing-game.component';

describe('RolePlayingGameComponent', () => {
  let component: RolePlayingGameComponent;
  let fixture: ComponentFixture<RolePlayingGameComponent>;
  let game: IRolePlayingGame = {
    Name: 'Ducky',
    CreatedByUser: '',
    Description: '',
    Id: '',
    ImageUrl: '',
    Characters: [],
    Users: []
  };

  /**
   * Creates the default test bed.
   */
  function createDefaultTestBed() {
    TestBed.overrideProvider(AuthService, { useValue : new MockAuthService() });
    TestBed.overrideProvider(RolePlayingGameService, {
      useValue: {
        GetRolePlayingGame: (gameId: string) => {
          return new Observable((obs) => {
            obs.next(game);
            obs.complete();
          });
        }
      }
    });
    fixture = TestBed.createComponent(RolePlayingGameComponent);
    component = fixture.componentInstance;
    component.CharactersComponent = {
      reloadCharacters: () => {}
    } as unknown as RolePlayingGameCharactersComponent;
    fixture.detectChanges();
  }

  /**
   * Creates an error testbed.
   */
  function createErroringTestBed() {
    TestBed.overrideProvider(AuthService, { useValue : new MockAuthService() });
    TestBed.overrideProvider(RolePlayingGameService, {
      useValue: {
        GetRolePlayingGame: (gameId: string) => {
          return new Observable(obs => {
            obs.error('');
          });
        }
      }
    });
    fixture = TestBed.createComponent(RolePlayingGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        HttpClientTestingModule,
        MatDialogModule,
        MockModule(OAuthModule.forRoot()),
        RouterTestingModule
      ],
      declarations: [ RolePlayingGameComponent, MockComponent(RolePlayingGameCharactersComponent) ],
      providers: [{
        provide: ActivatedRoute,
        useValue: {
          paramMap: from([{
            get: (val: string = 'no-error') => {
              if (val === 'error') {
                return 'error';
              }
              return '00000000-0000-0000-0000-000000000000';
            }
          }]),
        },
      }, {
        provide: MatDialog,
        useValue: {
          open: () => ({
            afterClosed: () => new Observable((obs) => {
              obs.next(null);
            })
          })
        }
      }]
    })
    .compileComponents();
  });

  it('should create', () => {
    createDefaultTestBed();
    expect(component).toBeTruthy();
  });

  it('should load its role playing game after its route is injected', () => {
    createDefaultTestBed();
    expect(component.GameId).toEqual('00000000-0000-0000-0000-000000000000')
  });

  it('should correctly load the role-playing game', () => {
    createDefaultTestBed();
    expect(component.Game).toEqual(game);
  });

  it('should correctly handle loading if an error occurs', () => {
    createErroringTestBed();
    expect(component.IsLoading).toBeFalse();
  });

  it('should open a dialog and then refresh when it\'s closed',() => {
    createDefaultTestBed();
    component.selectCharacters(false);
    expect(component.Game).not.toBeNull();
    component.selectCharacters(true);
    expect(component.Game).not.toBeNull();
  });

  it('should not do anything if the game was not correctly loaded',() => {
    createErroringTestBed();
    try {
      component.selectCharacters(false);
    } catch {
      expect(component.IsLoading).toEqual(false);
    }
  });

});
