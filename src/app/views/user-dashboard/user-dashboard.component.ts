import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent {
  /**
   * The name of the current logged-in user.
   * @constructor
   */
  public get LoggedInUserName(): string {
    return this.AuthService.loggedInUser.username || "";
  }

  /**
   * Provides the redirect URL for the user to get their profile from.
   * @constructor
   */
  public get RedirectURL(): string {
    return this.AuthService.getIssuerAccountRedirect();
  }

  /**
   * Creates a new instance of the dashboard component.
   * @param AuthService
   */
  constructor(
    protected readonly AuthService: AuthService,
  ) { }
}
