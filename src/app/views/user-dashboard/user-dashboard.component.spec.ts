import {ComponentFixture, inject, TestBed} from '@angular/core/testing';

import { UserDashboardComponent } from './user-dashboard.component';
import {MockModule} from "ng-mocks";
import {OAuthModule} from "angular-oauth2-oidc";
import {RouterTestingModule} from "@angular/router/testing";
import {MockAuthService, UninitialisedAuthService} from "../../../../spec/MockAuthService";
import {AuthService, IAuthService} from "../../services/auth.service";

describe('UserDashboardComponent', () => {
  let component: UserDashboardComponent;
  let fixture: ComponentFixture<UserDashboardComponent>;
  let mockAuthService: IAuthService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MockModule(OAuthModule.forRoot()),
        RouterTestingModule,
      ],
      declarations: [ UserDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    mockAuthService = new MockAuthService();
    TestBed.overrideProvider(AuthService, { useValue : mockAuthService });
    fixture = TestBed.createComponent(UserDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should allow the user to view their profile', () => {
    const link = fixture.nativeElement.querySelector(".dynastra-user-dashboard-action a");
    expect(link.href.slice(-1)).toEqual("#");
  });
  it('if the username is not set, the username should not error', () => {
    mockAuthService.loggedInUser.username = undefined;
    fixture.detectChanges();
    expect(component.LoggedInUserName).toEqual("");
  });
});
