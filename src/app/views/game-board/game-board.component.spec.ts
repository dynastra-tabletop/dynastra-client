import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { MockProvider } from 'ng-mocks';
import { from, Observable } from 'rxjs';
import { GameChatService } from 'src/app/services/game-chat.service';
import { SnackbarService } from 'src/app/services/snackbar.service';

import { ActionService } from '../../services/action.service';
import { RolePlayingGameService } from '../../services/role-playing-game.service';
import { GameBoardComponent } from './game-board.component';

describe('GameBoardComponent', () => {
  let component: GameBoardComponent;
  let fixture: ComponentFixture<GameBoardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule,
        HttpClientTestingModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              url: [null, {
                path: '00000000-0000-0000-0000-000000000000'
              }]
            },
            paramMap: from([{
              get: (val: string = 'no-error') => {
                if (val === 'error') {
                  return 'error';
                }
                return '00000000-0000-0000-0000-000000000000';
              }
            }]),
          },
        },
        MockProvider(ActionService, {
          joinGameSession: () => new Observable(),
          ListenToAction: () => new Observable(),
        }),
        MockProvider(RolePlayingGameService, {
          GetDiceRollsForRolePlayingGame: () => from([]),
          GetRolePlayingGame: () => from([{}]) as Observable<any>
        }),
        MockProvider(SnackbarService, {}),
        MockProvider(GameChatService, {
          ListenForChatEvents: () => new Observable<any>(),
          ListenForReactionEvents: () => new Observable<any>(),
          sendTextChatMessage: () => Promise.resolve({
            event_id: ''
          }),
        })
      ],
      declarations: [ GameBoardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle the drawer when requested', () => {
    component.toggleDrawer();
    fixture.detectChanges();
    const collapsedMenu = fixture.nativeElement.querySelector('.dynastra-game-board-collapsed-menu');
    expect(collapsedMenu).not.toBeNull();
  });

  it('should correctly send a message', () => {
    component.GameSession = undefined;
    fixture.detectChanges();
    component.OnChatMessageSent('testing');
  });
});
