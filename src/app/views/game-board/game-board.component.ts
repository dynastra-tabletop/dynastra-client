import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { IDynastraCompactChatEvent, IDynastraCompactReaction } from 'src/lib/interfaces/Chat.interfaces';
import { IGameActionResult, IGameActionRollResult } from '../../../lib/interfaces/Action.interfaces';
import { IRolePlayingGame, IRolePlayingGameSession } from 'src/lib/interfaces/RolePlayingGame.interfaces';

import { ActionService } from '../../services/action.service';
import { ActivatedRoute } from '@angular/router';
import {
  GameBoardSnackbarRollResultComponent,
} from '../../components/game-board-snackbar-roll-result/game-board-snackbar-roll-result.component';
import { GameChatService } from 'src/app/services/game-chat.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RolePlayingGameService } from '../../services/role-playing-game.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { Subscription } from 'rxjs';
import { noop } from 'lodash';

const _  = { noop };

@Component({
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.scss']
})
export class GameBoardComponent implements OnInit, OnDestroy {
  /**
   * Display the side-drawer?
   */
  public displayDrawer: boolean = true;

  /**
   * The game session ID.
   * @protected
   */
  public GameSessionId!: string;

  /**
   * The role-playing game.
   *
   * @type {IRolePlayingGame}
   * @memberof GameBoardComponent
   */
  public RolePlayingGame!: IRolePlayingGame;

  /**
   * The game session that is currently in
   *
   * @type {IRolePlayingGameSession}
   * @memberof GameBoardComponent
   */
  public GameSession!: IRolePlayingGameSession | undefined;

  /**
   * Are there unread chats?
   *
   * @type {number}
   * @memberof GameBoardComponent
   */
  public UnreadChats: number = 0;

  /**
   * The action service subscription.
   * @protected
   */
  protected ActionServiceSubscription!: Subscription;

  /**
   * The roleplaying game service subscription.
   *
   * @protected
   * @type {Subscription}
   * @memberof GameBoardComponent
   */
  protected RolePlayingGameServiceSubscription!: Subscription;

  /**
   * The subscription for game chat.
   *
   * @protected
   * @type {Subscription}
   * @memberof GameBoardComponent
   */
  protected GameChatServiceSubscription!: Subscription;

  /**
   * The roll result.
   * @protected
   */
  public RollHistory: Array<IGameActionRollResult> = [];

  /**
   * The history of the chat received from the
   *
   * @type {Array<IDynastraCompactChatEvent>}
   * @memberof GameBoardComponent
   */
  public GameBoardChatHistory: Array<IDynastraCompactChatEvent> = [];

  /**
   * Are we waiting for the chat history from the server?
   *
   * @type {boolean}
   * @memberof GameBoardComponent
   */
  public IsChatHistoryLoading: boolean = true;

  /**
   * Toggles drawer display
   */
  public toggleDrawer(): void {
    this.UnreadChats = 0;
    this.displayDrawer = !this.displayDrawer;
  }

  /**
   * Sends a message
   *
   * @param {string} message
   * @memberof GameBoardComponent
   */
  public OnChatMessageSent(message: string) {
    this.GameChatService.sendTextChatMessage(this.GameSession?.ChatRoomIdentifier || '', message)
      .then(_.noop);
  }

  /**
   * Displays a roll result snackbar.
   * @param actionResult
   * @private
   */
  private displayRollResult(actionResult: IGameActionResult): void {
    this.SnackBar.openFromComponent(GameBoardSnackbarRollResultComponent, {
      horizontalPosition: "end",
      verticalPosition: "bottom",
      duration: 4000,
      politeness: 'assertive',
      data: {
        actionResult,
      }
    })
  }

  /**
   * Adds the rolls to the component, removing duplicates.
   * @param results
   * @private
   */
  private addRollsToExisting(results: Array<IGameActionRollResult>) {
      if (this.RollHistory.length === 0) {
        this.RollHistory = results;
      } else {
        const withoutDuplicates = results.filter((result: IGameActionRollResult) => {
          return this.RollHistory.find((existing: IGameActionRollResult) => existing.id === result.id) === undefined;
        });
        this.RollHistory = this.RollHistory.concat(withoutDuplicates);
      }
      this.ChangeDetector.markForCheck();
  }

  /*
    m.relates_to: {rel_type: "m.annotation", event_id: "$16497383465ZfOWf:localhost", key: "😀"}
    event_id: "$16497383465ZfOWf:localhost"
    key: "😀"
    rel_type: "m.annotation"
  */

  /**
   * Handles a new chat event
   *
   * @private
   * @param {IDynastraChatEvent} event
   * @memberof GameBoardComponent
   */
  private handleNewChatEvent(event: IDynastraCompactChatEvent): void {
    this.IsChatHistoryLoading = false;
    this.GameBoardChatHistory.push(event);

    if (!this.displayDrawer) {
      this.UnreadChats = this.UnreadChats + 1;
      this.SnackBarService.Message(
        'message',
        event.sender,
        event.messageBody
      );
    }

    this.ChangeDetector.detectChanges();
  }

  /**
   * Handles a reaction.
   *
   * @private
   * @param {IDynastraCompactReaction} event
   * @memberof GameBoardComponent
   */
  private handleChatReaction(event: IDynastraCompactReaction): void {
    this.IsChatHistoryLoading = false;
    const originalMessage = this.GameBoardChatHistory
      .findIndex((item: IDynastraCompactChatEvent) => item.messageId === event.originalMessageId);

    // It's for something we don't have in our history
    if (originalMessage === -1) {
      return;
    }

    const foundItem = this.GameBoardChatHistory[originalMessage]
      .emojiReactions
      .findIndex((item: IDynastraCompactReaction) => item.reactionId === event.reactionId);

    if (foundItem !== -1) {
      return;
    }

    if (!this.displayDrawer) {
      this.UnreadChats = this.UnreadChats + 1;
      this.SnackBarService.Message(
        'emoji',
        'Reaction', // TODO: Full matrix, this should be the sender.
        event.targetEmoji
      );
    }

    this.GameBoardChatHistory[originalMessage].emojiReactions.push(event);
    this.ChangeDetector.detectChanges();
  }

  /**
   * Inititialises the chat for the game board.
   *
   * @private
   * @return {*}  {Promise<void>}
   * @memberof GameBoardComponent
   */
  private async initialiseChat(): Promise<void> {
    // Wait for login, then ask for invite if we don't have it.
    await this.GameChatService.waitForLogin();
    await this.GameChatService.requestInviteToRoom(
      this.GameSession?.SessionId || '',
      this.GameSession?.ChatRoomIdentifier || '',
      this.GameChatService.LoggedInUserId,
    );

    this.IsChatHistoryLoading = false;

    this.GameChatServiceSubscription = this.GameChatService
      .ListenForChatEvents(this.GameSession?.ChatRoomIdentifier || '')
      .subscribe((event: IDynastraCompactChatEvent) => this.handleNewChatEvent(event));

    this.GameChatService
      .ListenForReactionEvents(this.GameSession?.ChatRoomIdentifier || '')
      .subscribe((event: IDynastraCompactReaction) => this.handleChatReaction(event));
  }

  /**
   * Called when component is created.
   */
  public ngOnInit(): void {
    // Join this game session so we're in the correct
    this.ActionService
        .joinGameSession(this.GameSessionId)
        .subscribe(noop);

    this.RolePlayingGameServiceSubscription = this.RolePlayingGameService
      .GetDiceRollsForRolePlayingGame(this.GameSessionId)
      .subscribe((rolls: Array<IGameActionRollResult> ) => {
        this.addRollsToExisting(rolls);
      });

    this.ActionServiceSubscription = this.ActionService.ListenToAction("RollDice")
      .subscribe({
        next: (actionResponse: IGameActionResult) => {
          this.displayRollResult(actionResponse);
          this.addRollsToExisting([actionResponse.gameActionResult]);
        },
      });

    this.RolePlayingGameService.GetRolePlayingGame(this.GameSessionId)
      .subscribe((game: IRolePlayingGame) => {
        this.RolePlayingGame = game;
        this.GameSession = this.RolePlayingGameService.GetLatestSessionForGame(game);
        this.initialiseChat().then(_.noop);
      });
  }

  /**
   * Removes subscriptions on the game board when the component is destroyed.
   *
   * @memberof GameBoardComponent
   */
  public ngOnDestroy(): void {
    this.ActionServiceSubscription.unsubscribe();
    this.GameChatServiceSubscription?.unsubscribe();
    this.RolePlayingGameServiceSubscription.unsubscribe();
  }

  /**
   * Creates a new instance of the game board.
   * @param Route
   * @param ActionService
   */
  constructor(
    protected readonly ChangeDetector: ChangeDetectorRef,
    protected readonly Route: ActivatedRoute,
    protected readonly ActionService: ActionService,
    protected readonly SnackBar: MatSnackBar,
    protected readonly RolePlayingGameService: RolePlayingGameService,
    protected readonly GameChatService: GameChatService,
    protected readonly SnackBarService: SnackbarService
    ) {
    const currentRoute = this.Route.snapshot.url;
    this.GameSessionId = currentRoute[1].path;
  }
}
