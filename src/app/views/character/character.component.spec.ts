import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CharacterComponent} from './character.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {MockModule, MockProvider} from "ng-mocks";
import {OAuthModule} from "angular-oauth2-oidc";
import {RouterTestingModule} from "@angular/router/testing";
import {MockAuthService} from "../../../../spec/MockAuthService";
import {AuthService, IAuthService} from "../../services/auth.service";
import {MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {CharactersService} from "../../services/characters.service";
import {from} from "rxjs";
import {ICharacter} from "../../../lib/interfaces/Character.interfaces";
import {UserCharactersComponent} from "../user-characters/user-characters.component";
import {IsLoggedInGuard} from "../../guards/is-logged-in.guard";
import {CharacterDisplayType} from "./character.enums";

const exampleCharacter: ICharacter = {
  Id: 'test',
  UserId: 'test',
  CharacterName: 'test',
  CharacterAbout: 'test',
  CharacterImgUrl: 'https://localhost/lolz.png',
  IsPublic: true
};

const charactersServiceOverrides = {
  DeleteCharacter: (id: string) => from([1]),
  UpdateCharacter: (id: string) => from([true]),
  GetCharacter: (id: string) => from([exampleCharacter]),
};

describe('CharacterComponent', () => {
  let component: CharacterComponent;
  let fixture: ComponentFixture<CharacterComponent>;
  let mockAuthService: IAuthService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([{
          path: 'me/my-characters',
          component: UserCharactersComponent,
          canActivate: [IsLoggedInGuard]
        }]),
        MatDialogModule,
        MockModule(OAuthModule.forRoot()),
      ],
      declarations: [ CharacterComponent ],
      providers: [
        MockAuthService,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    mockAuthService = new MockAuthService();
    TestBed.overrideProvider(MatDialogRef, { useValue: {
      open: () => {
        return {
          afterClosed: () => from([true, true, true, true]),
        };
      },
    }});
    TestBed.overrideProvider(AuthService, { useValue : mockAuthService });
    TestBed.overrideProvider(CharactersService, MockProvider<CharactersService>(CharactersService, charactersServiceOverrides as any))
    fixture = TestBed.createComponent(CharacterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should start up initially in a state awaiting characters', () => {
    expect(component.IsAwaitingREST).toEqual(false);
    expect(component.LoadedCharacter as any).toEqual(exampleCharacter);
  });
  it('should correctly receive a request to delete', () => {
    component.DeleteCharacter();
    fixture.detectChanges();
    expect(component.IsLoading).toBeFalse();
  });
  it('should correctly receive a request to update', () => {
    component.UpdateCharacter();
    fixture.detectChanges();
    expect(component.IsLoading).toBeFalse();
  });
  it('should switch to a form when edit button is clicked', () => {
    fixture.nativeElement.querySelector('#dynastra-character-header-actions #edit-button')
      .click();
    fixture.detectChanges();
    expect(component.DisplayType).toEqual(CharacterDisplayType.Edit);
  });
  it('should display save and cancel buttons when the mode is displayed',() => {
    fixture.nativeElement.querySelector('#dynastra-character-header-actions #edit-button')
      .click();
    fixture.detectChanges();

    const editButton = fixture.nativeElement.querySelector('#dynastra-character-header-actions #edit-button');
    const deleteButton = fixture.nativeElement.querySelector('#dynastra-character-header-actions #delete-button');
    const saveButton = fixture.nativeElement.querySelector('#dynastra-character-header-actions #save-button');
    const cancelButton = fixture.nativeElement.querySelector('#dynastra-character-header-actions #cancel-button')

    expect(editButton).toEqual(null);
    expect(deleteButton).toEqual(null);
    expect(saveButton).not.toEqual(null);
    expect(cancelButton).not.toEqual(null);
  });
  it('should switch back to regular mode when cancel is clicked',() => {
    fixture.nativeElement.querySelector('#dynastra-character-header-actions #edit-button')
      .click();
    fixture.detectChanges();
    const cancelButton = fixture.nativeElement.querySelector('#dynastra-character-header-actions #cancel-button')
    cancelButton.click();
    fixture.detectChanges();
    expect(component.DisplayType).toEqual(CharacterDisplayType.View);
  });
  it('should return to view mode after save completes', () => {
    fixture.nativeElement.querySelector('#dynastra-character-header-actions #edit-button')
      .click();
    fixture.detectChanges();
    const saveButton = fixture.nativeElement.querySelector('#dynastra-character-header-actions #save-button')
    saveButton.click();
    expect(component.DisplayType).toEqual(CharacterDisplayType.View);
  });
  it('should prevent actions if loading', () => {
    component.LoadedCharacter = null as any;
    component.UpdateCharacter();
    fixture.detectChanges();
    expect(component.IsLoading).toEqual(true);
    component.DeleteCharacter();
    expect(component.IsLoading).toEqual(true);
    component.SaveCharacter();
    expect(component.IsLoading).toEqual(true);
  });
  it('should display a confirmation modal when delete is clicked', () => {
    fixture.nativeElement.querySelector('#dynastra-character-header-actions #delete-button')
      .click();
    fixture.detectChanges();
  });
});
