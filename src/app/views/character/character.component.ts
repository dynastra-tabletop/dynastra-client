import {Component, OnInit} from '@angular/core';
import { Router } from "@angular/router";
import {CharactersService} from "../../services/characters.service";
import {ICharacter} from "../../../lib/interfaces/Character.interfaces";
import {ActivatedRoute, ParamMap} from "@angular/router";
import {CharacterDisplayType} from "./character.enums";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {DialogConfirmComponent} from "../../components/dialog-confirm/dialog-confirm.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss'],
  providers: [
    CharactersService,
    MatDialog
  ]
})
export class CharacterComponent implements OnInit {
  /**
   * The character this component has loaded.
   */
  public LoadedCharacter!: ICharacter;

  /**
   * Is a REST response awaiting ?
   */
  public IsAwaitingREST: boolean | null = null;

  /**
   * The form for the editing
   */
  public EditingForm: FormGroup = new FormGroup({
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    about: new FormControl('', [
      Validators.required
    ]),
    imageUrl: new FormControl('', [
      Validators.required,
      Validators.pattern('[(http(s)?):\\/\\/(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)'),
    ]),
  });

  /**
   * Checks if the character display is loading the character.
   * @constructor
   */
  public get IsLoading(): boolean {
    return  this.EncounteredError !== true && (this.LoadedCharacter === null ||
            this.IsAwaitingREST ||
            this.CharacterId === null);
  }

  /**
   * The ID for the character passed to the page.
   */
  public CharacterId!: string;

  /**
   * Did the REST operation encounter an error?
   */
  public EncounteredError: boolean | null = null;

  /**
   * The way the component is displaying itself.
   */
  public DisplayType: CharacterDisplayType = CharacterDisplayType.View;

  public get name(): AbstractControl | null {
    return this.EditingForm.get('name');
  }

  public get about(): AbstractControl | null {
    return this.EditingForm.get('about');
  }

  public get imageUrl(): AbstractControl | null {
    return this.EditingForm.get('imageUrl');
  }

  /**
   * Edits the character.
   * @constructor
   */
  public EditCharacter(): void {
    this.loadForm()
    this.DisplayType = CharacterDisplayType.Edit;
  }

  /**
   * Loads the form with the data from the server.
   * @protected
   */
  protected loadForm() {
    (this.name as AbstractControl).setValue(this.LoadedCharacter.CharacterName);
    (this.about as AbstractControl).setValue(this.LoadedCharacter.CharacterAbout);
    (this.imageUrl as AbstractControl).setValue(this.LoadedCharacter.CharacterImgUrl);
  }

  /**
   * Standard multi-purpose function for ending loading a given character.
   * @param isError
   * @constructor
   */
  public EndLoading(isError: boolean = false, returnToView: boolean = false): () => void {
    return () => {
      if (isError) {
        this.EncounteredError = true;
      }

      if (returnToView) {
        this.DisplayType = CharacterDisplayType.View;
      }

      this.IsAwaitingREST = false;
    }
  }

  /**
   * Confirms that the character
   * @constructor
   */
  public ConfirmDeleteCharacter(): void {
    const dialogRef = this.Dialog.open(DialogConfirmComponent, {
      data: {
        Title: `Are you sure you want to delete your character: ${this.LoadedCharacter.CharacterName}?`,
        Description: `This action cannot be undone. The character will be deleted from all games.`,
        TypeToConfirm: true,
        TypeToConfirmText: this.LoadedCharacter.CharacterName,
        ConfirmText: 'Yes, delete this character',
        CancelText: 'No, don\'t delete!'
      }
    });
    dialogRef.afterClosed().subscribe((confirmDelete: boolean) => {
      if(confirmDelete) {
        this.DeleteCharacter();
      }
    });
  }

  /**
   * Deletes the character.
   * @constructor
   */
  public DeleteCharacter(): void {
    if (this.IsLoading) {
      return;
    }

    this.IsAwaitingREST = true;
    this.CharactersService.DeleteCharacter(this.LoadedCharacter.Id as string)
      .subscribe({
        next: () => {
          this.EndLoading();
          this.Router.navigate(['me/my-characters'])
            .then (() => {});
        },
        error: this.EndLoading(true),
        complete: this.EndLoading(),
      })
  }

  /**
   * Updates the character based on the form.
   * @constructor
   */
  public UpdateCharacter(): void {
    if(this.IsLoading) {
      return;
    }

    this.IsAwaitingREST = true;
    this.CharactersService.UpdateCharacter(this.LoadedCharacter.Id, this.LoadedCharacter)
      .subscribe({
        next: this.EndLoading(true, true),
        error: this.EndLoading(true),
        complete: this.EndLoading()
      });

  }

  /**
   * Gets the character by a given ID.
   * @param characterId
   * @protected
   */
  protected getCharacterFromId(characterId: string): void {
    this.IsAwaitingREST = true;
    this.CharactersService.GetCharacter(characterId)
      .subscribe({
        next: (character: ICharacter) => {
          this.LoadedCharacter = character;
          this.IsAwaitingREST = false;
        },
        error: this.EndLoading(true),
        complete: this.EndLoading(),
      });
  }

  /**
   * Returns to view mode.
   * @constructor
   */
  public ReturnToView() {
    this.DisplayType = CharacterDisplayType.View;
  }

  /**
   * Saves the character.
   * @constructor
   */
  public SaveCharacter() {
    if (this.EditingForm.invalid) {
      return;
    }

    this.populateCharacterFromForm();
    this.UpdateCharacter();
  }

  /**
   * Populates the character data from the form
   * @protected
   */
  protected populateCharacterFromForm() {
    this.LoadedCharacter.CharacterName = (this.name as AbstractControl).value;
    this.LoadedCharacter.CharacterAbout = (this.about as AbstractControl).value;
    this.LoadedCharacter.CharacterImgUrl = (this.imageUrl as AbstractControl).value;
  }

  /**
   * Called on the init of component.
   */
  ngOnInit(): void {
    this.ActivatedRoute.paramMap.subscribe((params: ParamMap) => {
      const id = params.get('character-id') as string;
      this.getCharacterFromId(id);
    });
  }

  /**
   * Creates a new instance of the character component.
   * @param CharactersService
   */
  constructor(
    protected readonly CharactersService: CharactersService,
    protected readonly ActivatedRoute: ActivatedRoute,
    protected readonly Dialog: MatDialog,
    protected readonly Router: Router
  ) {

  }
}
