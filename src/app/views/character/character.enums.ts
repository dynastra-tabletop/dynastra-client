export enum CharacterDisplayType {
  View = 'VIEW_CHARACTER',
  Edit = 'EDIT_CHARACTER',
}
