import { Component, OnInit } from '@angular/core';
import { finalize, tap } from 'rxjs/operators';
import { RolePlayingGameService } from 'src/app/services/role-playing-game.service';
import { IRolePlayingGame } from 'src/lib/interfaces/RolePlayingGame.interfaces';
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {
  RolePlayingGamesCreateModalComponent
} from "../../components/role-playing-games-create-modal/role-playing-games-create-modal.component";

@Component({
  selector: 'app-role-playing-games',
  templateUrl: './role-playing-games.component.html',
  styleUrls: ['./role-playing-games.component.scss'],
  providers: [
    RolePlayingGameService,
    MatDialog,
  ]
})
export class RolePlayingGamesComponent implements OnInit {
  /**
   * If the games are being loaded currently.
   *
   * @type {boolean}
   * @memberof RolePlayingGamesComponent
   */
  public LoadingGames: boolean | null = null;

  /**
   * The reference for the dialog, kept so it can be destroyed later.
   * @protected
   */
  protected DialogRef!: MatDialogRef<any>;

  /**
   * If the games are currently loading or not.
   * @constructor
   */
  public get IsLoading() {
    return  this.LoadingGames !== null &&
            this.LoadingGames;
  }

  /**
   * The games are currently being loaded.
   *
   * @type {Array<IRolePlayingGame>}
   * @memberof RolePlayingGamesComponent
   */
  public RolePlayingGames: Array<IRolePlayingGame> = [];

  /**
   * Callback used when the user clicks "create game" FAB.
   */
  public clickCreateGame() {
    this.DialogRef = this.Dialog.open(RolePlayingGamesCreateModalComponent);
    this.DialogRef!.afterClosed().subscribe({
      next: this.getRolePlayingGames.bind(this),
      error: this.getRolePlayingGames.bind(this),
    });
  }

  /**
   * Gets all the role-playing games from the server.
   * NOTE: When actions are added in, this will change.
   * @protected
   */
  protected getRolePlayingGames(): void {
    this.LoadingGames = true;
    this.RolePlayingGame
      .GetRolePlayingGames()
      .pipe(
        finalize(() => {
          this.LoadingGames = false;
        })
      ).subscribe((games: Array<IRolePlayingGame>) => {
      this.RolePlayingGames = games;
    });
  }

  /**
   * Implements OnInit.
   *
   * @memberof RolePlayingGamesComponent
   */
  ngOnInit(): void {
    this.getRolePlayingGames();
  }

  /**
   * The display items have changed, time to reload.
   * @param $event
   */
  public onDisplayItemsChanged($event: boolean) {
    this.getRolePlayingGames();
  }

  /**
   * Creates an instance of RolePlayingGamesComponent.
   * @param {RolePlayingGameService} RolePlayingGame
   * @memberof RolePlayingGamesComponent
   */
  constructor(
    protected readonly Dialog: MatDialog,
    protected readonly RolePlayingGame: RolePlayingGameService,
  ) {

  }


}
