import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RolePlayingGamesComponent } from './role-playing-games.component';
import {MatDialog, MatDialogModule} from "@angular/material/dialog";
import {HttpClientModule} from "@angular/common/http";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {MockModule, MockProvider} from "ng-mocks";
import {OAuthModule} from "angular-oauth2-oidc";
import {RouterTestingModule} from "@angular/router/testing";
import {MatButtonModule} from "@angular/material/button";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {
  RolePlayingGamesSearchComponent
} from "../../components/role-playing-games-search/role-playing-games-search.component";
import {
  RolePlayingGamesListDisplayComponent
} from "../../components/role-playing-games-list-display/role-playing-games-list-display.component";
import {AuthService} from "../../services/auth.service";
import {RolePlayingGameService} from "../../services/role-playing-game.service";
import {IRolePlayingGame} from "../../../lib/interfaces/RolePlayingGame.interfaces";
import {from, Observable, sample} from "rxjs";

describe('RolePlayingGamesComponent', () => {
  let component: RolePlayingGamesComponent;
  let fixture: ComponentFixture<RolePlayingGamesComponent>;
  let sampleGameResults: Array<IRolePlayingGame> = [{
    Id: '',
    Name: 'Test 1',
    Description: 'Game 1',
    CreatedByUser: '',
    ImageUrl: '',
    Characters: [],
    Users: []
  }, {
    Id: '',
    Name: 'Test 2',
    Description: 'Game 2',
    CreatedByUser: '',
    ImageUrl: '',
    Characters: [],
    Users: []
  }];

  beforeEach(async () => {
    TestBed.overrideProvider(MatDialog, {
      useValue: {
        open: () => ({
          afterClosed: () => new Observable((obs) => {
            obs.next(true);
            obs.error(true);
          }),
        }),
      }
    })
    TestBed.overrideProvider(AuthService, {
      useValue: {
        events: new Observable((obs) => {
          obs.next({
            type: 'session_error'
          });
          obs.next({
            type: 'invalid_nonce_in_state'
          })
        })
      }
    });
    TestBed.overrideProvider(RolePlayingGameService, {useValue: {
      GetRolePlayingGames: () => new Observable((obs) => {
        obs.next(sampleGameResults);
        obs.complete();
      }),
    }})
    await TestBed.configureTestingModule({
      imports: [
        MatButtonModule,
        MatButtonToggleModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        HttpClientTestingModule,
        MockModule(OAuthModule.forRoot()),
        RouterTestingModule,
      ],
      providers: [
        MockProvider<RolePlayingGameService>(RolePlayingGameService)
      ],
      declarations: [ RolePlayingGamesComponent, RolePlayingGamesSearchComponent, RolePlayingGamesListDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RolePlayingGamesComponent);
    component = fixture.componentInstance;
    component.RolePlayingGames = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should retrieve role-playing games when the component initialises', () => {
    expect(component.LoadingGames).toBeFalse();
    expect(component.IsLoading).toBeFalse();
    expect(component.RolePlayingGames).toEqual(sampleGameResults);
  });
  it('should open a dialog when the "create" FAB is clicked and maintain state', () => {
    component.clickCreateGame();
    expect(component.IsLoading).toBeFalse();
    expect(component.RolePlayingGames).toEqual(sampleGameResults);
  });
  it('should load role playing games when told items have been updated', () => {
    component.onDisplayItemsChanged(true);
    expect(component.RolePlayingGames.length).toBeGreaterThan(0);
  })
});
