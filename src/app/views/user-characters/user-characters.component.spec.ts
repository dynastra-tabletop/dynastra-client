import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCharactersComponent } from './user-characters.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {MatDialogModule} from "@angular/material/dialog";
import {MockModule, MockProvider} from "ng-mocks";
import {OAuthModule} from "angular-oauth2-oidc";
import {RouterTestingModule} from "@angular/router/testing";
import {AuthService, IAuthService} from "../../services/auth.service";
import {MockAuthService} from "../../../../spec/MockAuthService";
import {CharactersService} from "../../services/characters.service";
import {from} from "rxjs";
import {ICharacter} from "../../../lib/interfaces/Character.interfaces";

const exampleCharacter: ICharacter = {
  Id: 'test',
  UserId: 'test',
  CharacterName: 'test',
  CharacterAbout: 'test',
  CharacterImgUrl: 'https://localhost/lolz.png',
  IsPublic: true
};

const charactersServiceOverrides = {
  DeleteCharacter: (id: string) => from([1]),
  UpdateCharacter: (id: string) => from([true]),
  GetCharacter: (id: string) => from([exampleCharacter]),
  GetCharactersForUser: () => from([[exampleCharacter, exampleCharacter]])
};

describe('UserCharactersComponent', () => {
  let component: UserCharactersComponent;
  let fixture: ComponentFixture<UserCharactersComponent>;
  let mockAuthService: IAuthService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatDialogModule,
        RouterTestingModule,
        MockModule(OAuthModule.forRoot()),
      ],
      declarations: [ UserCharactersComponent ],
      providers: [MockAuthService, ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    mockAuthService = new MockAuthService();
    TestBed.overrideProvider(AuthService, { useValue : mockAuthService });
    TestBed.overrideProvider(CharactersService, MockProvider<CharactersService>(CharactersService, charactersServiceOverrides as any))
    fixture = TestBed.createComponent(UserCharactersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
