import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {ICharacter} from "../../../lib/interfaces/Character.interfaces";
import {CharactersService} from "../../services/characters.service";

@Component({
  selector: 'app-user-characters',
  templateUrl: './user-characters.component.html',
  styleUrls: ['./user-characters.component.scss'],
  providers: [
    CharactersService,
  ]
})
export class UserCharactersComponent implements OnInit {

  /**
   * The characters retrieved from the server.
   */
  public Characters: Array<ICharacter> = [];

  /**
   * No characters were found.
   */
  private CharsFound: boolean | null = null;

  /**
   * Were any characters found after loading from the rest request?
   * @constructor
   */
  public get AnyCharsFoundAfterLoad(): boolean {
    return this.CharsFound !== null && this.CharsFound === true;
  }

  /**
   * Is called when the component has been initialised.
   */
  ngOnInit(): void {
    this.CharactersService.GetCharactersForUser()
      .subscribe({
        next: (characters: Array<ICharacter> ) => {
          this.Characters = characters;
          this.CharsFound = characters.length > 0;
        }
      })
  }

  /**
   * Creates a new instance of the user characters component.
   * @param CharactersService
   */
  constructor(
    protected readonly CharactersService: CharactersService
  ) { }
}
