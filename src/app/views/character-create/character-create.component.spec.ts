import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterCreateComponent } from './character-create.component';
import {MockProvider} from "ng-mocks";
import {CharactersService} from "../../services/characters.service";
import {from} from "rxjs";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {AuthService} from "../../services/auth.service";
import {RouterTestingModule} from "@angular/router/testing";
import {ICharacter} from "../../../lib/interfaces/Character.interfaces";
import {CharacterComponent} from "../character/character.component";

const exampleCharacter: ICharacter = {
  Id: 'test',
  UserId: 'test',
  CharacterName: 'test',
  CharacterAbout: 'test',
  CharacterImgUrl: 'https://localhost/lolz.png',
  IsPublic: true
};

const charactersServiceOverrides = {
  CreateCharacter: (character: ICharacter) => from([exampleCharacter]),
};

describe('CharacterCreateComponent', () => {
  let component: CharacterCreateComponent;
  let fixture: ComponentFixture<CharacterCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([{
          path: 'me/my-characters',
          component: CharacterComponent
        }]),
      ],
      declarations: [ CharacterCreateComponent ],
      providers: [MockProvider<CharactersService>(CharactersService), MockProvider<AuthService>(AuthService)]
    })
    .compileComponents();
  });

  beforeEach(() => {
    TestBed.overrideProvider(CharactersService, MockProvider<CharactersService>(CharactersService, charactersServiceOverrides as any))
    fixture = TestBed.createComponent(CharacterCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should correctly populate the character object when the form is updated', () => {
    component.CreateForm?.get("name")?.setValue('test');
    component.CreateForm?.get("about")?.setValue('test');
    component.CreateForm?.get("imageUrl")?.setValue('https://i.playboard.app/p/AATXAJxWJ4Sgc8tnoa5WAcF36bDQLNfXDNoGmqEPsetmpA/default.jpg');
    expect(component.CreateForm.valid).toBeTruthy();
    component.CreateCharacter();
    fixture.detectChanges();
    expect(component.IsLoading).toBeFalse();
    expect(component.CreatingCharacter.CharacterName).toEqual('test');
    expect(component.CreatingCharacter.CharacterAbout).toEqual('test');
  });
  it('should not allow creation if the form is invalid', () => {
    component.CreateForm?.get("name")?.setValue('');
    component.CreateForm?.get("about")?.setValue('');
    component.CreateForm?.get("imageUrl")?.setValue('z1111lolz!!!....');
    fixture.detectChanges();
    component.CreateCharacter();
    expect(component.CreatingCharacter.CharacterAbout).toEqual('');
    expect(component.CreatingCharacter.CharacterName).toEqual('');
  });
});
