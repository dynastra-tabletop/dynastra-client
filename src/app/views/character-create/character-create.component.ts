import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {CharactersService} from "../../services/characters.service";
import {ICharacter} from "../../../lib/interfaces/Character.interfaces";
import { Router } from "@angular/router";

@Component({
  selector: 'app-character-create',
  templateUrl: './character-create.component.html',
  styleUrls: ['./character-create.component.scss']
})
export class CharacterCreateComponent {

  /**
   * The character being created in this component.
   */
  public CreatingCharacter: ICharacter = {
    Id: '',
    UserId: '',
    CharacterName: '',
    CharacterAbout: '',
    CharacterImgUrl: '',
    IsPublic: true
  };

  protected IsLoadingREST: boolean = false;

  /**
   * Checks if the save is loading.
   * @constructor
   */
  public get IsLoading(): boolean {
    return this.IsLoadingREST;
  }

  /**
   * Form for creating the new character.
   */
  public CreateForm: FormGroup = new FormGroup({
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    about: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
    ]),
    imageUrl: new FormControl('', [
      Validators.required,
      Validators.pattern('[(http(s)?):\\/\\/(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)'),
    ]),
  });

  /**
   * The name from the form.
   */
  public get name(): AbstractControl | null {
    return this.CreateForm.get('name');
  }

  /**
   * About the character you're making
   */
  public get about(): AbstractControl | null {
    return this.CreateForm.get('about');
  }

  /**
   * The URL for the image for this character.
   */
  public get imageUrl(): AbstractControl | null {
    return this.CreateForm.get('imageUrl');
  }

  /**
   * Single-purpose callback to mark that loading of elements is done.
   * @protected
   */
  protected updateLoading(error: boolean = false) {
    return () => {
      this.IsLoadingREST = false;

      if (!error) {
        this.Router.navigate(['/me/my-characters'])
          .then(() => {});
      }
    }
  }

  /**
   * Updates the object before sending it.
   * @protected
   */
  protected updateObjectFromForm() {
    this.CreatingCharacter.CharacterName = this.name?.value;
    this.CreatingCharacter.CharacterAbout = this.about?.value;
    this.CreatingCharacter.CharacterImgUrl = this.imageUrl?.value;
  }

  /**
   * Creates the character
   * @constructor
   */
  public CreateCharacter() {
    if (this.CreateForm.invalid) {
      return;
    }
    this.updateObjectFromForm();
    this.IsLoadingREST = true;
    this.CharactersService.CreateCharacter(this.CreatingCharacter)
      .subscribe({
        next: this.updateLoading(),
        error: this.updateLoading(true),
        complete: this.updateLoading(),
      });
  }

  /**
   * Creates a new instance of the character create component.
   * @param CharactersService
   */
  constructor(
    protected readonly CharactersService: CharactersService,
    protected readonly Router: Router,
  ) { }

}
