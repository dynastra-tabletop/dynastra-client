import {Component, Input, OnInit} from '@angular/core';
import {ICharacter} from "../../../lib/interfaces/Character.interfaces";

@Component({
  selector: 'app-role-playing-game-character',
  templateUrl: './role-playing-game-character.component.html',
  styleUrls: ['./role-playing-game-character.component.scss']
})
export class RolePlayingGameCharacterComponent {

  /**
   * A single character in this component.
   */
  @Input()
  public Character!: ICharacter;
}
