import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RolePlayingGameCharacterComponent } from './role-playing-game-character.component';
import {RouterTestingModule} from "@angular/router/testing";

describe('RolePlayingGameCharacterComponent', () => {
  let component: RolePlayingGameCharacterComponent;
  let fixture: ComponentFixture<RolePlayingGameCharacterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [ RolePlayingGameCharacterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RolePlayingGameCharacterComponent);
    component = fixture.componentInstance;
    component.Character = {
      CharacterAbout: "",
      CharacterImgUrl: "",
      CharacterName: "",
      Id: "",
      IsPublic: false,
      UserId: ""
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
