
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { KonvaComponent } from 'ng2-konva';
import { Observable, of, Subject } from 'rxjs';
import Konva from "konva";
import { GameBoardService } from "../../services/game-board.service";
import {IGameBoardSettings} from "../../../lib/interfaces/RolePlayingGame.interfaces";

@Component({
  selector: 'app-game-board-two-dimensional',
  templateUrl: './game-board-two-dimensional.component.html',
  styleUrls: ['./game-board-two-dimensional.component.scss']
})
export class GameBoardTwoDimensionalComponent implements AfterViewInit {
  /**
   * The game board's reference.
   */
	@ViewChild('gameBoard')
	public gameBoard: KonvaComponent | null = null;

  /**
   * The grid layer's reference.
   */
  @ViewChild('gridLayer')
  public gridLayer: KonvaComponent | null = null;

  /**
   * The image layer.
   */
  @ViewChild('imageLayer')
  public imageLayer: KonvaComponent | null = null;

	/**
	 * The level of the zoom applied to the canvas context.
	 *
	 * @protected
	 * @type {number}
	 * @memberof GameBoardComponent
	 */
	protected ZoomLevel: number = 1;

  /**
   * The number of pixels width between the grid.
   * @protected
   */
  protected spacingWidth: number = 200;

	/**
	 * The offset for the position of the camera.
	 *
	 * @protected
	 * @type {*}
	 * @memberof GameBoardComponent
	 */
	protected CameraOffset: any = { x: window.innerWidth/2, y: window.innerHeight/2 };

	/**
	 * The context for the canvas element which is going to be panned and zoomed.
	 *
	 * @protected
	 * @type {(CanvasRenderingContext2D | null)}
	 * @memberof GameBoardComponent
	 */
	protected CanvasContext: CanvasRenderingContext2D | null = null;
  protected MaxScale: number = 1.2;
  protected MinScale: number = 0.025;
  protected Settings!: Partial<IGameBoardSettings>;

	/**
	 * Configuration for the konva stage, loaded async via observables.
	 *
	 * @type {Observable<any>}
	 * @memberof GameBoardComponent
	 */
	public configStage: Observable<any> = of({
		width: 2000,
		height: 2000
	});

  public currentScale: { x: number; y: number; } = { x: 1, y: 1};
  public stageReference: any;

	/**
	 * Observable for the images being displayed.
	 *
	 * @type {Subject<any>}
	 * @memberof GameBoardComponent
	 */
	public configImages: Subject<any> = new Subject<any>();

  /**
   * Called when the scale is changed.
   * @param event
   */
	public onScaleChange(event: any) {
		console.log(event);
	}

	/**
	 * Loads a given image into the game board.
	 *
	 * @protected
	 * @param {string} url
	 * @memberof GameBoardComponent
	 */
	protected loadImageIntoGameBoard(url: string): void {
		const image: any = new Image();
		image.src = url;
		image.onload = () => {
			this.imageLayer?.getStage().add(
        new Konva.Image({
          x: 0,
          y: 0,
          image,
          draggable: true
        })
      );
		};
	}

  /**
   * Provides an un-scaled version of the given measurement.
   * @param stat
   * @protected
   */
  protected unScale(stat: number) {
    return stat / this.stageReference.scaleX();
  }

  /**
   * Draws the game grid over the konva board.
   * @protected
   */
  protected drawGameGrid(): void {
    this.gridLayer?.getStage().clear();
    this.gridLayer?.getStage().destroyChildren();

    // Largely adapted from (https://longviewcoder.com/2021/12/08/konva-a-better-grid/). Thanks Longview coder!
    // p.s. I still think the code was a mess :D
    const height = this.stageReference?.getStage().height();
    const width = this.stageReference?.getStage().width();
    const stepSize = height / 10;

    const stageRect = {
      x1: 0,
      y1: 0,
      x2: this.stageReference?.getStage().width(),
      y2: this.stageReference?.getStage().height(),
      offset: {
        x: this.unScale(this.stageReference.position().x),
        y: this.unScale(this.stageReference.position().y),
      }
    };

    const viewRect = {
      x1: -stageRect.offset.x,
      y1: -stageRect.offset.y,
      x2: this.unScale(width) - stageRect.offset.x,
      y2: this.unScale(height) - stageRect.offset.y
    };

    const gridOffset = {
      x: Math.ceil(this.unScale(this.stageReference.position().x) / stepSize) * stepSize,
      y: Math.ceil(this.unScale(this.stageReference.position().y) / stepSize) * stepSize,
    };

    const gridRect = {
      x1: -gridOffset.x,
      y1: -gridOffset.y,
      x2: this.unScale(width) - gridOffset.x + stepSize,
      y2: this.unScale(height) - gridOffset.y + stepSize
    };

    const gridFullRect = {
      x1: Math.min(stageRect.x1, gridRect.x1),
      y1: Math.min(stageRect.y1, gridRect.y1),
      x2: Math.max(stageRect.x2, gridRect.x2),
      y2: Math.max(stageRect.y2, gridRect.y2)
    };

    // set clip function to stop leaking lines into non-viewable space.
    this.gridLayer?.getStage().clip({
      x: viewRect.x1,
      y: viewRect.y1,
      width: viewRect.x2 - viewRect.x1,
      height: viewRect.y2 - viewRect.y1
    });

    let fullRect = gridFullRect;
    const xSize = (fullRect.x2 - fullRect.x1);
    const ySize = (fullRect.y2 - fullRect.y1);
    const xSteps = Math.round(xSize/ stepSize);
    const ySteps = Math.round(ySize / stepSize);

    const stroke = this.Settings.gridSettings?.gridLineColor || '#000000';
    const strokeWidth = this.Settings.gridSettings?.gridLineWidth || 10;
    const lineStyle = this.Settings.gridSettings?.gridLineStyle || 'solid';

    for (let i = 0; i <= xSteps; i++) {
      const lineToAdd = new Konva.Line({
        x: fullRect.x1 + i * stepSize,
        y: fullRect.y1,
        fillEnabled: false,
        shadowForStrokeEnabled: false,
        points: [0, 0, 0, ySize],
        stroke,
        strokeWidth,
      });

      if (lineStyle === 'dotted') {
        lineToAdd.dash([2, 30]);
      } else if (lineStyle === 'dashed') {
        lineToAdd.dash([10, 10]);
      }

      this.gridLayer?.getStage().add(lineToAdd);
    }

    for (let i = 0; i <= ySteps; i++) {
      const lineToAdd = new Konva.Line({
        x: fullRect.x1,
        y: fullRect.y1 + i * stepSize,
        fillEnabled: false,
        shadowForStrokeEnabled: false,
        points: [0, 0, xSize, 0],
        stroke,
        strokeWidth,
      });

      lineToAdd.alpha(1);

      if (lineStyle === 'dotted') {
        lineToAdd.dash([2, 30]);
      } else if (lineStyle === 'dashed') {
        lineToAdd.dash([10, 10]);
      }

      this.gridLayer?.getStage().add(lineToAdd);
    }

    // Draw a border around the viewport
    this.gridLayer?.getStage().add(
      new Konva.Rect({
        x: viewRect.x1 + 2,
        y: viewRect.y1 + 2,
        width: viewRect.x2 - viewRect.x1 - 4,
        height: viewRect.y2 - viewRect.y1 - 4,
        strokeWidth: 1,
        stroke: 'red'
      }))

    this.gridLayer?.getStage().batchDraw();
  }

  protected updateDraggableState(): void {
    this.gridLayer?.getStage().setZIndex(9);
    this.imageLayer?.getStage().setZIndex(1);
  }

  /**
   * Implements ngAfterViewInit.
   *
   * @memberof GameBoardComponent
   */
  public ngAfterViewInit(): void {
    // TODO: Remove this when 2D game board is wired up
    if (typeof this.gameBoard?.getStage !==  'function') {
      return;
    }

    // Get the reference for the konva stage and hoist dat boi.
    this.stageReference = this.gameBoard?.getStage();

    // Now draw grid.
    this.drawGameGrid();

    const scaleBy = 1.4;
    this.stageReference.on("wheel", (e: any) => {
      e.evt.preventDefault();
      const oldScale = this.stageReference.scaleX();
      const pointer = this.stageReference.getPointerPosition();
      const mousePointTo = {
        x: (pointer.x - this.stageReference.x()) / oldScale,
        y: (pointer.y - this.stageReference.y()) / oldScale,
      };

      let newScale =
        e.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;

      if (newScale > this.MaxScale) {
        newScale = this.MaxScale;
      } else if (newScale < this.MinScale) {
        newScale = this.MinScale;
      }

      this.currentScale = { x: newScale, y: newScale};
      this.stageReference.scale(this.currentScale);

      const newPos = {
        x: pointer.x - mousePointTo.x * newScale,
        y: pointer.y - mousePointTo.y * newScale,
      };

      this.stageReference.position(newPos);
      this.drawGameGrid();
    });

    this.updateDraggableState();
    this.loadImageIntoGameBoard("http://localhost:4200/assets/map.jpeg");
  }

  public OnStageDoubleTap(event: any): void {
    this.stageReference.scale({ x: this.MaxScale, y: this.MaxScale });
    this.stageReference.position({ x: 0, y: 0 });
    this.drawGameGrid();
  }

  /**
   * The scale is changing.
   * @param event
   * @constructor
   */
  public OnStageScaleChange(event: any) {
    console.log()
  }

  /**
   * Initialises the component.
   * @protected
   */
  protected init(): void {
    this.GameBoardService.onSettingsChanged()
      .subscribe({
        next: (settings: IGameBoardSettings) => {
          this.Settings = settings;
          this.drawGameGrid();
        }
      })
  }

  /**
   * Creates a new instance of a two-dimensional game board.
   * @param GameBoardService
   */
  constructor(
    protected readonly GameBoardService: GameBoardService
  ) {
    this.init();
  }
}
