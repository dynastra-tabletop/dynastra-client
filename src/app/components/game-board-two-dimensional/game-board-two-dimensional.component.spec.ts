import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GameBoardTwoDimensionalComponent } from './game-board-two-dimensional.component';
import {KonvaComponent, KonvaModule} from "ng2-konva";
import {MockComponent, MockModule} from "ng-mocks";

describe('GameBoardTwoDimensionalComponent', () => {
  let component: GameBoardTwoDimensionalComponent;
  let fixture: ComponentFixture<GameBoardTwoDimensionalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        GameBoardTwoDimensionalComponent,
      ],
      imports: [
        MockModule(KonvaModule)
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardTwoDimensionalComponent);
    component = fixture.componentInstance;
    component.gameBoard = {
      getStage: () => ({
        height: () => 2000,
        width: () => 2000,
        setZIndex: () => {},
        clear: () => {},
        destroyChildren: () => {}
      }),
    } as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should skip initialisation if the game board is not initialises', () => {
    expect(component.stageReference).toEqual(undefined);
  });
});
