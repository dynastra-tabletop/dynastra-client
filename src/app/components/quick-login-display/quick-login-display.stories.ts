import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { Meta, Story, moduleMetadata } from '@storybook/angular';
import { OAuthModule } from 'angular-oauth2-oidc';
import { KonvaModule } from 'ng2-konva';
import { AuthService } from 'src/app/services/auth.service';
import { LoggedInUserDisplayComponent } from '../logged-in-user-display/logged-in-user-display.component';
import { LoggedOutUserDisplayComponent } from '../logged-out-user-display/logged-out-user-display.component';
import { QuickLoginDisplayComponent } from './quick-login-display.component';

export default {
  title: 'Quick Login Display',
  component: QuickLoginDisplayComponent,
  decorators: [
    moduleMetadata({
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
        AuthService,
      ],
      declarations: [
        LoggedInUserDisplayComponent,
        LoggedOutUserDisplayComponent
      ],
      imports: [
        BrowserModule,
        RouterModule.forRoot([], { useHash: true }),
        KonvaModule,
        BrowserAnimationsModule,
        MatSidenavModule,
        MatExpansionModule,
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        MatMenuModule,
        HttpClientModule,
        MatProgressSpinnerModule,
        OAuthModule.forRoot(),
      ],
    }),
  ]
} as Meta;

const Template: Story<QuickLoginDisplayComponent> = (args) => ({
  props: args,
});

export const ShowLoginWhenNotLoggedIn = Template.bind({});
ShowLoginWhenNotLoggedIn.args = {
  AuthService: {
    isLoggedIn: false,
    loggedInUser: {
      username: "Test",
      email: "test@dynastra.vtt"
    }
  } as unknown as AuthService,
};

export const ShowLogoutWhenLoggedIn = Template.bind({});
ShowLogoutWhenLoggedIn.args = {
  AuthService: {
    isLoggedIn: true,
    loggedInUser: {
      username: "Test",
      email: "test@dynastra.vtt"
    }
  } as unknown as AuthService,
};