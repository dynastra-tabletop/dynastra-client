import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import noop from "lodash.noop";

@Component({
  selector: 'app-quick-login-display',
  templateUrl: './quick-login-display.component.html',
  styleUrls: ['./quick-login-display.component.scss']
})
export class QuickLoginDisplayComponent {
  /**
   * Is the user currently logged into the system?
   *
   * @readonly
   * @type {boolean}
   * @memberof QuickLoginDisplayComponent
   */
  public get IsLoggedIn(): boolean { 
    return this.AuthService.isLoggedIn;
  }

  /**
   * The username retrieved from the OIDC user context.
   *
   * @readonly
   * @type {string}
   * @memberof QuickLoginDisplayComponent
   */
  public get Username(): string { 
    return this.AuthService.loggedInUser.username || "";
  }

  /**
   * The e-mail address retrieved from the OIDC user context.
   *
   * @readonly
   * @type {string}
   * @memberof QuickLoginDisplayComponent
   */
  public get EmailAddress(): string { 
    return this.AuthService.loggedInUser.email || "";
  }

  /**
   * The redirect URL for the issuer when we have user identity claims.
   *
   * @readonly
   * @type {string}
   * @memberof QuickLoginDisplayComponent
   */
  public get RedirectURL(): string {
    const redirect: string = this.AuthService.getIssuerAccountRedirect();
    return `${redirect}`;
  }

  /**
   * Logs the user out (passed from the user-logged-in component)
   *
   * @memberof QuickLoginDisplayComponent
   */
  public onLogoutReceived(): void {
    this.AuthService.logout();
  }

  /**
   * Logs the user in (passed from the user-logged-in component)
   *
   * @memberof QuickLoginDisplayComponent
   */
  public onLoginReceived(): void {
    this.AuthService.initAuth()
      .then(noop);
  }

  /**
   * Creates an instance of QuickLoginDisplayComponent.
   * @param {AuthService} AuthService
   * @memberof QuickLoginDisplayComponent
   */
  constructor(public readonly AuthService: AuthService) {}
}
