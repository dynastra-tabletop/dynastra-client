import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { RouterTestingModule } from '@angular/router/testing';
import { OAuthModule, OAuthService } from 'angular-oauth2-oidc';
import { GravatarModule } from 'ngx-gravatar';
import { AuthService, IAuthService } from 'src/app/services/auth.service';
import { ILoggedInUser } from 'src/lib/user-login.interfaces';
import { LoggedInUserDisplayComponent } from '../logged-in-user-display/logged-in-user-display.component';
import { LoggedOutUserDisplayComponent } from '../logged-out-user-display/logged-out-user-display.component';
import { QuickLoginDisplayComponent } from './quick-login-display.component';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MockModule} from "ng-mocks";

/**
 * A mock authentication service which is used to test the auth service for the quick login display.
 *
 * @class MockAuthService
 * @implements {IAuthService}
 */
class MockAuthService implements IAuthService {
  public isAwaitingAuthentication: boolean = false;
  public failedToContactAuthentication: boolean = false;
  public profile?: any = {};
  public isLoggedIn: boolean = true;
  public getIssuerAccountRedirect(): string {
      return "#";
  }
  public loggedInUser: Partial<ILoggedInUser> = {
    username: "TestUserNameWhichIsLong",
    email: "someverylongemail@dynastra.vtt",
  };
  public loggedInUserContext: any = {};
  initAuth(initialURL?: string): Promise<void> {
    return Promise.resolve();
  }
  logout(): void {
    return;
  }
}

describe('QuickLoginDisplayComponent', () => {
  let component: QuickLoginDisplayComponent;
  let fixture: ComponentFixture<QuickLoginDisplayComponent>;
  let mockAuthService: IAuthService;
  let loader: HarnessLoader;

  beforeEach(async () => {
    mockAuthService = new MockAuthService();
    TestBed.overrideProvider(AuthService, { useValue : mockAuthService });
    await TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        GravatarModule,
        MatMenuModule,
        MatIconModule,
        HttpClientModule,
        RouterTestingModule,
        MockModule(OAuthModule.forRoot()),
      ],
      declarations: [
        QuickLoginDisplayComponent,
        LoggedInUserDisplayComponent,
        LoggedOutUserDisplayComponent
      ],
      providers: [AuthService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickLoginDisplayComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display empty parts if the contract is broken and properties are missing', () => {
    mockAuthService.loggedInUser = {};
    fixture.detectChanges();
    expect(component.EmailAddress).toEqual("");
    expect(component.Username).toEqual("");
  });

  it('when the user is logged in, it should display the logged in user component', () => {
    const loggedInComponent = fixture.nativeElement.querySelector("#dynastra-logged-in-user-display-wrapper");
    const loggedOutComponent = fixture.nativeElement.querySelector("#dynastra-logged-out-user-display-wrapper");
    expect(loggedInComponent).not.toBeNull();
    expect(loggedOutComponent).toBeNull();
  });
  it('when the user is logged out, it should display the logged out user component', () => {
    mockAuthService.isLoggedIn = false;
    fixture.detectChanges();
    mockAuthService.isLoggedIn = false;
    const loggedInComponent = fixture.nativeElement.querySelector("#dynastra-logged-in-user-display-wrapper");
    const loggedOutComponent = fixture.nativeElement.querySelector("#dynastra-logged-out-user-display-wrapper");
    expect(loggedInComponent).toBeNull();
    expect(loggedOutComponent).not.toBeNull();
  });
  it('when the user is logged out and requests to log in, they should be logged in', async () => {
    // Configure the testing module to use a logged out auth service instance.
    mockAuthService.isLoggedIn = false;
    const logInSpy = spyOn(mockAuthService, "initAuth").and.returnValue(Promise.resolve());

    // Update the element with the new mock auth service change.
    fixture.detectChanges();

    const elem = fixture.nativeElement
      .querySelector("#dynastra-logged-out-user-display-logout");

    elem.click();
    expect(logInSpy).toHaveBeenCalled();
  });
  it('should redirect to the account console of keycloak (hardcoded for now)', () => {
    mockAuthService.isLoggedIn = true;
    const issuerSpy = spyOn(mockAuthService, "getIssuerAccountRedirect").and.returnValue("#");
    fixture.detectChanges();

    const elem = fixture.nativeElement
      .querySelector("#dynastra-logged-in-user-display-name-link");
    const endOfHref = elem.href.slice(-1);
    expect(endOfHref).toEqual("e");
  });
});
