import { Component, Input, OnInit } from '@angular/core';
import { IDynastraChatEvent, IDynastraCompactChatEvent } from 'src/lib/interfaces/Chat.interfaces';

@Component({
  selector: 'app-game-board-chat-item',
  templateUrl: './game-board-chat-item.component.html',
  styleUrls: ['./game-board-chat-item.component.scss']
})
export class GameBoardChatItemComponent implements OnInit {
  /**
   * Classes that get appended on identification.
   *
   * @type {Array<string>}
   * @memberof GameBoardChatItemComponent
   */
  public ChatClasses: Array<string> = [];

  /**
   * The event we're dealing with.
   *
   * @type {IDynastraCompactChatEvent}
   * @memberof GameBoardChatItemComponent
   */
  @Input()
  public ChatEvent!: IDynastraCompactChatEvent;

  /**
   * Identifies what kind of chat item this is.
   * Added because this is gonna need to say whether
   * the chat includes HTML or not.
   *
   * @protected
   * @memberof GameBoardChatItemComponent
   */
  protected IdentifyChatItem(): void {
    if (this.ChatEvent.isEmojiOnlyMessage) {
      this.ChatClasses.push('game-board-chat-item-emoji-only');
    } else {
      this.ChatClasses.push('game-board-chat-item-message');
    }
  }

  /**
   * Implements NgOnInit.
   *
   * @memberof GameBoardChatItemComponent
   */
  public ngOnInit(): void {
    this.IdentifyChatItem();
  }
}
