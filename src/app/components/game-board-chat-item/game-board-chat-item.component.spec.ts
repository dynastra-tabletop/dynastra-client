import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameBoardChatItemComponent } from './game-board-chat-item.component';

describe('GameBoardChatItemComponent', () => {
  let component: GameBoardChatItemComponent;
  let fixture: ComponentFixture<GameBoardChatItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameBoardChatItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardChatItemComponent);
    component = fixture.componentInstance;
    component.ChatEvent = {
      roomId: '',
      messageBody: '',
      messageId: '',
      isEmojiOnlyMessage: false,
      sender: '',
      timestamp: 0,
      emojiReactions: []
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
