import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RolePlayingGameCharactersComponent } from './role-playing-game-characters.component';
import {RolePlayingGameService} from "../../services/role-playing-game.service";
import {MockModule, MockProvider} from "ng-mocks";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {ICharacter} from "../../../lib/interfaces/Character.interfaces";
import {from, Observable, throwError} from "rxjs";
import {AuthService} from "../../services/auth.service";
import {MockAuthService} from "../../../../spec/MockAuthService";
import {OAuthModule} from "angular-oauth2-oidc";
import {RouterTestingModule} from "@angular/router/testing";



describe('RolePlayingGameCharactersComponent', () => {
  let component: RolePlayingGameCharactersComponent;
  let fixture: ComponentFixture<RolePlayingGameCharactersComponent>;
  let foundCharacters: Array<ICharacter> = [{
    CharacterName: '',
    CharacterAbout: '',
    CharacterImgUrl: '',
    IsPublic: false,
    Id: '',
    UserId: ''
  }];
  let gameServiceMock: any;

  function regularSetup() {
    TestBed.overrideProvider(AuthService, {
      useValue: new MockAuthService(),
    });
    TestBed.overrideProvider(RolePlayingGameService, {
      useValue: gameServiceMock
    });
    fixture = TestBed.createComponent(RolePlayingGameCharactersComponent);
    component = fixture.componentInstance;
    component.GameId = "";
    fixture.detectChanges();
  }

  function errorsSetup() {
    TestBed.overrideProvider(AuthService, {
      useValue: new MockAuthService(),
    });
    TestBed.overrideProvider(RolePlayingGameService, {
      useValue: {
        ListCharactersInRolePlayingGame: () => new Observable((obs) => {
          obs.error('error');
        }),
      }
    });
    fixture = TestBed.createComponent(RolePlayingGameCharactersComponent);
    component = fixture.componentInstance;
    component.GameId = "";
    fixture.detectChanges();
  }

  beforeEach(async () => {
    gameServiceMock = {
      ListCharactersInRolePlayingGame: () => from([foundCharacters])
    };

    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MockModule(OAuthModule.forRoot()),
        RouterTestingModule,
      ],
      providers: [
        MockProvider<RolePlayingGameService>(RolePlayingGameService),
        MockProvider<AuthService>(AuthService),
      ],
      declarations: [ RolePlayingGameCharactersComponent ]
    })
    .compileComponents();
  });

  it('should create', () => {
    regularSetup();
    expect(component).toBeTruthy();
  });

  it('should start looking for new characters when created', () => {
    regularSetup();
    expect(component.Characters).toEqual(foundCharacters);
  });
  it('should conclude loading if the request to retrieve characters fails', () => {
    regularSetup();
    fixture.detectChanges();
    expect(component.IsLoading).toBeFalsy();
  });
  it('should correctly notify when remove or add is selected to communicate with parent', () => {
    regularSetup();
    expect(() => {
      component.addCharacter();
      component.removeCharacter();
      component.reloadCharacters();
    }).not.toThrow();
  });
  it('should correctly handle an error when the endpoint errors', () => {
    errorsSetup();
    expect(component.EncounteredError).not.toBeNull();
  });
  it('should not allow retrieval if there is no valid game ID', () => {
    errorsSetup();
    expect(component.EncounteredError).not.toBeNull();
  });
});
