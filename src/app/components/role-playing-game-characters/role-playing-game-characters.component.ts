import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RolePlayingGameService} from "../../services/role-playing-game.service";
import {ICharacter} from "../../../lib/interfaces/Character.interfaces";

@Component({
  selector: 'app-role-playing-game-characters',
  templateUrl: './role-playing-game-characters.component.html',
  styleUrls: ['./role-playing-game-characters.component.scss']
})
export class RolePlayingGameCharactersComponent implements OnInit {
  /**
   * Is the REST request loading?
   * @protected
   */
  protected IsRESTLoading: boolean | null = null;

  /**
   * Did this encounter an error when trying to get characters?
   * @protected
   */
  public EncounteredError: boolean | null = null;

  /**
   * The characters received.
   * @protected
   */
  public Characters: Array<ICharacter> | null = null;

  /**
   * The ID of the role-playing game
   */
  @Input()
  public GameId!: string | undefined;

  /**
   * Outputs to note that the user is requesting to add a character
   * so the parent needs to launch the modal.
   */
  @Output()
  public ShouldSelectCharacters: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * Outputs to state the user wants to remove their characters from the game.
   */
  @Output()
  public ShouldSelectRemoveCharacters: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * Checks if characters are loaded and there are characters.
   * @constructor
   */
  public get HasCharactersInGame(): boolean {
    return this.Characters !== null && this.Characters.length > 0;
  }

  /**
   * Is this component currently loading?
   * @constructor
   */
  public get IsLoading() : boolean {
    return this.Characters !== null && this.IsRESTLoading == true;
  }

  /**
   * Reloads its characters when requested to.
   */
  public reloadCharacters(): void {
    this.GetCharactersInRolePlayingGame();
  }

  /**
   * Calls the parent to say that characters need to be selected.
   */
  public addCharacter() {
    this.ShouldSelectCharacters.emit(true);
  }

  /**
   * Calls the parent to say that characters need to be removed.
   */
  public removeCharacter() {
    this.ShouldSelectRemoveCharacters.emit(true);
  }

  /**
   * Single-purpose for dealing with load completion.
   * @param error
   * @protected
   */
  protected onLoadFinished(error: boolean = false) {
      if (error) {
        this.EncounteredError = true;
        return;
      }
      this.IsRESTLoading = false;
  }

  /**
   * Loads the characters for this role-playing game.
   * @constructor
   * @protected
   */
  protected GetCharactersInRolePlayingGame(): void {
    this.IsRESTLoading = true;
    this.RolePlayingGameService.ListCharactersInRolePlayingGame(this.GameId as string)
      .subscribe({
        next: (characters) => {
          this.Characters = characters;
          this.onLoadFinished();
        },
        error: () => {
          this.onLoadFinished(true);
        },
        complete: () => {
          this.onLoadFinished();
        }
      })
  }

  /**
   * Calls on init.
   */
  ngOnInit(): void {
    this.GetCharactersInRolePlayingGame();
  }

  /**
   * Creates a new instance of a role-playing characters component.
   * @param RolePlayingGameService
   */
  constructor(
    protected readonly RolePlayingGameService: RolePlayingGameService,
  ) { }
}
