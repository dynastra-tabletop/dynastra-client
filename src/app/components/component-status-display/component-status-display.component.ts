import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-component-status-display',
  templateUrl: './component-status-display.component.html',
  styleUrls: ['./component-status-display.component.scss']
})
export class ComponentStatusDisplayComponent implements OnInit {

  @Input()
  public ImageURL!: string;

  @Input()
  public StatusDisplayText!: string;

  @Input()
  public ActionButtonText!: string;

  @Output()
  public OnActionClicked: EventEmitter<boolean> = new EventEmitter<boolean>();

  public ActionClicked() {
    this.OnActionClicked.emit(true);
  }

  ngOnInit(): void {
  }

  constructor() { }


}
