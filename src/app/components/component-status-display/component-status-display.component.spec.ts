import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentStatusDisplayComponent } from './component-status-display.component';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";

describe('ComponentStatusDisplayComponent', () => {
  let component: ComponentStatusDisplayComponent;
  let fixture: ComponentFixture<ComponentStatusDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NoopAnimationsModule],
      declarations: [ ComponentStatusDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentStatusDisplayComponent);
    component = fixture.componentInstance;
    component.ImageURL ='';
    component.StatusDisplayText = '';
    component.ActionButtonText = '';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should output correctly', (done) => {
    component.OnActionClicked.subscribe(() => {
      expect(true).toEqual(true);
      done();
    });
    component.ActionClicked();
  })
});
