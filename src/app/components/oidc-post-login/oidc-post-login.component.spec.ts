import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OidcPostLoginComponent } from './oidc-post-login.component';
import {RouterTestingModule} from "@angular/router/testing";
import {OAuthModule} from "angular-oauth2-oidc";
import {MockModule} from "ng-mocks";
import {AuthService} from "../../services/auth.service";
import {OidcComponent} from "../oidc/oidc.component";

describe('OidcPostLoginComponent', () => {
  let component: OidcPostLoginComponent;
  let fixture: ComponentFixture<OidcPostLoginComponent>;

  beforeEach(async () => {
    TestBed.overrideProvider(AuthService, {
      useValue: {
        checkForAuth: () => {},
      }
    })
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MockModule(OAuthModule.forRoot()),
      ],
      declarations: [ OidcPostLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OidcPostLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create',() => {
    expect(component).toBeTruthy();
  })
});
