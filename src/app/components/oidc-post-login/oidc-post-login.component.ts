import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-oidc-post-login',
  templateUrl: './oidc-post-login.component.html',
  styleUrls: ['./oidc-post-login.component.scss']
})
export class OidcPostLoginComponent implements OnInit {
  /**
   * Oioi
   *
   * @memberof OidcPostLoginComponent
   */
  ngOnInit(): void {
    this.AuthService.checkForAuth();
  }

  /**
   * Creates an instance of OidcPostLoginComponent.
   * @param {ActivatedRoute} route
   * @memberof OidcPostLoginComponent
   */
  constructor(
    private route: ActivatedRoute,
    protected readonly AuthService: AuthService
  ) {}

}
