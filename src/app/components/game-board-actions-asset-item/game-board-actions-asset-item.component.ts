import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IRolePlayingGameAsset } from 'src/lib/interfaces/RolePlayingGame.interfaces';

@Component({
  selector: 'app-game-board-actions-asset-item',
  templateUrl: './game-board-actions-asset-item.component.html',
  styleUrls: ['./game-board-actions-asset-item.component.scss']
})
export class GameBoardActionsAssetItemComponent implements OnInit {
  /**
   * The asset which is being displayed.
   *
   * @type {IRolePlayingGameAsset}
   * @memberof GameBoardActionsAssetItemComponent
   */
  @Input()
  public Asset!: IRolePlayingGameAsset;

  /**
   * The full image.
   *
   * @type {string}
   * @memberof GameBoardActionsAssetItemComponent
   */
  public AssetImage: string = "";

  /**
   * Implements OnInit.
   *
   * @memberof GameBoardActionsAssetItemComponent
   */
  public ngOnInit(): void {
    this.AssetImage = `${environment.asset_prefix}${this.Asset.FileName}`;
  }

}
