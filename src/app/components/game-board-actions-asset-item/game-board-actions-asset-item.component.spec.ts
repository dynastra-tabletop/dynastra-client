import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RolePlayingGameAssetType } from 'src/lib/enums/role-playing-games.enums';

import { GameBoardActionsAssetItemComponent } from './game-board-actions-asset-item.component';

describe('GameBoardActionsAssetItemComponent', () => {
  let component: GameBoardActionsAssetItemComponent;
  let fixture: ComponentFixture<GameBoardActionsAssetItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameBoardActionsAssetItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardActionsAssetItemComponent);
    component = fixture.componentInstance;
    component.Asset = {
      FileName: "",
      AssetType: RolePlayingGameAssetType.Token,
      AssetReferenceId: "",
      DateAdded: "",
      Description: "",
      RolePlayingGameId: "",
      CreatedBy: "",
      Title: ""
    };
    component.AssetImage = "";
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
