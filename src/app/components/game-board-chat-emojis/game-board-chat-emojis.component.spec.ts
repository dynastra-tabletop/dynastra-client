import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameBoardChatEmojisComponent } from './game-board-chat-emojis.component';

describe('GameBoardChatEmojisComponent', () => {
  let component: GameBoardChatEmojisComponent;
  let fixture: ComponentFixture<GameBoardChatEmojisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameBoardChatEmojisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardChatEmojisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
