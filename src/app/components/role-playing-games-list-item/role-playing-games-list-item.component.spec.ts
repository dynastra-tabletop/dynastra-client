import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RolePlayingGamesListItemComponent } from './role-playing-games-list-item.component';
import {IRolePlayingGame} from "../../../lib/interfaces/RolePlayingGame.interfaces";
import {MatMenuModule} from "@angular/material/menu";
import {MatCardFooter, MatCardHeader, MatCardModule, MatCardSubtitle, MatCardTitle} from "@angular/material/card";
import {MatIcon, MatIconModule} from "@angular/material/icon";
import {
  RolePlayingGamesListEditItemComponent
} from "../role-playing-games-list-edit-item/role-playing-games-list-edit-item.component";
import {
  RolePlayingGamesListDisplayItemComponent
} from "../role-playing-games-list-display-item/role-playing-games-list-display-item.component";

describe('RolePlayingGamesListItemComponent', () => {
  let component: RolePlayingGamesListItemComponent;
  let fixture: ComponentFixture<RolePlayingGamesListItemComponent>;
  let completeRolePlayingGame: IRolePlayingGame;
  let nativeElement: any;
  let hasEditItem: any;
  let hasJoinButton: any;
  let hasCancelButton: any;
  let hasSaveButton: any;
  beforeAll(() => {
    completeRolePlayingGame = {
      Id: "",
      Name: "At the car at the side of the road",
      Description: "You should know. Time's tide will smother you, and I will too.",
      ImageUrl: "https://cdn.britannica.com/53/195353-050-C5EFAA74/Che-Guevara.jpg",
      CreatedByUser: "A Sexy Beast",
      Characters: [],
      Users: []
    };
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatMenuModule,
        MatCardModule,
        MatIconModule
      ],
      declarations: [
        RolePlayingGamesListItemComponent,
        RolePlayingGamesListEditItemComponent,
        RolePlayingGamesListDisplayItemComponent
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RolePlayingGamesListItemComponent);
    component = fixture.componentInstance;
    component.RolePlayingGame = completeRolePlayingGame;
    fixture.detectChanges();
    nativeElement = fixture.nativeElement;
    hasEditItem = nativeElement.querySelector("app-role-playing-games-edit-display-item");
    hasJoinButton = nativeElement.querySelector("button#join-button");
    hasCancelButton = nativeElement.querySelector("button#cancel-button");
    hasSaveButton = nativeElement.querySelector("button#save-button");
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should by default display in "display" mode', () => {    const hasDisplayItem = nativeElement.querySelector("app-role-playing-games-list-display-item");
    expect(hasDisplayItem).not.toBeNull();
    expect(hasEditItem).toBeNull();
    expect(hasJoinButton).not.toBeNull();
    expect(hasCancelButton).toBeNull();
    expect(hasSaveButton).toBeNull();
  });
  it('should switch to "edit" mode when instructed', () => {
    component.DisplayMode = component.EDIT_MODE;
    fixture.detectChanges();
    const hasDisplayItem = nativeElement.querySelector("app-role-playing-games-list-display-item");
    const hasEditItem = nativeElement.querySelector("app-role-playing-games-edit-display-item");
    const hasJoinButton = nativeElement.querySelector("button#join-button");
    const hasCancelButton = nativeElement.querySelector("button#cancel-button");
    const hasSaveButton = nativeElement.querySelector("button#save-button");
    expect(hasDisplayItem).toBeNull();
    expect(hasEditItem).toBeNull();
    expect(hasJoinButton).toBeNull();
    expect(hasCancelButton).not.toBeNull();
    expect(hasSaveButton).not.toBeNull();
  });
  it('should save the edited item when requested (not called via menu)', () => {
      component.onRolePlayingGameUpdated(completeRolePlayingGame);
      component.onSaveEditedItem(true);
      fixture.detectChanges();
      expect(component.DisplayMode).toEqual(component.DISPLAY_MODE);
  });
  it('should return to the display mode if no changes are made', () => {
    component.onSaveEditedItem(true);
    fixture.detectChanges();
    expect(component.DisplayMode).toEqual(component.DISPLAY_MODE);
  });
  it('should request from the parent to delete the selected item (not called via menu)', () => {
    expect(() => component.onRequestDelete(true)).not.toThrow();
  })
});
