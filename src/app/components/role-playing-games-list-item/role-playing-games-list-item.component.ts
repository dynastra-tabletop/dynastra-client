import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IRolePlayingGame} from "../../../lib/interfaces/RolePlayingGame.interfaces";
import {RolePlayingGamesListItemMode} from "./role-playing-games-list-item.enums";

@Component({
  selector: 'app-role-playing-games-list-item',
  templateUrl: './role-playing-games-list-item.component.html',
  styleUrls: ['./role-playing-games-list-item.component.scss']
})
export class RolePlayingGamesListItemComponent {
  /**
   * A middle-way solution to Angular not accessing enums.
   * (By the way, I agree with angular not doing that!)
   */
  public DISPLAY_MODE = RolePlayingGamesListItemMode.Display;

  /**
   * A middle-way solution to Angular not accessing enums.
   * (By the way, I agree with angular not doing that!)
   */
  public EDIT_MODE = RolePlayingGamesListItemMode.Edit;

  /**
   * Determines which subcomponent gets displayed.
   * @protected
   */
  public DisplayMode: RolePlayingGamesListItemMode = RolePlayingGamesListItemMode.Display;

  /**
   * Passes the item being deleted to the parent, so it maintains a singleton.
   */
  @Output()
  public OnDelete: EventEmitter<string> = new EventEmitter<string>();

  /**
   * Is called when the user completes editing this selected item.
   */
  @Output()
  public OnEdit: EventEmitter<IRolePlayingGame> = new EventEmitter<IRolePlayingGame>();

  /**
   * Emits when the user requests to join the game.
   */
  @Output()
  public OnJoin: EventEmitter<string> = new EventEmitter<string>();

  /**
   * The game which is being displayed by this component.
   */
  @Input()
  public RolePlayingGame!: IRolePlayingGame;

  /**
   * The ID of the logged-in user.
   */
  @Input()
  public LoggedInUserId!: string;

  /**
   * The role-playing game that is updated when data is passed back and forth,
   * this prevents updating the original object.
   * @protected
   */
  protected unSavedRolePlayingGame!: IRolePlayingGame | undefined;

  /**
   * Checks to see if the user can join this given game.
   * @constructor
   */
  public get CanJoinGame(): boolean {
    return !this.IsCurrentUserInGame;
  }

  /**
   * Checks to see if the current user is in this game.
   * @constructor
   * @protected
   */
  public get IsCurrentUserInGame(): boolean {
    return (this.RolePlayingGame.Users!.find((u) => u.ProviderUserId === this.LoggedInUserId)) !== undefined;
  }

  /**
   * Used when the display mode changes.
   * @param mode
   */
  public modeChangeRequest(mode: RolePlayingGamesListItemMode) {
    this.DisplayMode = mode;
  }

  /**
   * Requests the user join the game.
   * @param event
   */
  public onRequestJoin(event: any) {
    this.OnJoin.emit(this.RolePlayingGame?.Id);
  }

  /**
   * When the individual client is in "display" mode and the user clicks "delete"
   * @param event
   */
  public onRequestDelete(event: any) {
    this.OnDelete.emit(this.RolePlayingGame.Id);
  }

  /**
   * When the edited item is saved, this is called to pass to the parent the item to save.
   * @param event
   */
  public onSaveEditedItem(event: any) {
    if (this.unSavedRolePlayingGame === undefined) {
      this.modeChangeRequest(RolePlayingGamesListItemMode.Display);
      return;
    }
    this.OnEdit.emit(this.unSavedRolePlayingGame);
    this.RolePlayingGame = this.unSavedRolePlayingGame;
    this.unSavedRolePlayingGame = undefined;
    this.modeChangeRequest(RolePlayingGamesListItemMode.Display);
  }

  /**
   * Called when the role-playing game is updated
   * @param rolePlayingGame
   */
  public onRolePlayingGameUpdated(rolePlayingGame: IRolePlayingGame) {
    this.unSavedRolePlayingGame = rolePlayingGame;
  }
}
