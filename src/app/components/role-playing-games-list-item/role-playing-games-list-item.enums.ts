/**
 * The display mode to either display or edit the
 */
export enum RolePlayingGamesListItemMode {
  Loading = "LOADING",
  Display = "DISPLAY",
  Edit  = "EDIT"
}
