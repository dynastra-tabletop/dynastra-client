import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OidcAwaitingComponent } from './oidc-awaiting.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";

describe('OidcAwaitingComponent', () => {
  let component: OidcAwaitingComponent;
  let fixture: ComponentFixture<OidcAwaitingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatProgressSpinnerModule,
      ],
      declarations: [ OidcAwaitingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OidcAwaitingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
