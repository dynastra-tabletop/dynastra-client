import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { IDynastraCompactChatEvent } from 'src/lib/interfaces/Chat.interfaces';

@Component({
  selector: 'app-game-board-chat',
  templateUrl: './game-board-chat.component.html',
  styleUrls: ['./game-board-chat.component.scss']
})
export class GameBoardChatComponent implements OnChanges {
  /**
   * Are we showing the emoji board for the user to select emojis from?
   *
   * @type {boolean}
   * @memberof GameBoardChatComponent
   */
  public displayingEmojiBoard: boolean = false;

  /**
   * The subscription for the chat which is incoming from the chat service.
   *
   * @protected
   * @type {Subscription}
   * @memberof GameBoardChatComponent
   */
  protected ChatSubscription!: Subscription;

  /**
   * The chat history that was passed into the chat component.
   *
   * @type {Array<IDynastraCompactChatEvent>}
   * @memberof GameBoardChatComponent
   */
  @Input()
  public ChatHistory: Array<IDynastraCompactChatEvent> = [];


  /**
   * Used to tell the component that the chat history is loading.
   *
   * @type {boolean}
   * @memberof GameBoardChatComponent
   */
  @Input()
  public IsChatHistoryLoading: boolean = true;

  /**
   * Called when a chat message is sent via the chat component.
   *
   * @type {EventEmitter<string>}
   * @memberof GameBoardChatComponent
   */
  @Output()
  public OnChatMessageSent: EventEmitter<string> = new EventEmitter<string>();

  /**
   * Is the component loading?
   * (Avoid using getters for performance please!)
   *
   * @type {boolean}
   * @memberof GameBoardChatComponent
   */
  public IsLoading: boolean = true;

  @ViewChild('gameBoardChatLog')
  private ChatLogContainer!: ElementRef;

  /**
   * The form for the chat box.
   *
   * @type {FormGroup}
   * @memberof GameBoardChatComponent
   */
  public ChatForm: FormGroup = new FormGroup({
    chatText: new FormControl('', [
      Validators.required,
    ])
  })

  /**
   * Toggles the emoji board display below the text input.
   *
   * @return {*}  {boolean}
   * @memberof GameBoardChatComponent
   */
  public ToggleEmojiBoard(): void {
    this.displayingEmojiBoard = !this.displayingEmojiBoard;
  }

  /**
   * The emoji was selected by the user, and now we're inserting it into the chatbox.
   *
   * @param {{ emoji: any; }} emojiEvent
   * @memberof GameBoardChatComponent
   */
  public OnEmojiSelected(emojiEvent: { emoji: any; }): void {
    this.ChatForm.controls['chatText'].setValue(
      this.ChatForm.controls['chatText'].value + emojiEvent.emoji.native
    );
  }

  /**
   * Sends the message through to the
   *
   * @memberof GameBoardChatComponent
   */
  public SendMessage() {
    const chatInputComponent = this.ChatForm.controls['chatText'];
    this.OnChatMessageSent.emit(chatInputComponent.value);
    chatInputComponent.reset();
  }

  /**
   * A new message (chat only for beta) which has been received from the server.
   *
   * @protected
   * @param {IDynastraChatEvent} message
   * @memberof GameBoardChatComponent
   */
  protected OnMessageReceived(message: IDynastraCompactChatEvent) {
    this.IsLoading = false;
    this.ChatHistory.push(message);
    this.ChangeDetector.detectChanges();
    this.resetScrollToBottom();
  }

  /**
   * Resets the scroll to the bottom of the element.
   *
   * @protected
   * @memberof GameBoardChatComponent
   */
  protected resetScrollToBottom() {
    try {
      this.ChatLogContainer.nativeElement.scrollTop = this.ChatLogContainer.nativeElement.scrollHeight;
    } catch (_err) {}
  }

  /**
   * Called when the inputs change
   *
   * @param {SimpleChanges} changes
   * @memberof GameBoardChatComponent
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (changes['IsChatHistoryLoading'].currentValue) {
      return;
    }
    this.ChangeDetector.detectChanges()

    this.resetScrollToBottom();
  }


  /**
   * Creates an instance of GameBoardChatComponent.
   * @param {GameChatService} GameChatService
   * @memberof GameBoardChatComponent
   */
  constructor(protected readonly ChangeDetector: ChangeDetectorRef) {}
}
