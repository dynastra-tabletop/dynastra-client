import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameBoardChatComponent } from './game-board-chat.component';

describe('GameBoardChatComponent', () => {
  let component: GameBoardChatComponent;
  let fixture: ComponentFixture<GameBoardChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameBoardChatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('when the user clicks the emoji button, the emoji board should toggle', () => {
    var toggleEmojis = fixture.nativeElement.querySelector("#game-board-chat-toggle-emoji-button");
    toggleEmojis.click();
    fixture.detectChanges();
    var emojiDisplay = fixture.nativeElement.querySelector("#dynastra-game-board-chat-emojis");
    expect(emojiDisplay).not.toBeNull();
  });
  it('should insert an emoji into the text input', () => {
    component.OnEmojiSelected({
      emoji: {
        native: '💩'
      }
    });
    fixture.detectChanges();
    expect(component.ChatForm.value.chatText).toEqual('💩');
  });
});
