import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RolePlayingGameAssetType } from 'src/lib/enums/role-playing-games.enums';
import { IRolePlayingGameAsset } from 'src/lib/interfaces/RolePlayingGame.interfaces';

import { GameBoardUploadModalComponent } from '../game-board-upload-modal/game-board-upload-modal.component';

@Component({
  selector: 'app-game-board-actions-tokens',
  templateUrl: './game-board-actions-tokens.component.html',
  styleUrls: ['./game-board-actions-tokens.component.scss']
})
export class GameBoardActionsTokensComponent implements OnChanges {
  /**
   * The ID for the role-playing game.
   *
   * @type {string}
   * @memberof GameBoardActionsTokensComponent
   */
  @Input()
  public GameSessionId!: string;

  /**
   * The assets this component is displaying.
   *
   * @type {Array<IRolePlayingGameAsset>}
   * @memberof GameBoardActionsTokensComponent
   */
  @Input()
  public Assets!: Array<IRolePlayingGameAsset>;

  /**
   * Opens the modal for the upload.
   *
   * @memberof GameBoardActionsTokensComponent
   */
  public OpenModal(): void {
    this.dialog.open(GameBoardUploadModalComponent, {
      data: {
        sessionId: this.GameSessionId || '',
        assetType: RolePlayingGameAssetType.Token,
      },
      minWidth: 600,
      minHeight: 600,
      disableClose: true,
    });
  }

  /**
   * Implements OnChanges.
   *
   * @param {SimpleChanges} changes
   * @memberof GameBoardActionsTokensComponent
   */
  public ngOnChanges(changes: SimpleChanges): void {
    if (changes["Assets"].firstChange) {
      return;
    }

    if (changes["Assets"].currentValue !== this.Assets) {
      this.Assets = changes["Assets"].currentValue;
    }
  }

  /**
   * Creates an instance of GameBoardActionsTokensComponent.
   * @param {MatDialog} dialog
   * @memberof GameBoardActionsTokensComponent
   */
  constructor(protected readonly dialog: MatDialog) {}
}
