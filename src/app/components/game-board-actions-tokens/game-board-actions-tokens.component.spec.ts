import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { RolePlayingGameAssetType } from 'src/lib/enums/role-playing-games.enums';
import { IRolePlayingGameAsset } from 'src/lib/interfaces/RolePlayingGame.interfaces';

import { GameBoardActionsTokensComponent } from './game-board-actions-tokens.component';

describe('GameBoardActionsTokensComponent', () => {
  let component: GameBoardActionsTokensComponent;
  let fixture: ComponentFixture<GameBoardActionsTokensComponent>;
  let assets: Array<IRolePlayingGameAsset> = [{
    AssetReferenceId: '',
    AssetType: RolePlayingGameAssetType.Token,
    RolePlayingGameId: '',
    FileName: '0.jpg',
    DateAdded: new Date().toISOString(),
    Title: 'It is a title 1',
    Description: 'It is a description lol',
    CreatedBy: ''
  }, {
    AssetReferenceId: '',
    AssetType: RolePlayingGameAssetType.Token,
    RolePlayingGameId: '',
    FileName: '0.jpg',
    DateAdded: new Date().toISOString(),
    Title: 'It is a title 2',
    Description: 'It is a description lol',
    CreatedBy: ''
  }];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatDialogModule
      ],
      declarations: [ GameBoardActionsTokensComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardActionsTokensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should correctly update assets when component inputs are changed', () => {
    component.ngOnChanges({
      Assets: new SimpleChange(null, assets, true),
    });
    fixture.detectChanges();
    expect(component.Assets).not.toEqual(assets);
    component.ngOnChanges({
      Assets: new SimpleChange(null, assets, false),
    });
    fixture.detectChanges();
    expect(component.Assets).toEqual(assets);
  });

  it('should attempt to open a dialog when the upload button is clicked', () => {
    try {
      fixture.nativeElement
        .querySelector("#game-board-actions-token-upload-button")
        .click();
      fixture.detectChanges();
      expect(true).toBeTrue();
    } catch {
      expect(true).toEqual(false);
    }
  });

});
