export enum GameUploadModalDisplayStage {
  Idle,
  UploadSuccessAddMetaData,
  UploadFailed
}
