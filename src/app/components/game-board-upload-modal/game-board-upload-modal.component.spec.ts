import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { OAuthModule } from 'angular-oauth2-oidc';
import { MockModule } from 'ng-mocks';
import { from } from 'rxjs';
import { MockAuthService } from 'spec/MockAuthService';
import { AuthService } from 'src/app/services/auth.service';
import { RolePlayingGameAssetType } from 'src/lib/enums/role-playing-games.enums';

import { GameBoardUploadModalComponent } from './game-board-upload-modal.component';

describe('GameBoardUploadModalComponent', () => {
  let component: GameBoardUploadModalComponent;
  let fixture: ComponentFixture<GameBoardUploadModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [{
        provide: MAT_DIALOG_DATA,
        useValue: {
          assetType: RolePlayingGameAssetType.Token,
          sessionId: '',
        }
      },{
        provide: MatDialogRef,
        useValue: {
          open: () => {
            return {
              afterClosed: () => from([true, true, true, true]),
            };
          },
        }
      }, {
        provide: AuthService,
        useValue: new MockAuthService()
      }],
      imports: [
        HttpClientTestingModule,
        MockModule(OAuthModule.forRoot()),
        RouterTestingModule
      ],
      declarations: [ GameBoardUploadModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardUploadModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
