import { HttpClient } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FilePreviewModel } from 'ngx-awesome-uploader';
import { RolePlayingGameService } from 'src/app/services/role-playing-game.service';
import { environment } from 'src/environments/environment';
import { RolePlayingGameAssetType } from 'src/lib/enums/role-playing-games.enums';
import { IRolePlayingGameAssetRequest } from 'src/lib/interfaces/RolePlayingGame.interfaces';
import { DynastraUploadAdapter } from 'src/lib/Upload.adapter';

import { GameUploadModalDisplayStage } from './game-board-upload-modal.enums';

@Component({
  selector: 'app-game-board-upload-modal',
  templateUrl: './game-board-upload-modal.component.html',
  styleUrls: ['./game-board-upload-modal.component.scss']
})
export class GameBoardUploadModalComponent {
  /**
   * The adapter required for the uploader.
   *
   * @type {DynastraUploadAdapter}
   * @memberof GameBoardUploadModalComponent
   */
  public adapter!: DynastraUploadAdapter;

  /**
   * Which stage the current component is at.
   *
   * @type {GameUploadModalDisplayStage}
   * @memberof GameBoardUploadModalComponent
   */
  public Stage: GameUploadModalDisplayStage = GameUploadModalDisplayStage.Idle;

  /**
   * The URL of the image preview.
   *
   * @type {string}
   * @memberof GameBoardUploadModalComponent
   */
  public ImagePreviewURL: string = '';

  /**
   * Is the component loading (or never started?)
   *
   * @type {(boolean | null)}
   * @memberof GameBoardUploadModalComponent
   */
  public IsLoading: boolean | null = null;

  /**
   * Extensions which are usable within the uploader.
   *
   * @type {string}
   * @memberof GameBoardUploadModalComponent
   */
  public UsableFileExtensions: Array<string> = [];

  /**
   * The file extensions for display in the UI.
   *
   * @type {string}
   * @memberof GameBoardUploadModalComponent
   */
  public UsableFileExtensionsAsString: string = "";

  /**
   * Is the extension of the file invalid?
   *
   * @type {boolean}
   * @memberof GameBoardUploadModalComponent
   */
  public IsInvalidExtension: boolean = false;

  /**
   * The name of the image.
   *
   * @private
   * @type {string}
   * @memberof GameBoardUploadModalComponent
   */
  private ImageName!: string;

  /**
   * The value of the name field from the form
   */
  public get title() {
    return this.metadataForm.controls["title"];
  }

  /**
   * The description of the imageURL field from the form
   */
  public get description() {
    return this.metadataForm.controls["description"]
  }

  /**
   * Handles creation of the metadata form.
   *
   * @memberof GameBoardUploadModalComponent
   */
  public handleCreate(): void {
    if (this.IsLoading !== null && this.IsLoading) {
      return;
    }

    this.IsLoading = true;

    this.RolePlayingGameService.CreateGameAsset(
      this.data.sessionId,
      this.ImageName || '',
      this.title.value,
      this.description.value,
      this.data.assetType
    ).subscribe({
      next: (asset: IRolePlayingGameAssetRequest) => {
        this.IsLoading = false;
        this.MatDialogRef.close();
      },
      error: () => {
        this.IsLoading = false;
      }
    });
  }

  /**
   * Metadata about this given item.
   *
   * @type {FormGroup}
   * @memberof GameBoardUploadModalComponent
   */
  public metadataForm: FormGroup = new FormGroup({
    title: new FormControl('',[
      Validators.minLength(4),
      Validators.required,
    ]),
    description: new FormControl('', [
      Validators.required,
    ])
  });

  /**
   * When the file is uploaded.
   *
   * @memberof GameBoardUploadModalComponent
   */
  public OnFileUploaded(file: FilePreviewModel): void {
    this.IsInvalidExtension = false;
    this.ImageName = file.uploadResponse.filename;
    this.ImagePreviewURL = `${environment.asset_prefix}${this.ImageName}`;
    this.Stage = GameUploadModalDisplayStage.UploadSuccessAddMetaData;
  }

  /**
   * Failed to upload the file, need to retry.
   *
   * @memberof GameBoardUploadModalComponent
   */
  public OnFileUploadError(): void {
    this.Stage = GameUploadModalDisplayStage.UploadFailed;
  }

  /**
   * Called to reset the state of the invalid display.
   *
   * @memberof GameBoardUploadModalComponent
   */
  public OnValidationSuccess(): void {
    this.IsInvalidExtension = false;
  }

  /**
   * There was an invalid file provided.
   *
   * @memberof GameBoardUploadModalComponent
   */
  public OnValidationError(): void {
    this.IsInvalidExtension = true;
  }

  /**
   * Creates an instance of GameBoardUploadModalComponent.
   * @param {HttpClient} HttpClient
   * @memberof GameBoardUploadModalComponent
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { assetType: RolePlayingGameAssetType; sessionId: string; },
    protected readonly MatDialogRef: MatDialogRef<GameBoardUploadModalComponent>,
    protected readonly RolePlayingGameService: RolePlayingGameService,
    HttpClient: HttpClient
  ) {
    this.adapter = new DynastraUploadAdapter(HttpClient);
    this.UsableFileExtensions = environment.asset_upload_allowed_extensions.split(',');
    this.UsableFileExtensionsAsString = "." + environment.asset_upload_allowed_extensions
      .replace(/,/gi, " , .");
  }
}
