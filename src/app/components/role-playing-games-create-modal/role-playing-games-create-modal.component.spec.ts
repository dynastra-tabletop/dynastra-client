import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RolePlayingGamesCreateModalComponent } from './role-playing-games-create-modal.component';
import {MockModule} from "ng-mocks";
import {OAuthModule} from "angular-oauth2-oidc";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {AuthService} from "../../services/auth.service";
import {Observable} from "rxjs";
import {MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {ReactiveFormsModule} from "@angular/forms";
import {RolePlayingGameService} from "../../services/role-playing-game.service";

describe('RolePlayingGamesCreateModalComponent', () => {
  let component: RolePlayingGamesCreateModalComponent;
  let fixture: ComponentFixture<RolePlayingGamesCreateModalComponent>;

  beforeEach(async () => {
    TestBed.overrideProvider(AuthService, {
      useValue: {
        events: new Observable((obs) => {}),
      }
    });
    TestBed.overrideProvider(RolePlayingGameService, {
      useValue: {
        CreateRolePlayingGame: () => new Observable((obs) => {
          obs.next(true);
          obs.error(true);
          obs.complete();
        }),
      }
    })
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MockModule(OAuthModule.forRoot()),
        MatDialogModule,
        RouterTestingModule,
        ReactiveFormsModule,
      ],
      providers: [{
        provide: MatDialogRef,
        useValue: {
          close: () => {}
        }
      }],
      declarations: [ RolePlayingGamesCreateModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RolePlayingGamesCreateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should correctly update when form elements are updated',  () => {
    const nameFormItem = fixture.nativeElement.querySelector("#role-playing-game-name");
    const descFormItem = fixture.nativeElement.querySelector("#role-playing-game-description");
    const imgFormItem = fixture.nativeElement.querySelector("#role-playing-game-imageUrl");

    nameFormItem.value = "Skies of Arcadia";
    descFormItem.value = "Is an excellent game";
    imgFormItem.value = "http//www.sega.com/Dreamcast.jpg";

    nameFormItem.dispatchEvent(new Event('input'));
    descFormItem.dispatchEvent(new Event('input'));
    imgFormItem.dispatchEvent(new Event('input'));

    fixture.detectChanges();

    const nameFormVal = component.createModalForm.controls["name"];
    const descFormVal = component.createModalForm.controls["description"];
    const imgFormVal = component.createModalForm.controls["imageURL"];
    expect(nameFormVal.value).toEqual(nameFormItem.value);
    expect(descFormVal.value).toEqual(descFormItem.value);
    expect(imgFormVal.value).toEqual(imgFormItem.value);
    expect(component.createModalForm.valid).toBeTrue();
  });
  it('should not allow incorrect form elements to be input into the modal', () => {
    const nameFormItem = fixture.nativeElement.querySelector("#role-playing-game-name");
    const descFormItem = fixture.nativeElement.querySelector("#role-playing-game-description");
    const imgFormItem = fixture.nativeElement.querySelector("#role-playing-game-imageUrl");

    nameFormItem.value = 'shr';
    descFormItem.value = "";
    imgFormItem.value = "!!!!INVALID";

    nameFormItem.dispatchEvent(new Event('input'));
    descFormItem.dispatchEvent(new Event('input'));
    imgFormItem.dispatchEvent(new Event('input'));

    fixture.detectChanges();

    const nameFormVal = component.createModalForm.controls["name"];
    const descFormVal = component.createModalForm.controls["description"];
    const imgFormVal = component.createModalForm.controls["imageURL"];

    expect(nameFormVal.valid).toBeFalse();
    expect(descFormVal.valid).toBeFalse();
    expect(imgFormVal.valid).toBeFalse();
    expect(nameFormVal.errors!['minlength']).not.toBeUndefined();
    expect(descFormVal.errors!['required']).not.toBeUndefined();
    expect(imgFormVal.errors!['pattern']).not.toBeUndefined();
    expect(component.createModalForm.valid).toBeFalse();
  });
  it('should send requests to create if the component is not loading', () => {
    fixture.nativeElement.querySelector("#modal-action-create")
      .dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(component.isLoading).toBeFalsy();
  });
  it('should display text when the component is loading', () => {
    component.isLoading = true;
    fixture.detectChanges();
    expect(component.buttonActionText).toEqual('Saving..');
    component.handleCreate();
    expect(component.isLoading).toBeTrue();
  })
});
