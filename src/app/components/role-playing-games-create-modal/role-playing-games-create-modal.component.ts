import { Component } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { finalize } from 'rxjs/operators';

import { IRolePlayingGame } from '../../../lib/interfaces/RolePlayingGame.interfaces';
import { RolePlayingGameService } from '../../services/role-playing-game.service';

@Component({
  selector: 'app-role-playing-games-create-modal',
  templateUrl: './role-playing-games-create-modal.component.html',
  styleUrls: ['./role-playing-games-create-modal.component.scss'],
  providers: [
    RolePlayingGameService,
    MatDialogModule,
  ]
})
export class RolePlayingGamesCreateModalComponent {
  /**
   * The component is loading when trying to create the role playing game.
   * @protected
   */
  public isLoading: boolean | null = null;

  /**
   * The reference for the image URL control.
   * @private
   */
  private get imageURLControl(): AbstractControl {
    return this.createModalForm.controls["imageURL"];
  }

  /**
   * If the image is valid, then we display the image content.
   */
  public get hasValidImagePopulated(): boolean {
    return this.imageURLControl.valid &&
      this.imageURLControl.dirty;
  }

  /**
   * Text to display on the modal changes while loading.
   */
  public get buttonActionText(): string {
    if (this.isLoading) {
      return "Saving..";
    }
    return "Save"
  }

  /**
   * The value of the imagePreviewURL field from the form
   */
  public get imagePreviewURL(): string {
    return this.createModalForm.controls["imageURL"].value;
  }

  /**
   * The value of the name field from the form
   */
  public get name() {
    return this.createModalForm.controls["name"];
  }

  /**
   * The description of the imageURL field from the form
   */
  public get description() {
    return this.createModalForm.controls["description"]
  }

  /**
   * The value of the imageURL field from the form
   */
  public get imageURL() {
    return this.createModalForm.controls["imageURL"];
  }

  /**
   * The role-playing game generated from the form elements.
   */
  public get rolePlayingGameItem(): IRolePlayingGame {
    return {
      Name: this.name.value,
      Description: this.description.value,
      ImageUrl: this.imageURL.value,
      Characters: [],
      Users: [],
      CreatedByUser: "",
    };
  }

  /**
   * The form group that is bound to the form in the "Create"
   * modal.
   */
  public createModalForm: FormGroup = new FormGroup({
    name: new FormControl('',[
      Validators.minLength(4),
      Validators.required,
    ]),
    description: new FormControl('', [
      Validators.required,
    ]),
    imageURL: new FormControl('', [
      Validators.required,
      Validators.pattern('[(http(s)?):\\/\\/(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)'),
    ]),
  });

  /**
   * Starts the process to create
   */
  public handleCreate(): void {
    if(this.isLoading) {
      return;
    }

    this.isLoading = true;

    this.RolePlayingGameService
      .CreateRolePlayingGame(this.rolePlayingGameItem)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      ).subscribe({
        next: () => this.dialogRef.close(),
        error: () => this.dialogRef.close(),
      });
  }

  /**
   * Creates a new instance of the modal.
   * @param RolePlayingGameService
   * @param dialogRef
   */
  constructor(
    protected readonly RolePlayingGameService: RolePlayingGameService,
    public dialogRef: MatDialogRef<RolePlayingGamesCreateModalComponent>,
  ) { }
}
