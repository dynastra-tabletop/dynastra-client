import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IRolePlayingGame} from "../../../lib/interfaces/RolePlayingGame.interfaces";
import {RolePlayingGameDisplayType} from "../../../lib/enums/role-playing-games.enums";

@Component({
  selector: 'app-role-playing-games-list-display-item',
  templateUrl: './role-playing-games-list-display-item.component.html',
  styleUrls: ['./role-playing-games-list-display-item.component.scss']
})
export class RolePlayingGamesListDisplayItemComponent {
  /**
   * Gets the number of characters in a game.
   * @constructor
   */
  public get NumberOfCharacters(): number {
    return this.RolePlayingGame.Characters!.length;
  }

  /**
   * Gets the number of users in the game.
   * @constructor
   */
  public get NumberOfUsers(): number {
    return this.RolePlayingGame.Users!.length;
  }

  /**
   * The role playing game that this list display item is displaying, taken from parent.
   */
  @Input()
  public RolePlayingGame!: IRolePlayingGame;

  /**
   * The type of display. Currently, defaulted to "grid".
   */
  @Input()
  public DisplayType: RolePlayingGameDisplayType = RolePlayingGameDisplayType.Grid;
}
