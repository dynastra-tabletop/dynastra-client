import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RolePlayingGamesListDisplayItemComponent } from './role-playing-games-list-display-item.component';
import {IRolePlayingGame} from "../../../lib/interfaces/RolePlayingGame.interfaces";
import {RouterTestingModule} from "@angular/router/testing";

describe('RolePlayingGamesListDisplayItemComponent', () => {
  let component: RolePlayingGamesListDisplayItemComponent;
  let fixture: ComponentFixture<RolePlayingGamesListDisplayItemComponent>;

  let completeRolePlayingGame: IRolePlayingGame;

  beforeAll(() => {
    completeRolePlayingGame = {
      Id: "",
      Name: "A test game",
      Description: "OMG A test game",
      ImageUrl: "http://localhost/sexy-pix.jpg",
      CreatedByUser: "",
      Characters: [],
      Users: []
    }
  })

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [ RolePlayingGamesListDisplayItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RolePlayingGamesListDisplayItemComponent);
    component = fixture.componentInstance;
    component.RolePlayingGame = completeRolePlayingGame;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the role-playing game when its input is provided', async () => {
    const contents = fixture.nativeElement.querySelector(".dynastra-role-playing-games-list-display-description p");
    const imageContents = fixture.nativeElement.querySelector(".dynastra-role-playing-games-list-display-image img");
    expect(contents.innerText).toEqual(completeRolePlayingGame.Description);
    expect(imageContents.src).toEqual(completeRolePlayingGame.ImageUrl);
    expect(imageContents.alt).toEqual(completeRolePlayingGame.Description);
  });
});
