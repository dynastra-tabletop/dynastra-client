import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RolePlayingGamesSearchComponent } from './role-playing-games-search.component';

describe('RolePlayingGamesSearchComponent', () => {
  let component: RolePlayingGamesSearchComponent;
  let fixture: ComponentFixture<RolePlayingGamesSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RolePlayingGamesSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RolePlayingGamesSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
