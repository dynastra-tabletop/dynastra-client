import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Router } from '@angular/router';
import { IRolePlayingGame } from 'src/lib/interfaces/RolePlayingGame.interfaces';
import {RolePlayingGameService} from "../../services/role-playing-game.service";
import {RolePlayingGamesListItemMode} from "../role-playing-games-list-item/role-playing-games-list-item.enums";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-role-playing-games-list-display',
  templateUrl: './role-playing-games-list-display.component.html',
  styleUrls: ['./role-playing-games-list-display.component.scss'],
  providers: [
    RolePlayingGameService,
    AuthService,
  ]
})
export class RolePlayingGamesListDisplayComponent {
  /**
   * Notifier for the list if it's loading.
   */
  public isLoadingInternally: boolean | null = null;

  /**
   * Checks if parent or itself is loading.
   */
  public get isLoading(): boolean {
    return this.isLoadingInternally == true || this.LoadingGames == true;
  }

  /**
   * Checks to see if games have been hydrated from the parent.
   */
  public get hasHydratedGames(): boolean {
    return this.LoadingGames !== null;
  }

  /**
   * Tells the component if the component loaded but there's no results.
   */
  public get noGamesFound(): boolean {
    return this.hasHydratedGames && this.RolePlayingGames.length === 0;
  }

  /**
   * The userID of the current logged-in user.
   */
  public get loggedInUserId(): string {
    return this.AuthService.loggedInUser?.userID || '';
  }

  /**
   * Gets told by its parent if it has any results.
   */
  @Input()
  public LoadingGames: boolean | null = null;

  /**
   * The role playing games that the component is displaying.
   *
   * @type {Array<IRolePlayingGame>}
   * @memberof RolePlayingGamesListDisplayComponent
   */
  @Input()
  public RolePlayingGames: Array<IRolePlayingGame> = [];

  /**
   * When this component is telling the parent it is switching back to another view.
   * Okay, this isn't ideal, however, the child element doesn't use card, so there is
   * no common element; load neither. So, as loathe as I am to use an output, it's warranted.
    */
  @Output()
  public changeMode: EventEmitter<RolePlayingGamesListItemMode> = new EventEmitter<RolePlayingGamesListItemMode>();

  /**
   * The items in this display have changed, the parent needs to refresh them.
   */
  @Output()
  public displayItemsChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * Ends the loading of this component when REST calls are over.
   * @protected
   */
  protected endLoading() {
    this.isLoadingInternally = false;
    this.displayItemsChanged.emit(true);
  }

  /**
   * Gets called when the user requests delete, this is used to prevent injecting into N items.
   * @param id
   */
  public onRequestDelete(id: string) {
    const endLoadingCallback = this.endLoading.bind(this);
    this.RolePlayingGameService.DeleteRolePlayingGame(id)
      .subscribe({
        next: endLoadingCallback,
        error: endLoadingCallback,
        complete: endLoadingCallback,
      });
  }

  /**
   * When the child component requests for the list to change
   * @param rolePlayingGame
   */
  public onRequestEdit(rolePlayingGame: IRolePlayingGame): void {
    const endLoadingCallback = this.endLoading.bind(this);
    this.RolePlayingGameService.UpdateRolePlayingGame(rolePlayingGame.Id as string, rolePlayingGame)
      .subscribe({
        next: endLoadingCallback,
        error: endLoadingCallback,
        complete: endLoadingCallback
      });
  }

  /**
   * Requests to join a given role-playing game.
   * @param rolePlayingGame
   */
  public onRequestJoin(gameId: string): void {
    const endLoadingCallback = this.endLoading.bind(this);
    this.RolePlayingGameService.AddCurrentUserToRolePlayingGame(gameId)
      .subscribe({
        next: () => {
          this.endLoading();
          this.Router.navigate(["game", gameId])
            .then(() => {});
        },
        error: endLoadingCallback,
        complete: endLoadingCallback
      });
  }

  /**
   * Creates a new instance of the component.
   * @param RolePlayingGameService
   */
  constructor(
    protected readonly Router: Router,
    protected readonly AuthService: AuthService,
    protected readonly RolePlayingGameService: RolePlayingGameService
  ) {
  }
}
