import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockModule, MockProvider } from 'ng-mocks';

import { AuthService } from '../../services/auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { IRolePlayingGame } from '../../../lib/interfaces/RolePlayingGame.interfaces';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MockAuthService } from '../../../../spec/MockAuthService';
import { OAuthModule } from 'angular-oauth2-oidc';
import { RolePlayingGameService } from '../../services/role-playing-game.service';
import { RolePlayingGamesListDisplayComponent } from './role-playing-games-list-display.component';
import { RolePlayingGamesListItemComponent } from '../role-playing-games-list-item/role-playing-games-list-item.component';
import { RouterTestingModule } from '@angular/router/testing';
import { from } from 'rxjs';

class MockAuthProvider {

}

describe('RolePlayingGamesListDisplayComponent', () => {
  let component: RolePlayingGamesListDisplayComponent;
  let fixture: ComponentFixture<RolePlayingGamesListDisplayComponent>;
  let retrievedGames: Array<IRolePlayingGame> = [{
    Id: "",
    ImageUrl: "",
    Name: "First Game",
    Description: "The First Game",
    CreatedByUser: "",
    Characters: [],
    Users: []
  }, {
    Id: "",
    ImageUrl: "",
    Name: "Second Game",
    Description: "The Second Game",
    CreatedByUser: "",
    Characters: [],
    Users: []
  }];

  beforeEach(async () => {
    TestBed.overrideProvider(RolePlayingGameService, { useValue: {
        UpdateRolePlayingGame: () => from([true]),
        DeleteRolePlayingGame: () => from([true]),
      }});
    await TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        MatMenuModule,
        HttpClientTestingModule,
        MockModule(OAuthModule.forRoot()),
        RouterTestingModule.withRoutes([]),
      ],
      declarations: [ RolePlayingGamesListDisplayComponent, RolePlayingGamesListItemComponent ],
      providers: [
        MockProvider(RolePlayingGameService, {
          UpdateRolePlayingGame: () => from([true]),
          DeleteRolePlayingGame: () => from([true]),
        } as any),
      ],
    })
    .compileComponents();
    TestBed.overrideProvider(AuthService, { useValue : new MockAuthService() });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RolePlayingGamesListDisplayComponent);
    component = fixture.componentInstance;
    component.LoadingGames = false;
    component.RolePlayingGames = retrievedGames;
    component.isLoadingInternally = false;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display role playing games in a list', () => {
    const listDisplayItem = fixture.nativeElement.querySelector("#dynastra-role-playing-games-list-display-content");
    expect(listDisplayItem).not.toBeUndefined();
  });
  it('should display a message if a REST request was made and no results exist', () => {
    component.RolePlayingGames = [];
    fixture.detectChanges();
    const noneFoundDisplay = fixture.nativeElement.querySelector("#dynastra-role-playing-games-no-games-found div");
    expect(noneFoundDisplay).not.toBeUndefined();
  });
  it('should correctly edit the role-playing game ', (done) => {
    component.displayItemsChanged.subscribe((changed) => {
      expect(component.isLoadingInternally).toEqual(false);
      expect(changed).toEqual(true);
      done();
    });
    component.RolePlayingGames = retrievedGames;
    fixture.detectChanges();
    component.onRequestEdit(retrievedGames[0]);
  });
  it('should correctly delete the role-playing game ', (done) => {
    component.displayItemsChanged.subscribe((changed) => {
      expect(component.isLoadingInternally).toEqual(false);
      expect(changed).toEqual(true);
      done();
    });
    component.RolePlayingGames = retrievedGames;
    fixture.detectChanges();
    component.onRequestDelete("");
  });
});
