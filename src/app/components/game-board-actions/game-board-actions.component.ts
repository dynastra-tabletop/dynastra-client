import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { RolePlayingGameService } from 'src/app/services/role-playing-game.service';
import { RolePlayingGameAssetType } from 'src/lib/enums/role-playing-games.enums';
import { IRolePlayingGameAsset } from 'src/lib/interfaces/RolePlayingGame.interfaces';

@Component({
  selector: 'app-game-board-actions',
  templateUrl: './game-board-actions.component.html',
  styleUrls: ['./game-board-actions.component.scss'],
})
export class GameBoardActionsComponent implements OnInit {
  /**
   * The session ID that we're currently using.
   *
   * @type {string}
   * @memberof GameBoardActionsComponent
   */
  @Input()
  public RolePlayingGameId!: string;

  /**
   * Is the element currently loading?
   *
   * @type {(boolean | null)}
   * @memberof GameBoardActionsComponent
   */
  public IsLoading: boolean | null = null;

  /**
   * The assets that are going to be loaded.
   *
   * @type {Array<IRolePlayingGameAsset>}
   * @memberof GameBoardActionsTokensComponent
   */
   public TokenAssets!: Array<IRolePlayingGameAsset>;

   /**
    * The map object assets.
    *
    * @type {Array<IRolePlayingGameAsset>}
    * @memberof GameBoardActionsComponent
    */
   public ObjectAssets!: Array<IRolePlayingGameAsset>;

   /**
    * Gets the token assets for the role-playing game.
    *
    * @private
    * @memberof GameBoardActionsTokensComponent
    */
   private GetTokenAssetsForRolePlayingGame() {
     this.IsLoading = true;

      forkJoin({
        tokenAssets: this.RolePlayingGameService.GetAssetsForGame(this.RolePlayingGameId, RolePlayingGameAssetType.Token),
        objectAssets: this.RolePlayingGameService.GetAssetsForGame(this.RolePlayingGameId, RolePlayingGameAssetType.GameObject)
      }).subscribe({
        next: (result: { tokenAssets: Array<IRolePlayingGameAsset>; objectAssets: Array<IRolePlayingGameAsset>; }) => {
          this.TokenAssets = result.tokenAssets;
          this.ObjectAssets = result.objectAssets;
          this.IsLoading = false;
        },
        error: () => {
          this.IsLoading = false;
        }
      });
   }

   /**
    * Gets the token assets for the role playing game.
    *
    * @memberof GameBoardActionsTokensComponent
    */
   ngOnInit(): void {
     this.GetTokenAssetsForRolePlayingGame();
   }

   /**
    * Creates an instance of GameBoardActionsComponent.
    * @param {RolePlayingGameService} RolePlayingGameService
    * @memberof GameBoardActionsComponent
    */
   constructor(
     protected readonly RolePlayingGameService: RolePlayingGameService,
     protected readonly ChangeDetectorRef: ChangeDetectorRef
    ) {}
}
