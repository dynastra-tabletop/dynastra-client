import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { OAuthModule } from 'angular-oauth2-oidc';
import { MockModule } from 'ng-mocks';
import { from, throwError } from 'rxjs';
import { MockAuthService } from 'spec/MockAuthService';
import { AuthService } from 'src/app/services/auth.service';
import { RolePlayingGameService } from 'src/app/services/role-playing-game.service';
import { RolePlayingGameAssetType } from 'src/lib/enums/role-playing-games.enums';
import { IRolePlayingGameAsset } from 'src/lib/interfaces/RolePlayingGame.interfaces';

import { GameBoardActionsComponent } from './game-board-actions.component';

describe('GameBoardActionsComponent', () => {
  let component: GameBoardActionsComponent;
  let fixture: ComponentFixture<GameBoardActionsComponent>;
  let tokenAssets: Array<IRolePlayingGameAsset> = [{
    AssetReferenceId: '',
    AssetType: RolePlayingGameAssetType.Token,
    CreatedBy: '',
    Description: '',
    FileName: '0.jpg',
    RolePlayingGameId: '',
    Title: '',
    DateAdded: new Date().toISOString(),
  }];
  let objectAssets: Array<IRolePlayingGameAsset> = [{
    AssetReferenceId: '',
    AssetType: RolePlayingGameAssetType.GameObject,
    CreatedBy: '',
    Description: '',
    FileName: '0.jpg',
    RolePlayingGameId: '',
    Title: '',
    DateAdded: new Date().toISOString(),
  }];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatDialogModule,
        MockModule(OAuthModule.forRoot()),
        RouterTestingModule,
      ],
      providers: [
        {
          provide: AuthService,
          useValue: new MockAuthService(),
        }
      ],
      declarations: [ GameBoardActionsComponent ]
    })
    .compileComponents();
  });


  it('should create', () => {
    fixture = TestBed.createComponent(GameBoardActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
  it('should initialise and retrieve both assets', () => {
    TestBed.overrideProvider(RolePlayingGameService, {
      useValue: {
        GetAssetsForGame: (id: string, type: RolePlayingGameAssetType) => {
          if (type === RolePlayingGameAssetType.GameObject) {
            return from([objectAssets]);
          }
          return from([tokenAssets]);
        }
      }
    });
    fixture = TestBed.createComponent(GameBoardActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component.ObjectAssets.length).toEqual(1);
    expect(component.TokenAssets.length).toEqual(1);
    expect(component.TokenAssets).toEqual(tokenAssets);
    expect(component.ObjectAssets).toEqual(objectAssets);
  });
  it("should correctly handle an error and end loading", () => {
    TestBed.overrideProvider(RolePlayingGameService, {
      useValue: {
        GetAssetsForGame: (id: string, type: RolePlayingGameAssetType) => {
          return throwError('Error');
        }
      }
    });
    const newFixture = TestBed.createComponent(GameBoardActionsComponent);
    const newComponent = newFixture.componentInstance;
    newFixture.detectChanges();
    expect(newComponent.IsLoading).toEqual(false);
  });
});
