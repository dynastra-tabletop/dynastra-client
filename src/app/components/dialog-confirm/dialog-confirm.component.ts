import {ChangeDetectorRef, Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.scss']
})
export class DialogConfirmComponent implements OnInit {

  /**
   * The topmost part of the dialog HTML.
   */
  public Title!: string;

  /**
   * The contents of the dialog box.
   */
  public Description!: string;

  /**
   * If true, the user has to type to confirm the name of the thing they are deleting.
   */
  public TypeToConfirm: boolean = false;

  /**
   * The text the user has to type to confirm the deletion.
   */
  public TypeRequiredForConfirm: string = "";

  /**
   * The title of the "confirm" (true) button
   */
  public ConfirmText: string = "Confirm";

  /**
   * The title of the "cancel" (false) button.
   */
  public CancelText: string = "Cancel";

  @Output()
  public OnDialogConfirm: EventEmitter<true> = new EventEmitter<true>();

  /**
   * Disables the form if the type is enabled.
   * @constructor
   */
  public get IsFormDisabled(): boolean {
    return this.TypeToConfirm && this.TypeToConfirmForm.invalid;
  }

  /**
   * The form control for typing
   */
  public TypeToConfirmFormControl: FormControl = new FormControl('', []);

  /**
   * Validates that a form element is equal to a given string (e.g. to confirm)
   * @param typeValue
   * @constructor
   * @protected
   */
  protected ValidateEqual(typeValue: string) {
    return (control: AbstractControl): {[key: string]: any} | null => {
      if (control.value && control.value !== typeValue) {
        return { 'notEqual': true };
      }
      return null;
    }
  }

  /**
   * The form that is used to force the user to type in to delete.
   */
  public TypeToConfirmForm: FormGroup = new FormGroup({});

  /**
   * Person clicked "yes" on the dialog.
   */
  public onAcceptClicked() {
    this.OnDialogConfirm.emit(true);
    this.MatDialogRef.close();
  }

  ngOnInit(): void {
    if (this.TypeToConfirm) {
      this.TypeToConfirmFormControl.addValidators(Validators.required);
      this.TypeToConfirmFormControl.addValidators(this.ValidateEqual(this.TypeRequiredForConfirm));
      this.TypeToConfirmForm.addControl("typeValue", this.TypeToConfirmFormControl);
      this.TypeToConfirmForm.updateValueAndValidity();
      this.ChangeDetector.detectChanges();
    }
  }

  /**
   * Creates a new instance of the dialog.
   * @param Ref
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    protected readonly MatDialogRef: MatDialogRef<DialogConfirmComponent>,
    protected readonly ChangeDetector: ChangeDetectorRef,
  ) {
    this.Title = data.Title;
    this.Description = data.Description;
    this.TypeToConfirm = data.TypeToConfirm !== undefined;
    this.TypeRequiredForConfirm = data.TypeToConfirmText;
    this.ConfirmText = data.ConfirmText;
    this.CancelText = data.CancelText;

  }
}
