import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogConfirmComponent } from './dialog-confirm.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {MockModule} from "ng-mocks";
import {OAuthModule} from "angular-oauth2-oidc";
import {ChangeDetectorRef} from "@angular/core";

describe('DialogConfirmComponent', () => {
  let component: DialogConfirmComponent;
  let fixture: ComponentFixture<DialogConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        MockModule(OAuthModule.forRoot()),
      ],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            Title: '',
            Description: '',
            TypeToConfirm: true,
            TypeToConfirmText: 'steamed-hams',
            ConfirmText: 'Confirm',
            CancelText: 'Cancel'
          }
        },
        {
          provide: MatDialogRef,
          useValue: {
            close: () => {}
          }
        },
        { provide: ChangeDetectorRef, useValue: {} }
      ],
      declarations: [ DialogConfirmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogConfirmComponent);
    component = fixture.componentInstance;
    component.TypeToConfirm = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should launch and require the user to confirm the name provided if set', () => {
    component.TypeToConfirmForm.controls['typeValue'].setValue('lolz');
    fixture.detectChanges();
    expect(component.TypeToConfirmForm.invalid).toBeTrue();
    expect(component.TypeToConfirmForm.controls['typeValue'].errors!['notEqual']).toEqual(true);
    component.TypeToConfirmForm.controls['typeValue'].setValue('steamed-hams');
    fixture.detectChanges();
    expect(component.TypeToConfirmForm.valid).toBeTrue();
  });
  it('should allow close without error', () => {
    expect(() => component.onAcceptClicked()).not.toThrow();
  });
});

