import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RolePlayingGamesListEditItemComponent } from './role-playing-games-list-edit-item.component';
import {IRolePlayingGame} from "../../../lib/interfaces/RolePlayingGame.interfaces";
import {SimpleChange} from "@angular/core";

describe('RolePlayingGamesListEditItemComponent', () => {
  let component: RolePlayingGamesListEditItemComponent;
  let fixture: ComponentFixture<RolePlayingGamesListEditItemComponent>;
  let completeRolePlayingGame: IRolePlayingGame;

  beforeAll(() => {
    completeRolePlayingGame = {
      Id: "",
      Name: "A test game",
      Description: "OMG A test game",
      ImageUrl: "https://cdn.britannica.com/53/195353-050-C5EFAA74/Che-Guevara.jpg",
      CreatedByUser: "",
      Characters: [],
      Users: []
    }
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RolePlayingGamesListEditItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RolePlayingGamesListEditItemComponent);
    component = fixture.componentInstance;
    component.RolePlayingGame = completeRolePlayingGame;
    fixture.detectChanges();
  });

  it('should create', ( ) => {
    expect(component).toBeTruthy();
  });

  it('should render an empty form until changes are parsed', () => {
    expect(component.name.value).toEqual('');
    expect(component.description.value).toEqual('');
    expect(component.imageURL.value).toEqual('');

    component.ngOnChanges({
      RolePlayingGame: new SimpleChange(
        null,
        completeRolePlayingGame,
        true,
      ),
    });
    expect(component.name.value).toEqual(completeRolePlayingGame.Name);
    expect(component.description.value).toEqual(completeRolePlayingGame.Description);
    expect(component.imageURL.value).toEqual(completeRolePlayingGame.ImageUrl);
  });
  it('should show errors for when the edit form has invalid values',() => {
    component.ngOnChanges({
      RolePlayingGame: new SimpleChange(
        null,
        {
          ...completeRolePlayingGame,
          Name: 'sm',
          Description: '',
          ImageUrl: 'smb://wut-not-supported.exe'
        },
        true,
      ),
    });
    expect(component.editForm.invalid).toBeTruthy();
    // JUSTIFICATION: If it errors, it'll fail the test, which is intentional, so why bother typechecking, etc?
    // @ts-ignore
    expect(component.editForm.controls["name"].errors['minlength']).not.toBeUndefined();
    // @ts-ignore
    expect(component.editForm.controls["description"].errors['required']).not.toBeUndefined();
    // @ts-ignore
    expect(component.editForm.controls["imageURL"].errors['pattern']).not.toBeUndefined();
  });
  it('should not show errors when the edit form has valid values', () => {
    component.ngOnChanges({
      RolePlayingGame: new SimpleChange(
        null,
        {
          ...completeRolePlayingGame,
          Name: 'I am so correct',
          Description: 'And this is so correct too',
          ImageUrl: 'https://cdn.britannica.com/53/195353-050-C5EFAA74/Che-Guevara.jpg'
        },
        true,
      ),
    });
    expect(component.editForm.valid).toBeTruthy();
  })
});
