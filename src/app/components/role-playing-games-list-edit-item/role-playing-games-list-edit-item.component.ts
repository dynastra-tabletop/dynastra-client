import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {IRolePlayingGame} from "../../../lib/interfaces/RolePlayingGame.interfaces";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-role-playing-games-list-edit-item',
  templateUrl: './role-playing-games-list-edit-item.component.html',
  styleUrls: ['./role-playing-games-list-edit-item.component.scss']
})
export class RolePlayingGamesListEditItemComponent implements OnChanges  {

  @Input()
  public RolePlayingGame!: IRolePlayingGame;

  @Output()
  public RolePlayingGameUpdated: EventEmitter<IRolePlayingGame> = new EventEmitter<IRolePlayingGame>();

  protected EditingRolePlayingGame!: Partial<IRolePlayingGame>;

  public get name() {
    return this.editForm.controls["name"];
  }

  public get description() {
    return this.editForm.controls["description"]
  }

  public get imageURL() {
    return this.editForm.controls["imageURL"];
  }

  public editForm: FormGroup = new FormGroup({
    name: new FormControl('',[
      Validators.minLength(4),
      Validators.required,
    ]),
    description: new FormControl('', [
      Validators.required,
    ]),
    imageURL: new FormControl('', [
      Validators.required,
      Validators.pattern('[(http(s)?):\\/\\/(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)'),
    ]),
  });

  protected updateFormOnGameLoaded(rolePlayingGame: IRolePlayingGame) {
    this.editForm.controls["name"].setValue(rolePlayingGame.Name);
    this.editForm.controls["description"].setValue(rolePlayingGame.Description);
    this.editForm.controls["imageURL"].setValue(rolePlayingGame.ImageUrl);
  }

  /**
   * Called when the input changes from nothing to a role playing game.
   * @param changes
   */
  public ngOnChanges(changes: SimpleChanges) {
    if (changes['RolePlayingGame'].currentValue !== null && changes['RolePlayingGame'].currentValue !== undefined) {
      const changed: IRolePlayingGame = changes['RolePlayingGame'].currentValue;
      this.updateFormOnGameLoaded(Object.assign({}, changed));
    }
  }

  protected onFormChanges(changes: { name: string; description: string; imageURL: string;  }) {
    if(!this.EditingRolePlayingGame) {
      this.EditingRolePlayingGame = {};
    }

    // Don't want there to be invalid objects sent via service to backend.
    this.EditingRolePlayingGame.Name = changes.name;
    this.EditingRolePlayingGame.Description = changes.description;
    this.EditingRolePlayingGame.ImageUrl = changes.imageURL;

    if(this.editForm.valid) {
      // Messy, but 30x faster than rest operator.
      const composited: IRolePlayingGame = Object.assign(
        Object.assign({}, this.RolePlayingGame),
        this.EditingRolePlayingGame,
      );
      this.RolePlayingGameUpdated.emit(composited);
    }
  }

  protected init(): void {
    this.editForm.valueChanges.subscribe({
      next: this.onFormChanges.bind(this),
    })
  }

  constructor() {
    this.init();
  }
}
