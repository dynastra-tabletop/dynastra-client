import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameBoardActionsGameBoardConfigComponent } from './game-board-actions-game-board-config.component';

describe('GameBoardActionsGameBoardConfigComponent', () => {
  let component: GameBoardActionsGameBoardConfigComponent;
  let fixture: ComponentFixture<GameBoardActionsGameBoardConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameBoardActionsGameBoardConfigComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardActionsGameBoardConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
