import { Component, OnInit } from '@angular/core';
import {GameBoardService} from "../../services/game-board.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-game-board-actions-game-board-config',
  templateUrl: './game-board-actions-game-board-config.component.html',
  styleUrls: ['./game-board-actions-game-board-config.component.scss']
})
export class GameBoardActionsGameBoardConfigComponent implements OnInit {
  /**
   * The config form.
   */
  public BoardConfigForm: FormGroup = new FormGroup({
    gridLineColor: new FormControl('', [
      Validators.required,
      Validators.pattern('#([0-9A-Za-z]){6}')
    ]),
    gridLineStyle: new FormControl('', [
      Validators.required
    ]),
    gridLineWidth: new FormControl('', [
      Validators.required
    ])
  });

  /**
   * Gets the grid line color.
   */
  public get gridLineColor() {
    return this.BoardConfigForm.get('gridLineColor');
  }

  /**
   * The style of the grid line.
   */
  public get gridLineStyle() {
    return this.BoardConfigForm.get('gridLineStyle');
  }

  /**
   * Gets the grid line width.
   */
  public get gridLineWidth() {
    return this.BoardConfigForm.get('gridLineWidth');
  }

  /**
   * Updates the board configuration.
   */
  public UpdateBoardConfiguration() {
    this.GameBoardService.updateSettings({
      gridSettings: {
        gridLineWidth: this.BoardConfigForm.get('gridLineWidth')?.value,
        gridLineStyle: this.BoardConfigForm.get('gridLineStyle')?.value,
        gridLineColor: this.BoardConfigForm.get('gridLineColor')?.value
      }
    });
  }

  /**
   * Updates the form with the settings from the service.
   * @protected
   */
  protected updateFormWithSettings(): void {
    const settings = this.GameBoardService.getSettings();
    this.BoardConfigForm.get('gridLineColor')?.setValue(settings.gridSettings?.gridLineColor);
    this.BoardConfigForm.get('gridLineStyle')?.setValue(settings.gridSettings?.gridLineStyle);
    this.BoardConfigForm.get('gridLineWidth')?.setValue(settings.gridSettings?.gridLineWidth);
  }

  /**
   * Calls the onInit hook
   */
  ngOnInit(): void {
    this.updateFormWithSettings();
  }

  /**
   * Creates a new instance of the config.
   * @param GameBoardService
   */
  constructor(protected GameBoardService: GameBoardService) { }
}
