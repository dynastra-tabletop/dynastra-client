import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedInUserDisplayComponent } from './logged-in-user-display.component';
import { GravatarModule } from 'ngx-gravatar';
import { MatMenuModule } from '@angular/material/menu';
import { not } from '@angular/compiler/src/output/output_ast';
describe('LoggedInUserDisplayComponent', () => {
  let component: LoggedInUserDisplayComponent;
  let fixture: ComponentFixture<LoggedInUserDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        GravatarModule,
        MatMenuModule,
      ],
      declarations: [ LoggedInUserDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedInUserDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should correctly display the layout with props provided', () => {
    component.Username = "DynastraTestUser";
    fixture.detectChanges();
    const userDisplayName = fixture.nativeElement.querySelector('#dynastra-logged-in-user-display-name');
    expect(userDisplayName.innerText).not.toEqual("");
    expect(userDisplayName.innerText).toEqual(component.Username);
  });
  it('should correctly create a gravatar with a given e-mail address', () => {
    component.EmailAddress = "test@dynastra.vtt";
    fixture.detectChanges();
    const gravatar = fixture.nativeElement.querySelector('#dynastra-logged-in-user-display-avatar img');
    expect(gravatar.src).toContain("www.gravatar.com");
  });
  it('should contain a gravatar if there is no e-mail address', () => {
    const gravatar = fixture.nativeElement.querySelector('#dynastra-logged-in-user-display-avatar img');
    expect(gravatar.src).toContain("www.gravatar.com");
  });
});
