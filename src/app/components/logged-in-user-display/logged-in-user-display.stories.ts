import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { Meta, Story, moduleMetadata } from '@storybook/angular';
import { OAuthModule } from 'angular-oauth2-oidc';
import { KonvaModule } from 'ng2-konva';
import { GravatarModule } from 'ngx-gravatar';
import { AuthService } from 'src/app/services/auth.service';
import { LoggedInUserDisplayComponent } from "./logged-in-user-display.component";

export default {
  title: 'Logged In User Display',
  component: LoggedInUserDisplayComponent,
  decorators: [
    moduleMetadata({
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
        AuthService,
      ],
      declarations: [],
      imports: [
        BrowserModule,
        RouterModule.forRoot([], { useHash: true }),
        KonvaModule,
        BrowserAnimationsModule,
        MatSidenavModule,
        MatExpansionModule,
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        MatMenuModule,
        HttpClientModule,
        MatProgressSpinnerModule,
        OAuthModule.forRoot(),
        GravatarModule,
      ],
    }),
  ]
} as Meta;

const Template: Story<LoggedInUserDisplayComponent> = (args) => ({
  props: args,
});

export const UserIsLoggedIn = Template.bind({});
UserIsLoggedIn.args = {
  EmailAddress: "test@dynastra.vtt",
  Username: "Baron Humboldt Von Shootyface"
};