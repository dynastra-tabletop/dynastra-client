import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
  selector: 'app-logged-in-user-display',
  templateUrl: './logged-in-user-display.component.html',
  styleUrls: ['./logged-in-user-display.component.scss']
})
export class LoggedInUserDisplayComponent {
  /**
   * The person's logged-in username.
   *
   * @type {string}
   * @memberof LoggedInUserDisplayComponent
   */
  @Input()
  public Username!: string;

  /**
   * The person's logged-in e-mail address.
   *
   * @type {string}
   * @memberof LoggedInUserDisplayComponent
   */
  @Input()
  public EmailAddress: string = "test@dynastra.vtt";

  /**
   * Gets the redirect URL from the OIDC issuer to redirect the user to.
   *
   * @type {string}
   * @memberof LoggedInUserDisplayComponent
   */
  @Input()
  public RedirectURL: string = "#";

  /**
   * When the user clicks to log out, notifies the parent component.
   *
   * @type {EventEmitter<void>}
   * @memberof LoggedInUserDisplayComponent
   */
  @Output()
  public Logout: EventEmitter<void> = new EventEmitter<void>();

  /**
   * The trigger for calling the menu programmatically.
   *
   * @type {MatMenuTrigger}
   * @memberof LoggedInUserDisplayComponent
   */
  @ViewChild('menuTrigger')
  public menuTrigger!: MatMenuTrigger;

  /**
   * Logs the user out of the UI (passes to the quick-login to do it)
   *
   * @memberof LoggedInUserDisplayComponent
   */
  public logout(): void {
    this.Logout.next();
  }
}
