import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectUserCharacterModalComponent } from './select-user-character-modal.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {MockProvider} from "ng-mocks";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {AuthService} from "../../services/auth.service";
import {MockAuthService} from "../../../../spec/MockAuthService";
import {CharactersService} from "../../services/characters.service";
import {ICharacter} from "../../../lib/interfaces/Character.interfaces";
import {from, Observable} from "rxjs";
import {RolePlayingGameService} from "../../services/role-playing-game.service";
import {MatSelectionList} from "@angular/material/list";
import {HarnessLoader} from "@angular/cdk/testing";
import {TestbedHarnessEnvironment} from "@angular/cdk/testing/testbed";

describe('SelectUserCharacterModalComponent', () => {
  let component: SelectUserCharacterModalComponent;
  let fixture: ComponentFixture<SelectUserCharacterModalComponent>;
  let userCharacters: Array<ICharacter> = [{
    Id: '1',
    UserId: '',
    CharacterName: '',
    CharacterAbout: '',
    CharacterImgUrl: '',
    IsPublic: true
  }, {
    Id: '2',
    UserId: '',
    CharacterName: '',
    CharacterAbout: '',
    CharacterImgUrl: '',
    IsPublic: true
  }];

  function normalSetup() {
    TestBed.overrideProvider(CharactersService, {
      useValue: {
        GetCharactersForUser: () => from([userCharacters])
      }
    });
    TestBed.overrideProvider(RolePlayingGameService, {
      useValue: {
        AddCharactersToRolePlayingGame: () => from([true]),
        RemoveCharactersFromRolePlayingGame: () => from([true])
      }
    })
    fixture = TestBed.createComponent(SelectUserCharacterModalComponent);
    component = fixture.componentInstance;
    component.SelectCharList = TestBed.createComponent(MatSelectionList).componentInstance;
    fixture.detectChanges();
  }

  function errorSetup() {
    TestBed.overrideProvider(CharactersService, {
      useValue: {
        GetCharactersForUser: () => new Observable((obs) => {
          obs.error('error');
        })
      }
    })
    fixture = TestBed.createComponent(SelectUserCharacterModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        MockProvider<typeof MAT_DIALOG_DATA>(MAT_DIALOG_DATA, {
          remove: false,
          existingCharacters: [{
            Id: '1',
            UserId: '',
            CharacterName: '',
            CharacterAbout: '',
            CharacterImgSrc: '',
            IsPublic: true
          }],
          rolePlayingGameId: '',
        } as any),
        {
          provide: MatDialogRef,
          useValue: {
            close: () => {}
          }
        }
      ],
      declarations: [ SelectUserCharacterModalComponent ]
    })
    .compileComponents();
    TestBed.overrideProvider(AuthService, { useValue : new MockAuthService() });
  });


  it('should create', () => {
    normalSetup();
    expect(component).toBeTruthy();
  });

  it('should load the characters on initialising', () => {
    normalSetup();
    expect(component.LoadedCharacters).toEqual(userCharacters);
  });

  it('should finish loading on error', () => {
    errorSetup();
    expect(component.IsLoading).toBeFalse();
  });

  it('should correctly add selected characters', () => {
      normalSetup();
      expect(() => component.AddCharactersToGame()).toThrow();
  });
});
