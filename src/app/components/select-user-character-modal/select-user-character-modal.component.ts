import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {CharactersService} from "../../services/characters.service";
import {ICharacter} from "../../../lib/interfaces/Character.interfaces";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {RolePlayingGameService} from "../../services/role-playing-game.service";
import {lastValueFrom} from "rxjs";
import {MatSelectionList} from "@angular/material/list";

@Component({
  selector: 'app-select-user-character-modal',
  templateUrl: './select-user-character-modal.component.html',
  styleUrls: ['./select-user-character-modal.component.scss'],
})
export class SelectUserCharacterModalComponent implements OnInit {

  /**
   * Is the REST request in progress?
   * @protected
   */
  protected IsRESTLoading: boolean | null = null;

  /**
   * The list element so we can see which elements are selected.
   */
  @ViewChild('characterList')
  public SelectCharList!: MatSelectionList;

  /**
   * The characters that are loaded into the component.
   * @protected
   */
  public LoadedCharacters: Array<ICharacter> = [];

  /**
   * The characters to display.
   */
  public DisplayCharacters: Array<ICharacter> | null = null;

  /**
   * Does the person have no more characters to add? (Or no characters?)
   * @constructor
   */
  public get HasNoCharactersToAdd(): boolean {
    return this.DisplayCharacters !== null && !this.IsLoading && this.DisplayCharacters.length === 0;
  }

  /**
   * Is the component currently loading?
   * @constructor
   */
  public get IsLoading(): boolean {
    return this.IsRESTLoading !== null && this.IsRESTLoading;
  }

  /**
   * Applies the operand based on the mode requested.
   * @param existingChar
   * @protected
   */
  protected applyOperand(existingChar: any) {
    if(this.data.remove) {
      return existingChar !== undefined;
    }
    return existingChar === undefined;
  }

  /**
   * Filters applicable users which already exist in the game.
   * @protected
   */
  protected filterApplicableUsers() {
    this.DisplayCharacters = this.LoadedCharacters.filter((char: ICharacter) => {
        const existingChar = this.data.existingCharacters.find((item: any) => item.CharacterId === char.Id);
        return this.applyOperand(existingChar);
    });
  }

  /**
   * Loads the user's characters.
   * @constructor
   * @protected
   */
  protected LoadUserCharacters(): void {
    this.IsRESTLoading = true;
    this.CharactersService.GetCharactersForUser()
      .subscribe({
        next: (characters: Array<ICharacter>) => {
          this.IsRESTLoading = false;
          this.LoadedCharacters = characters;
          this.filterApplicableUsers();
        },
        error: (err: any) => {
          this.IsRESTLoading = false;
        },
        complete: () => {
          this.IsRESTLoading = false;
        }
      });
  }

  /**
   * Removes characters from the game.
   * @constructor
   */
  public RemoveCharactersFromGame(): void {
    const selectedChars = this.SelectCharList.selectedOptions.selected;
    const charIds = selectedChars.map(ch => ch.value);

    this.IsRESTLoading = true;
    this.RolePlayingGameService.RemoveCharactersFromRolePlayingGame(
      this.data.rolePlayingGameId,
      charIds
    ).subscribe({
      next: () =>{
        this.IsRESTLoading = false;
        this.dialogRef.close();
      },
      error: () => {
        this.IsRESTLoading = false;
        this.dialogRef.close();
      },
      complete: () => {
        this.IsRESTLoading = false;
        this.dialogRef.close();
      }
    });
  }

  /**
   * Adds all the characters to game that were selected.
   * @constructor
   */
  public AddCharactersToGame(): void {
    const selectedChars = this.SelectCharList.selectedOptions.selected;
    const charIds = selectedChars.map(ch => ch.value);

    this.IsRESTLoading = true;
    this.RolePlayingGameService.AddCharactersToRolePlayingGame(
      this.data.rolePlayingGameId,
      charIds
    ).subscribe({
      next: () =>{
        this.IsRESTLoading = false;
        this.dialogRef.close();
      },
      error: () => {
        this.IsRESTLoading = false;
        this.dialogRef.close();
      },
      complete: () => {
        this.IsRESTLoading = false;
        this.dialogRef.close();
      }
    })
  }

  /**
   * Implements onInit event.
   */
  ngOnInit(): void {
    this.LoadUserCharacters();
  }

  /**
   * Creates a new instance of the character modal.
   * @param CharactersService
   */
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { existingCharacters: Array<ICharacter>; rolePlayingGameId: string; remove: boolean; },
    public readonly CharactersService: CharactersService,
    public readonly RolePlayingGameService: RolePlayingGameService,
    public dialogRef: MatDialogRef<SelectUserCharacterModalComponent>,
  ) {}
}
