import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameBoardRollLogComponent } from './game-board-roll-log.component';

describe('GameBoardRollLogComponent', () => {
  let component: GameBoardRollLogComponent;
  let fixture: ComponentFixture<GameBoardRollLogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameBoardRollLogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardRollLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
