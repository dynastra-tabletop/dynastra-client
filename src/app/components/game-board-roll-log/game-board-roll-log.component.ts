import {Component, Input, OnInit} from '@angular/core';
import {ActionService} from "../../services/action.service";
import {IGameActionResult, IGameActionRollResult} from "../../../lib/interfaces/Action.interfaces";

@Component({
  selector: 'app-game-board-roll-log',
  templateUrl: './game-board-roll-log.component.html',
  styleUrls: ['./game-board-roll-log.component.scss']
})
export class GameBoardRollLogComponent {
  /**
   * The actions received during this game session.
   */
  @Input()
  public RollHistory: Array<IGameActionRollResult> = [];
}
