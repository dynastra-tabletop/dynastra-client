import {Component, Input, OnInit} from '@angular/core';
import {ICharacter} from "../../../lib/interfaces/Character.interfaces";
import {MatDialog} from "@angular/material/dialog";
import {DialogConfirmComponent} from "../dialog-confirm/dialog-confirm.component";

@Component({
  selector: 'app-user-character-display',
  templateUrl: './user-character-display.component.html',
  styleUrls: ['./user-character-display.component.scss']
})
export class UserCharacterDisplayComponent {
  /**
   * The character loaded from the server.
   */
  @Input()
  public Character!: ICharacter;

  /**
   * Creates a new instance of the user character.
   * @param Dialog
   */
  constructor(
    public readonly Dialog: MatDialog
  ) { }
}
