import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCharacterDisplayComponent } from './user-character-display.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {MockModule} from "ng-mocks";
import {OAuthModule} from "angular-oauth2-oidc";
import {MatDialogModule} from "@angular/material/dialog";
import {MockAuthService} from "../../../../spec/MockAuthService";
import {AuthService, IAuthService} from "../../services/auth.service";

describe('UserCharacterDisplayComponent', () => {
  let component: UserCharacterDisplayComponent;
  let fixture: ComponentFixture<UserCharacterDisplayComponent>;
  let mockAuthService: IAuthService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatDialogModule,
        MockModule(OAuthModule.forRoot()),
      ],
      declarations: [ UserCharacterDisplayComponent ],
      providers: [MockAuthService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    mockAuthService = new MockAuthService();
    TestBed.overrideProvider(AuthService, { useValue : mockAuthService });
    fixture = TestBed.createComponent(UserCharacterDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
