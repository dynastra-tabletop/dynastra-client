import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { By } from '@angular/platform-browser';
import { RolePlayingGameAssetType } from 'src/lib/enums/role-playing-games.enums';
import { IRolePlayingGameAsset } from 'src/lib/interfaces/RolePlayingGame.interfaces';

import { GameBoardActionsMapObjectsComponent } from './game-board-actions-map-objects.component';

describe('GameBoardActionsMapObjectsComponent', () => {
  let component: GameBoardActionsMapObjectsComponent;
  let fixture: ComponentFixture<GameBoardActionsMapObjectsComponent>;
  let assets: Array<IRolePlayingGameAsset> = [{
    AssetReferenceId: '',
    AssetType: RolePlayingGameAssetType.GameObject,
    RolePlayingGameId: '',
    FileName: '0.jpg',
    DateAdded: new Date().toISOString(),
    Title: 'It is a title 1',
    Description: 'It is a description lol',
    CreatedBy: ''
  }, {
    AssetReferenceId: '',
    AssetType: RolePlayingGameAssetType.GameObject,
    RolePlayingGameId: '',
    FileName: '0.jpg',
    DateAdded: new Date().toISOString(),
    Title: 'It is a title 2',
    Description: 'It is a description lol',
    CreatedBy: ''
  }];
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatDialogModule
      ],
      declarations: [ GameBoardActionsMapObjectsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardActionsMapObjectsComponent);
    component = fixture.componentInstance;
    component.Assets = assets;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should list out displayed assets when initialised', () => {
    const assetItems = fixture.debugElement.queryAll(By.css("app-game-board-actions-asset-item"));
    expect(assetItems.length).toEqual(assets.length);
    expect(assetItems.length).not.toBeLessThan(assets.length);
    expect(assetItems.length).not.toBeGreaterThan(assets.length);
  });
  it('should correctly allow for the dialog to be opened via click', () => {
    try {
      fixture.elementRef.nativeElement.querySelector("#game-board-map-object-upload-button").click();
      fixture.detectChanges();
      expect(true).toBeTrue();
    } catch (err) {
      expect(false).toEqual(true);
    }

  });
});
