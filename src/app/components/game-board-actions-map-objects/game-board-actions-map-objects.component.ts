import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RolePlayingGameAssetType } from 'src/lib/enums/role-playing-games.enums';
import { IRolePlayingGameAsset } from 'src/lib/interfaces/RolePlayingGame.interfaces';

import { GameBoardUploadModalComponent } from '../game-board-upload-modal/game-board-upload-modal.component';

@Component({
  selector: 'app-game-board-actions-map-objects',
  templateUrl: './game-board-actions-map-objects.component.html',
  styleUrls: ['./game-board-actions-map-objects.component.scss']
})
export class GameBoardActionsMapObjectsComponent {
  /**
   * The game assets we're displaying.
   *
   * @type {Array<IRolePlayingGameAsset>}
   * @memberof GameBoardActionsMapObjectsComponent
   */
  @Input()
  public Assets!: Array<IRolePlayingGameAsset>;

  /**
   * The ID of the game session.
   *
   * @type {string}
   * @memberof GameBoardActionsMapObjectsComponent
   */
  @Input()
  public GameSessionId!: string;

  /**
   * Opens the modal for the upload.
   *
   * @memberof GameBoardActionsMapObjectsComponent
   */
  public OpenModal(): void {
    this.dialog.open(GameBoardUploadModalComponent, {
      data: {
        sessionId: this.GameSessionId || '',
        assetType: RolePlayingGameAssetType.GameObject,
      },
      minWidth: 600,
      minHeight: 600,
      disableClose: true,
    });
  }

  /**
   * Creates an instance of GameBoardActionsMapObjectsComponent.
   * @param {MatDialog} dialog
   * @memberof GameBoardActionsMapObjectsComponent
   */
  constructor(protected readonly dialog: MatDialog) {}
}
