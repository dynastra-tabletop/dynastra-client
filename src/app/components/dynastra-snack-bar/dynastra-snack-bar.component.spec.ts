import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

import { DynastraSnackBarComponent } from './dynastra-snack-bar.component';

describe('DynastraSnackBarComponent', () => {
  let component: DynastraSnackBarComponent;
  let fixture: ComponentFixture<DynastraSnackBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DynastraSnackBarComponent ],
      providers: [
        {
          provide: MAT_SNACK_BAR_DATA,
          useValue: {}
        }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynastraSnackBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
