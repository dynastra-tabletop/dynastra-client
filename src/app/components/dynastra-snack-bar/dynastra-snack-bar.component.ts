import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
  selector: 'app-dynastra-snack-bar',
  templateUrl: './dynastra-snack-bar.component.html',
  styleUrls: ['./dynastra-snack-bar.component.scss']
})
export class DynastraSnackBarComponent {
  public header!: string;
  public message!: string;
  public icon!: string;

  /**
   * Creates an instance of DynastraSnackBarComponent.
   * @param {string} data
   * @memberof DynastraSnackBarComponent
   */
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: { icon: string; header: string; message: string; }) {
    this.header = data.header;
    this.message = data.message;
    this.icon = data.icon;
  }
}
