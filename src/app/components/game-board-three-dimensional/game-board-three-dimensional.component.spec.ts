import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameBoardThreeDimensionalComponent } from './game-board-three-dimensional.component';

describe('GameBoardThreeDimensionalComponent', () => {
  let component: GameBoardThreeDimensionalComponent;
  let fixture: ComponentFixture<GameBoardThreeDimensionalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameBoardThreeDimensionalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardThreeDimensionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
