import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { Meta, Story, moduleMetadata } from '@storybook/angular';
import { OAuthModule } from 'angular-oauth2-oidc';
import { KonvaModule } from 'ng2-konva';
import { AuthService } from 'src/app/services/auth.service';
import { LoggedOutUserDisplayComponent } from './logged-out-user-display.component';

export default {
  title: 'Logged Out User Display',
  component: LoggedOutUserDisplayComponent,
  decorators: [
    moduleMetadata({
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
        AuthService,
      ],
      declarations: [],
      imports: [
        BrowserModule,
        RouterModule.forRoot([], { useHash: true }),
        KonvaModule,
        BrowserAnimationsModule,
        MatSidenavModule,
        MatExpansionModule,
        MatButtonModule,
        MatIconModule,
        MatToolbarModule,
        MatMenuModule,
        HttpClientModule,
        MatProgressSpinnerModule,
        OAuthModule.forRoot(),
      ],
    }),
  ]
} as Meta;

const Template: Story<LoggedOutUserDisplayComponent> = (args) => ({
  props: args,
});

export const UserIsLoggedOut = Template.bind({});
UserIsLoggedOut.args = {};