import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedOutUserDisplayComponent } from './logged-out-user-display.component';

describe('LoggedOutUserDisplayComponent', () => {
  let component: LoggedOutUserDisplayComponent;
  let fixture: ComponentFixture<LoggedOutUserDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoggedOutUserDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedOutUserDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display a login button', () => {
    const loginButton = fixture.nativeElement.querySelector('#dynastra-logged-out-user-display-logout');
    expect(loginButton).not.toBeNull();
    expect(loginButton).not.toBeUndefined();
  });
});
