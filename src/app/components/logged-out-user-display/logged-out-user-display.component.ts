import { Component, EventEmitter, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-logged-out-user-display',
  templateUrl: './logged-out-user-display.component.html',
  styleUrls: ['./logged-out-user-display.component.scss']
})
export class LoggedOutUserDisplayComponent {
  /**
   * The e-mail address used for the avatar placeholder.
   *
   * @type {string}
   * @memberof LoggedOutUserDisplayComponent
   */
  public PlaceholderEmail: string = "dynastra@dynastra.vtt";

  /**
   * The user is logging out (passed to the quick-login component)
   *
   * @type {EventEmitter<void>}
   * @memberof LoggedOutUserDisplayComponent
   */
  @Output()
  public Login: EventEmitter<void> = new EventEmitter<void>();

  /**
   * Logs the user in to the interface, calls the quick-login component.
   *
   * @memberof LoggedOutUserDisplayComponent
   */
  public login(): void {
    this.Login.next();
  }
}

