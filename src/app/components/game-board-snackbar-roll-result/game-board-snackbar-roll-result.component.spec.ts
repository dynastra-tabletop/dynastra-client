import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameBoardSnackbarRollResultComponent } from './game-board-snackbar-roll-result.component';
import {MAT_SNACK_BAR_DATA} from "@angular/material/snack-bar";
import {IGameActionResult} from "../../../lib/interfaces/Action.interfaces";

let actionResult: IGameActionResult = {
  gameAction: 'RollDice',
  gameActionResult: {
    title: 'Test title',
    subTitle: 'A test subtitle',
    rollResults: [{
      appliesToResultCalculation: true,
      rollDiceType: 'DiceTerm.d20',
      rollValue: 20
    }]
  }
};

describe('GameBoardSnackbarRollResultComponent', () => {
  let component: GameBoardSnackbarRollResultComponent;
  let fixture: ComponentFixture<GameBoardSnackbarRollResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [{
        provide: MAT_SNACK_BAR_DATA,
        useValue: {
          actionResult,
        }
      }],
      declarations: [ GameBoardSnackbarRollResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardSnackbarRollResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
