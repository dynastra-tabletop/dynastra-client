import {Component, Inject, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA} from "@angular/material/snack-bar";
import {
  IGameActionDisplayResult,
  IGameActionResult,
  IGameActionRollResult
} from "../../../lib/interfaces/Action.interfaces";

@Component({
  selector: 'app-game-board-snackbar-roll-result',
  templateUrl: './game-board-snackbar-roll-result.component.html',
  styleUrls: ['./game-board-snackbar-roll-result.component.scss']
})
export class GameBoardSnackbarRollResultComponent {

  /**
   * The action result.
   */
  public ActionResult!: IGameActionResult<IGameActionRollResult>;

  /**
   * The results from the service but in a displayable format.
   * @constructor
   */
  public get RollResults(): Array<IGameActionDisplayResult> {
    return this.ActionResult.gameActionResult.rollResults
        .map((result) => {
          const diceValue = result.rollDiceType.replace(/(DiceTerm.)/gi, '');
          return {
            diceValue,
            diceValueClassName: `df-${diceValue}-${result.rollValue}`,
          };
       });
  }

  /**
   * Creates a new snackbar component.
   * @param data
   */
  constructor(
    @Inject(MAT_SNACK_BAR_DATA) data : { actionResult: IGameActionResult<IGameActionRollResult> }
  ) {
    this.ActionResult = data.actionResult
  }
}
