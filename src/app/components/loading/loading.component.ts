import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent {
  /**
   * The context for the loading.
   *
   * @type {string}
   * @memberof LoadingComponent
   */
  @Input()
  public Context!: string;

}
