import {Component, Input, OnInit} from '@angular/core';
import {IGameActionRollResult, IGameActionRollResultIndividual} from "../../../lib/interfaces/Action.interfaces";
import { toNumber } from "lodash";
const _ = {
  toNumber
};
@Component({
  selector: 'app-dice-roll',
  templateUrl: './dice-roll.component.html',
  styleUrls: ['./dice-roll.component.scss']
})
export class DiceRollComponent implements OnInit {
  /**
   * The display for the roll result.
   * @constructor
   */
  public get RollResultDisplay(): string {
    const itemResults: string = this.RollResult.rollResults.reduce((iter: string, n: IGameActionRollResultIndividual, i: number) => {
      if (i === 0) {
        iter += ` ${n.rollValue}`;
        return iter;
      }

      iter += ` + ${n.rollValue}`;
      return iter;
    }, '');

    if (this.RollResult.rollResults.length === 1) {
      return `${this.RollResult.humanReadableRollResult}`;
    }

    return `${itemResults} = ${this.RollResult.humanReadableRollResult}`;
  }

  /**
   * The date this was rolled on.
   */
  @Input()
  public RollResult!: IGameActionRollResult;

  /**
   * The result of the parsed roll
   */
  public result: Array<{ modifier: number; dice: number; className: string; }> = [];

  /**
   * Generates roll classes.
   * @constructor
   * @protected
   */
  protected GenerateRollClasses() {
    const converted = this.RollResult.rollResults.map((result: IGameActionRollResultIndividual) => {
      const diceName = result.rollDiceType.replace(/(DiceTerm.d)/gi, '')
      const className = `df-d${diceName}-${result.rollValue}`;
      return {
        modifier: result.rollValue,
        dice: _.toNumber(diceName),
        className
      }
    });
    this.result = converted;
  }

  /**
   * When the class is initialised.
   */
  ngOnInit(): void {
    this.GenerateRollClasses();
  }
}
