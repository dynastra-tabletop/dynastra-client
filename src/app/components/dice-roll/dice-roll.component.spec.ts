import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiceRollComponent } from './dice-roll.component';

let rollResult = {
  id: "",
  title: "Some title",
  subTitle: "Some subtitle",
  dateRolled: new Date().toISOString(),
  requestedRoll: "2d10+10",
  rollResults: [{
    appliesToResultCalculation: true,
    rollDiceType: '10',
    rollValue: 20
  }],
  humanReadableRollResult: "10+10=20",
  rollResult: 20
};

describe('DiceRollComponent', () => {
  let component: DiceRollComponent;
  let fixture: ComponentFixture<DiceRollComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiceRollComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiceRollComponent);
    component = fixture.componentInstance;

    component.RollResult = rollResult;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should generate out the dice roll based on the roll result', () => {
    component.RollResult = {
      id: "",
      title: "Some title",
      subTitle: "Some subtitle",
      dateRolled: new Date().toISOString(),
      requestedRoll: "2d10+10",
      rollResults: [{
        appliesToResultCalculation: true,
        rollDiceType: '10',
        rollValue: 20
      }],
      humanReadableRollResult: "10 + 10 = 10+10=20",
      rollResult: 20
    };
    fixture.detectChanges();
    const rollTitleResult = fixture.nativeElement.querySelector("h3.roll-title-result");
    const rollTitleSubTitle = fixture.nativeElement.querySelector("h4.roll-title-subtitle");
    const rollResultDisplay = fixture.nativeElement.querySelector(".result-body .result-display");
    const requestedRoll = fixture.nativeElement.querySelector(".result-body .roll");
    expect(rollTitleResult.innerText).toEqual(component.RollResult.title);
    expect(rollTitleSubTitle.innerText).toEqual(component.RollResult.subTitle);
    expect(rollResultDisplay.innerText).toEqual(component.RollResult.humanReadableRollResult);
    expect(requestedRoll.innerText).toEqual(component.RollResult.requestedRoll);
  });

  it('should generate out a multi-array dice roll', () => {
    component.RollResult = {
      id: "",
      title: "Some title",
      subTitle: "Some subtitle",
      dateRolled: new Date().toISOString(),
      requestedRoll: "10 + 10 = 10+10=20",
      rollResults: [{
        appliesToResultCalculation: true,
        rollDiceType: '10',
        rollValue: 10
      },{
        appliesToResultCalculation: true,
        rollDiceType: '10',
        rollValue: 10
      }],
      humanReadableRollResult: "10 + 10 = 10+10=20",
      rollResult: 20
    };
    fixture.detectChanges();
    const rollTitleResult = fixture.nativeElement.querySelector("h3.roll-title-result");
    const rollTitleSubTitle = fixture.nativeElement.querySelector("h4.roll-title-subtitle");
    const requestedRoll = fixture.nativeElement.querySelector(".result-body .roll");
    expect(rollTitleResult.innerText).toEqual(component.RollResult.title);
    expect(rollTitleSubTitle.innerText).toEqual(component.RollResult.subTitle);
    expect(requestedRoll.innerText).toEqual(component.RollResult.requestedRoll);
  });
});
