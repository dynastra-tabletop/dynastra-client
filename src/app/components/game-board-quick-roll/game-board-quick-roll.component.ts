import {Component, Input, OnInit} from '@angular/core';
import {ActionService} from "../../services/action.service";

@Component({
  selector: 'app-game-board-quick-roll',
  templateUrl: './game-board-quick-roll.component.html',
  styleUrls: ['./game-board-quick-roll.component.scss'],
  providers: [
    ActionService
  ]
})
export class GameBoardQuickRollComponent {

  public static TITLE: string = 'Quick roll';
  public static SUBTITLE: string = 'Single dice roll';

  /**
   * The ID for the game session.
   */
  @Input()
  public GameSessionId!: string;

  /**
   * Display the full quick roll display?
   */
  public DisplayItems: boolean = true;

  /**
   * Toggles the display.
   */
  public toggleContractDisplay(): void {
    this.DisplayItems = !this.DisplayItems;
  }

  /**
   * Performs a dice roll.
   * @param rollText
   * @constructor
   */
  public PerformRoll(rollText: string) {
    this.ActionService.requestDiceRoll(this.GameSessionId, GameBoardQuickRollComponent.TITLE, GameBoardQuickRollComponent.SUBTITLE, rollText);
  }

  /**
   * Creates a new instance of the quick roll component.
   * @param ActionService
   */
  constructor(protected ActionService: ActionService) { }
}
