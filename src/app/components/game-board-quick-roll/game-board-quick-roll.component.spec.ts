import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameBoardQuickRollComponent } from './game-board-quick-roll.component';
import {MockModule, MockProvider} from "ng-mocks";
import {OAuthModule} from "angular-oauth2-oidc";
import {ActivatedRoute} from "@angular/router";
import {from} from "rxjs";
import {ActionService} from "../../services/action.service";
import {AuthService} from "../../services/auth.service";
import {MockAuthService} from "../../../../spec/MockAuthService";
import {MAT_SNACK_BAR_DATA, MatSnackBar} from "@angular/material/snack-bar";

describe('GameBoardQuickRollComponent', () => {
  let component: GameBoardQuickRollComponent;
  let fixture: ComponentFixture<GameBoardQuickRollComponent>;
  let mockAuthService: MockAuthService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MockModule(OAuthModule.forRoot()),
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: from([{
              get: (val: string = 'no-error') => {
                if (val === 'error') {
                  return 'error';
                }
                return '00000000-0000-0000-0000-000000000000';
              }
            }]),
          },
        },
        {
          provide: MAT_SNACK_BAR_DATA,
          useValue: {}
        },
        MockProvider(ActionService)
      ],
      declarations: [ GameBoardQuickRollComponent ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    mockAuthService = new MockAuthService();
    TestBed.overrideProvider(AuthService, { useValue : mockAuthService });
    fixture = TestBed.createComponent(GameBoardQuickRollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
