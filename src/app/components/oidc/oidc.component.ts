import { Component } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { AuthService } from 'src/app/services/auth.service';
import noop from "lodash.noop";

@Component({
  selector: 'app-oidc',
  templateUrl: './oidc.component.html',
  styleUrls: ['./oidc.component.scss']
})
export class OidcComponent {
  /**
   * Initialises the OIDC configuration.
   *
   * @protected
   * @memberof OidcComponent
   */
  protected init(): void {
    this.AuthService.initAuth()
      .then(noop);
  }

  /**
   * Creates an instance of OidcComponent.
   * @param {OAuthService} OAuthService
   * @memberof OidcComponent
   */
  constructor(private readonly AuthService: AuthService) {
    this.init();
  }
}
