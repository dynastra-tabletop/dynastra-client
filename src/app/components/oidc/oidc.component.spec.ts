import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OidcComponent } from './oidc.component';
import {OAuthModule} from "angular-oauth2-oidc";
import {HttpClientModule} from "@angular/common/http";
import {RouterTestingModule} from "@angular/router/testing";
import {MockModule} from "ng-mocks";
import {AuthService} from "../../services/auth.service";
import {OidcAwaitingComponent} from "../oidc-awaiting/oidc-awaiting.component";

describe('OidcComponent', () => {
  let component: OidcComponent;
  let fixture: ComponentFixture<OidcComponent>;

  beforeEach(async () => {
    TestBed.overrideProvider(AuthService, {
      useValue: {
        initAuth: () => Promise.resolve(),
      }
    })
    await TestBed.configureTestingModule({
      imports: [
        MockModule(OAuthModule.forRoot()),
        HttpClientModule,
        RouterTestingModule,
      ],
      declarations: [ OidcComponent, OidcAwaitingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OidcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
