import { DragDropModule } from '@angular/cdk/drag-drop';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { OAuthModule } from 'angular-oauth2-oidc';
import { KonvaModule } from 'ng2-konva';
import { FilePickerModule } from 'ngx-awesome-uploader';
import { GravatarModule } from 'ngx-gravatar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentStatusDisplayComponent } from './components/component-status-display/component-status-display.component';
import { DialogConfirmComponent } from './components/dialog-confirm/dialog-confirm.component';
import { DiceRollComponent } from './components/dice-roll/dice-roll.component';
import { DynastraSnackBarComponent } from './components/dynastra-snack-bar/dynastra-snack-bar.component';
import {
  GameBoardActionsAssetItemComponent,
} from './components/game-board-actions-asset-item/game-board-actions-asset-item.component';
import {
  GameBoardActionsMapObjectsComponent,
} from './components/game-board-actions-map-objects/game-board-actions-map-objects.component';
import { GameBoardActionsTokensComponent } from './components/game-board-actions-tokens/game-board-actions-tokens.component';
import { GameBoardActionsComponent } from './components/game-board-actions/game-board-actions.component';
import { GameBoardChatEmojisComponent } from './components/game-board-chat-emojis/game-board-chat-emojis.component';
import { GameBoardChatItemComponent } from './components/game-board-chat-item/game-board-chat-item.component';
import { GameBoardChatComponent } from './components/game-board-chat/game-board-chat.component';
import { GameBoardQuickRollComponent } from './components/game-board-quick-roll/game-board-quick-roll.component';
import { GameBoardRollLogComponent } from './components/game-board-roll-log/game-board-roll-log.component';
import {
  GameBoardSnackbarRollResultComponent,
} from './components/game-board-snackbar-roll-result/game-board-snackbar-roll-result.component';
import {
  GameBoardThreeDimensionalComponent,
} from './components/game-board-three-dimensional/game-board-three-dimensional.component';
import {
  GameBoardTwoDimensionalComponent,
} from './components/game-board-two-dimensional/game-board-two-dimensional.component';
import { GameBoardUploadModalComponent } from './components/game-board-upload-modal/game-board-upload-modal.component';
import { HeaderComponent } from './components/header/header.component';
import { LoadingComponent } from './components/loading/loading.component';
import { LoggedInUserDisplayComponent } from './components/logged-in-user-display/logged-in-user-display.component';
import { LoggedOutUserDisplayComponent } from './components/logged-out-user-display/logged-out-user-display.component';
import { LoggedOutComponent } from './components/logged-out/logged-out.component';
import { LoginErrorComponent } from './components/login-error/login-error.component';
import { OidcAwaitingComponent } from './components/oidc-awaiting/oidc-awaiting.component';
import { OidcPostLoginComponent } from './components/oidc-post-login/oidc-post-login.component';
import { OidcComponent } from './components/oidc/oidc.component';
import { QuickLoginDisplayComponent } from './components/quick-login-display/quick-login-display.component';
import {
  RolePlayingGameCharacterComponent,
} from './components/role-playing-game-character/role-playing-game-character.component';
import {
  RolePlayingGameCharactersComponent,
} from './components/role-playing-game-characters/role-playing-game-characters.component';
import {
  RolePlayingGamesCreateModalComponent,
} from './components/role-playing-games-create-modal/role-playing-games-create-modal.component';
import {
  RolePlayingGamesListDisplayItemComponent,
} from './components/role-playing-games-list-display-item/role-playing-games-list-display-item.component';
import {
  RolePlayingGamesListDisplayComponent,
} from './components/role-playing-games-list-display/role-playing-games-list-display.component';
import {
  RolePlayingGamesListEditItemComponent,
} from './components/role-playing-games-list-edit-item/role-playing-games-list-edit-item.component';
import {
  RolePlayingGamesListItemComponent,
} from './components/role-playing-games-list-item/role-playing-games-list-item.component';
import { RolePlayingGamesSearchComponent } from './components/role-playing-games-search/role-playing-games-search.component';
import {
  SelectUserCharacterModalComponent,
} from './components/select-user-character-modal/select-user-character-modal.component';
import { UserCharacterDisplayComponent } from './components/user-character-display/user-character-display.component';
import { CharacterCreateComponent } from './views/character-create/character-create.component';
import { CharacterComponent } from './views/character/character.component';
import { GameBoardComponent } from './views/game-board/game-board.component';
import { RolePlayingGameComponent } from './views/role-playing-game/role-playing-game.component';
import { RolePlayingGamesComponent } from './views/role-playing-games/role-playing-games.component';
import { UserCharactersComponent } from './views/user-characters/user-characters.component';
import { UserDashboardComponent } from './views/user-dashboard/user-dashboard.component';
import { WelcomeComponent } from './views/welcome/welcome.component';
import { GameBoardActionsGameBoardConfigComponent } from './components/game-board-actions-game-board-config/game-board-actions-game-board-config.component';
import {NgxMatColorPickerModule} from "@angular-material-components/color-picker";
import {MatSliderModule} from "@angular/material/slider";

@NgModule({
  declarations: [
    AppComponent,
    GameBoardTwoDimensionalComponent,
    GameBoardThreeDimensionalComponent,
    GameBoardActionsComponent,
    GameBoardActionsTokensComponent,
    HeaderComponent,
    LoggedInUserDisplayComponent,
    OidcComponent,
    OidcPostLoginComponent,
    OidcAwaitingComponent,
    LoggedOutComponent,
    LoginErrorComponent,
    LoggedOutUserDisplayComponent,
    QuickLoginDisplayComponent,
    RolePlayingGamesComponent,
    RolePlayingGamesListDisplayComponent,
    RolePlayingGamesListDisplayItemComponent,
    RolePlayingGamesSearchComponent,
    RolePlayingGamesCreateModalComponent,
    RolePlayingGamesListItemComponent,
    RolePlayingGamesListEditItemComponent,
    UserDashboardComponent,
    UserCharactersComponent,
    UserCharacterDisplayComponent,
    CharacterComponent,
    DialogConfirmComponent,
    WelcomeComponent,
    CharacterCreateComponent,
    RolePlayingGameComponent,
    RolePlayingGameCharactersComponent,
    RolePlayingGameCharacterComponent,
    SelectUserCharacterModalComponent,
    ComponentStatusDisplayComponent,
    GameBoardComponent,
    GameBoardRollLogComponent,
    DiceRollComponent,
    GameBoardQuickRollComponent,
    GameBoardSnackbarRollResultComponent,
    ComponentStatusDisplayComponent,
    UserCharacterDisplayComponent,
    GameBoardChatComponent,
    GameBoardChatEmojisComponent,
    GameBoardChatItemComponent,
    LoadingComponent,
    DynastraSnackBarComponent,
    GameBoardActionsAssetItemComponent,
    GameBoardActionsMapObjectsComponent,
    GameBoardUploadModalComponent,
    GameBoardActionsGameBoardConfigComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    KonvaModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatExpansionModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatMenuModule,
    MatTooltipModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    GravatarModule,
    MatButtonToggleModule,
    MatCardModule,
    MatChipsModule,
    MatInputModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatDialogModule,
    MatListModule,
    MatBadgeModule,
    MatSnackBarModule,
    MatIconModule,
    MatSnackBarModule,
    OAuthModule.forRoot(),
    PickerModule,
    DragDropModule,
    FilePickerModule,
    MatSliderModule
  ],
  providers: [{provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 3000}}],
  bootstrap: [AppComponent]
})
export class AppModule { }
