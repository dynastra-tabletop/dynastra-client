import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameBoardTwoDimensionalComponent } from './components/game-board-two-dimensional/game-board-two-dimensional.component';
import { LoggedOutComponent } from './components/logged-out/logged-out.component';
import { LoginErrorComponent } from './components/login-error/login-error.component';
import { OidcPostLoginComponent } from './components/oidc-post-login/oidc-post-login.component';
import { OidcComponent } from './components/oidc/oidc.component';
import { IsLoggedInGuard } from './guards/is-logged-in.guard';
import { IsNotLoggedInGuard } from './guards/is-not-logged-in.guard';
import { RolePlayingGamesComponent } from './views/role-playing-games/role-playing-games.component';
import {UserDashboardComponent} from "./views/user-dashboard/user-dashboard.component";
import {UserCharactersComponent} from "./views/user-characters/user-characters.component";
import {CharacterComponent} from "./views/character/character.component";
import {WelcomeComponent} from "./views/welcome/welcome.component";
import {CharacterCreateComponent} from "./views/character-create/character-create.component";
import {RolePlayingGameComponent} from "./views/role-playing-game/role-playing-game.component";
import {GameBoardComponent} from "./views/game-board/game-board.component";
import { RolePlayingGameGuard } from './guards/role-playing-game.guard';

const routes: Routes = [{
  path: "",
  component: WelcomeComponent,
  canActivate: [IsLoggedInGuard],
},{
  path: "role-playing-games",
  component: RolePlayingGamesComponent,
  canActivate: [IsLoggedInGuard],
},{
  path: "characters/:character-id",
  component: CharacterComponent,
  canActivate: [IsLoggedInGuard],
},{
  path: "me",
  component: UserDashboardComponent,
  canActivate: [IsLoggedInGuard]
},{
  path: "me/my-characters",
  component: UserCharactersComponent,
  canActivate: [IsLoggedInGuard],
}, {
  path: "me/create-character",
  component: CharacterCreateComponent,
  canActivate: [IsLoggedInGuard]
},{
  path: "game/:role-playing-game-id",
  component: RolePlayingGameComponent,
  canActivate: [IsLoggedInGuard]
}, {
  path: "game-board/:role-playing-game-session-id",
  component: GameBoardComponent,
  canActivate: [IsLoggedInGuard, RolePlayingGameGuard],
  children: [{
      path: ":game-board-type",
      canActivate: [IsLoggedInGuard],
      children: [{
          path: "**",
          component: GameBoardTwoDimensionalComponent,
          pathMatch: 'full',
      }],
      pathMatch: 'full'
  }]
}, {
  path: "login",
  component: OidcComponent,
},{
  path: "post-login",
  component: OidcPostLoginComponent,
}, {
  path: "logged-out",
  component: LoggedOutComponent,
  canActivate: [IsNotLoggedInGuard]
}, {
  path: "login-error",
  component: LoginErrorComponent,
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
