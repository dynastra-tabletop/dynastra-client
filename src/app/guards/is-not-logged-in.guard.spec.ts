import { TestBed } from '@angular/core/testing';

import { IsNotLoggedInGuard } from './is-not-logged-in.guard';
import {RouterTestingModule} from "@angular/router/testing";
import {OAuthModule} from "angular-oauth2-oidc";
import {HttpClientModule} from "@angular/common/http";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {AuthService} from "../services/auth.service";

describe('IsNotLoggedInGuard', () => {
  let guard: IsNotLoggedInGuard;
  let mockAuthService: { isLoggedIn: boolean } = {
    isLoggedIn: false,
  };

  beforeEach(() => {
    TestBed.overrideProvider(AuthService, {
      useValue: mockAuthService,
    });
    TestBed.configureTestingModule({
      imports: [
        OAuthModule.forRoot(),
        RouterTestingModule,
        HttpClientModule,
      ]
    });
    guard = TestBed.inject(IsNotLoggedInGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
  it('should correctly guard the URL if the user is not logged in', () => {
    expect(() => guard.canActivate(
      {} as unknown as ActivatedRouteSnapshot,
      {} as unknown as RouterStateSnapshot,
    )).not.toThrow();

    mockAuthService.isLoggedIn = true;

    expect(() => guard.canActivate(
      {} as unknown as ActivatedRouteSnapshot,
      {} as unknown as RouterStateSnapshot,
    )).not.toThrow();
  })
});
