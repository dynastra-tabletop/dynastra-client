import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { RolePlayingGameService } from '../services/role-playing-game.service';

@Injectable({
  providedIn: 'root'
})
export class RolePlayingGameGuard implements CanActivate {
  /**
   * Checks to see if the game exists.
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @return {*}  {(boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree>)}
   * @memberof RolePlayingGameGuard
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.RolePlayingGameService.DoesRolePlayingGameExist(route.params['role-playing-game-session-id']);
  }

  /**
   * Creates an instance of RolePlayingGameGuard.
   * @param {RolePlayingGameService} RolePlayingGameService
   * @memberof RolePlayingGameGuard
   */
  constructor(protected readonly RolePlayingGameService: RolePlayingGameService) {}
}
