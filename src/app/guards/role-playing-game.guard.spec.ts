import { AuthService } from '../services/auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MockAuthService } from 'spec/MockAuthService';
import { MockModule } from 'ng-mocks';
import { OAuthModule } from 'angular-oauth2-oidc';
import { RolePlayingGameGuard } from './role-playing-game.guard';
import { RouterTestingModule } from '@angular/router/testing';
import { TestBed } from '@angular/core/testing';

describe('RolePlayingGameGuard', () => {
  let guard: RolePlayingGameGuard;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        MockModule(OAuthModule.forRoot()),
      ],
      providers: [{
        provide: AuthService,
        useValue: new MockAuthService()
      }]
    });
    guard = TestBed.inject(RolePlayingGameGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
