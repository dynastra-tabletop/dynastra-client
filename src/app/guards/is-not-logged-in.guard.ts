import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class IsNotLoggedInGuard implements CanActivate {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(!this.AuthService.isLoggedIn) {
      return true;
    }

    return this.router.parseUrl("/");
  }

  /**
   * Creates an instance of IsLoggedInGuard.
   * @param {AuthService} AuthService
   * @memberof IsLoggedInGuard
   */
  constructor(protected readonly AuthService: AuthService, protected readonly router: Router) {}
}
