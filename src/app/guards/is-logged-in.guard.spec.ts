import { TestBed } from '@angular/core/testing';

import { IsLoggedInGuard } from './is-logged-in.guard';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientModule} from "@angular/common/http";
import {OAuthModule} from "angular-oauth2-oidc";
import {AuthService} from "../services/auth.service";
import {ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";

describe('IsLoggedInGuard', () => {
  let guard: IsLoggedInGuard;
  let mockAuthService: { isLoggedIn: boolean } = {
    isLoggedIn: false,
  };

  beforeEach(() => {
    TestBed.overrideProvider(AuthService, {
      useValue: mockAuthService,
    });
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        OAuthModule.forRoot(),
        HttpClientModule,
      ]
    });
    guard = TestBed.inject(IsLoggedInGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
  it('should correctly guard the URL if the user is not logged in', () => {
    expect(() => guard.canActivate(
      {} as unknown as ActivatedRouteSnapshot,
      {} as unknown as RouterStateSnapshot,
    )).not.toThrow();

    mockAuthService.isLoggedIn = true;

    expect(() => guard.canActivate(
      {} as unknown as ActivatedRouteSnapshot,
      {} as unknown as RouterStateSnapshot,
    )).not.toThrow();
  })
});
