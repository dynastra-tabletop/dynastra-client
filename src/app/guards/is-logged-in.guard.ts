import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class IsLoggedInGuard implements CanActivate {
  /**
   * Prevents a user from logging in if they have no auth.
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @return {*}  {(Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree)}
   * @memberof IsLoggedInGuard
   */
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(this.AuthService.isLoggedIn) {
      return true;
    }

    return this.router.parseUrl("/login");
  }

  /**
   * Creates an instance of IsLoggedInGuard.
   * @param {AuthService} AuthService
   * @memberof IsLoggedInGuard
   */
  constructor(protected readonly AuthService: AuthService, protected readonly router: Router) {}
}
