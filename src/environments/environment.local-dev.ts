export const environment = {
    production: false,
    oidc_discovery: "http://localhost:8080/auth/realms/Dynastra/.well-known/openid-configuration",
    oidc_baseURL: "http://localhost:4200",
    oidc_issuer: "http://localhost:8080/auth/realms/Dynastra",
    oidc_clientID: "Dynastra",
    api_prefix: "https://localhost:5001",
    socket_url: 'https://localhost:5001/dynastra-game-session',
    asset_prefix: 'https://localhost:5001/role-playing-games/assets/',
    asset_upload: 'https://localhost:5001/role-playing-games/asset',
    asset_upload_allowed_extensions: 'jpg,png,gif,webp'
};
