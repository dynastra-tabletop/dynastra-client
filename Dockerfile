FROM ubuntu:18.04

## Update ubuntu's packages
RUN apt upgrade && apt update

## 1 - KEYCLOAK
RUN cd / && cd /opt
RUN apt install wget -y
RUN wget https://github.com/keycloak/keycloak/releases/download/17.0.1/keycloak-legacy-17.0.1.tar.gz
RUN tar -xvf keycloak-legacy-17.0.1.tar.gz && mv keycloak-17.0.1 /opt/keycloak
RUN groupadd keycloak && useradd -r -g keycloak -d /opt/keycloak -s /sbin/nologin keycloak
RUN apt install default-jdk -y

## Add user for keycloak in default master realm
RUN TEMP_DYNASTRA_PASS={echo $RANDOM | md5sum | head -c 24; echo;}
RUN ./opt/keycloak/bin/add-user-keycloak.sh -r master -u dynastra -p $TEMP_DYNASTRA_PASS
ENTRYPOINT ["echo", "$TEMP_DYNASTRA_PASS"]

## 2 - RETHINK
RUN apt install gpg -y
RUN wget -qO- https://download.rethinkdb.com/repository/raw/pubkey.gpg | gpg --dearmor -o /usr/share/keyrings/rethinkdb-archive-keyrings.gpg

# Add the repository.
RUN echo "deb [signed-by=/usr/share/keyrings/rethinkdb-archive-keyrings.gpg] https://download.rethinkdb.com/repository/ubuntu-bionic bionic main" | tee /etc/apt/sources.list.d/rethinkdb.list

# Install rethink
RUN apt-get update
RUN apt-get install rethinkdb -y

## 3 - SYNAPSE
RUN apt install software-properties-common -y
RUN wget -qO - https://matrix.org/packages/debian/repo-key.asc | apt-key add -
RUN add-apt-repository https://matrix.org/packages/debian/
RUN apt install matrix-synapse -y
