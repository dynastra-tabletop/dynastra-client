import {IAuthService} from "../src/app/services/auth.service";
import {ILoggedInUser} from "../src/lib/user-login.interfaces";

export class MockAuthService implements IAuthService {
  public isAwaitingAuthentication: boolean = false;
  public failedToContactAuthentication: boolean = false;
  public profile?: any = {};
  public isLoggedIn: boolean = true;
  public getIssuerAccountRedirect(): string {
    return "#";
  }
  public loggedInUser: Partial<ILoggedInUser> = {
    username: "TestUserNameWhichIsLong",
    email: "someverylongemail@dynastra.vtt",
  };
  public loggedInUserContext: any = {};
  initAuth(initialURL?: string): Promise<void> {
    return Promise.resolve();
  }
  logout(): void {
    return;
  }
}

export class UninitialisedAuthService implements IAuthService {
  public isAwaitingAuthentication: boolean = true;
  public failedToContactAuthentication: boolean = false;
  public profile?: any = {};
  public isLoggedIn: boolean = false;
  public getIssuerAccountRedirect(): string {
    return "#";
  }
  public loggedInUser: Partial<ILoggedInUser> = {};
  public loggedInUserContext: any = {};
  initAuth(initialURL?: string): Promise<void> {
    return Promise.resolve();
  }
  logout(): void {
    return;
  }
}
