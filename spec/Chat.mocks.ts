const MOCK_FULL_CHAT_UPDATE: string = `
{
	"next_batch": "s406_7402_0_48_241_1_2_675_1",
	"account_data": {
		"events": [{
			"type": "im.vector.analytics",
			"content": {
				"pseudonymousAnalyticsOptIn": false
			}
		}, {
			"type": "im.vector.setting.breadcrumbs",
			"content": {
				"recent_rooms": ["!PLMUMQcazMKMyfTmoZ:localhost", "!NtfWdLXotblItexeqj:localhost", "!NpmoEguBBfReyhGRtq:localhost"]
			}
		}, {
			"type": "io.element.recent_emoji",
			"content": {
				"recent_emoji": [
					["\u263a\ufe0f", 2],
					["\ud83e\udd2a", 1],
					["\ud83d\ude06", 2],
					["\ud83d\udca6", 3],
					["\ud83d\ude17", 1],
					["\ud83d\udeb3", 1],
					["\ud83d\ude1c", 4],
					["\ud83d\ude09", 1],
					["\ud83e\udee2", 1],
					["\ud83d\ude1d", 1],
					["\ud83d\ude42", 1],
					["\ud83d\ude10\ufe0f", 1],
					["\ud83e\udd23", 2],
					["\ud83d\ude03", 1],
					["\ud83d\ude35\u200d\ud83d\udcab", 1],
					["\ud83d\ude1b", 1],
					["\ud83e\udee5", 1],
					["\ud83d\ude2c", 2],
					["\ud83e\udd72", 1],
					["\ud83d\ude00", 3],
					["\ud83e\udd2d", 1],
					["\ud83d\ude2a", 1]
				]
			}
		}, {
			"type": "m.push_rules",
			"content": {
				"global": {
					"underride": [{
						"conditions": [{
							"kind": "event_match",
							"key": "type",
							"pattern": "m.call.invite"
						}],
						"actions": ["notify", {
							"set_tweak": "sound",
							"value": "ring"
						}, {
							"set_tweak": "highlight",
							"value": false
						}],
						"rule_id": ".m.rule.call",
						"default": true,
						"enabled": true
					}, {
						"conditions": [{
							"kind": "room_member_count",
							"is": "2"
						}, {
							"kind": "event_match",
							"key": "type",
							"pattern": "m.room.message"
						}],
						"actions": ["notify", {
							"set_tweak": "sound",
							"value": "default"
						}, {
							"set_tweak": "highlight",
							"value": false
						}],
						"rule_id": ".m.rule.room_one_to_one",
						"default": true,
						"enabled": true
					}, {
						"conditions": [{
							"kind": "room_member_count",
							"is": "2"
						}, {
							"kind": "event_match",
							"key": "type",
							"pattern": "m.room.encrypted"
						}],
						"actions": ["notify", {
							"set_tweak": "sound",
							"value": "default"
						}, {
							"set_tweak": "highlight",
							"value": false
						}],
						"rule_id": ".m.rule.encrypted_room_one_to_one",
						"default": true,
						"enabled": true
					}, {
						"conditions": [{
							"kind": "event_match",
							"key": "type",
							"pattern": "m.room.message"
						}],
						"actions": ["notify", {
							"set_tweak": "highlight",
							"value": false
						}],
						"rule_id": ".m.rule.message",
						"default": true,
						"enabled": true
					}, {
						"conditions": [{
							"kind": "event_match",
							"key": "type",
							"pattern": "m.room.encrypted"
						}],
						"actions": ["notify", {
							"set_tweak": "highlight",
							"value": false
						}],
						"rule_id": ".m.rule.encrypted",
						"default": true,
						"enabled": true
					}, {
						"conditions": [{
							"kind": "event_match",
							"key": "type",
							"pattern": "im.vector.modular.widgets"
						}, {
							"kind": "event_match",
							"key": "content.type",
							"pattern": "jitsi"
						}, {
							"kind": "event_match",
							"key": "state_key",
							"pattern": "*"
						}],
						"actions": ["notify", {
							"set_tweak": "highlight",
							"value": false
						}],
						"rule_id": ".im.vector.jitsi",
						"default": true,
						"enabled": true
					}],
					"sender": [],
					"room": [],
					"content": [{
						"actions": ["notify", {
							"set_tweak": "sound",
							"value": "default"
						}, {
							"set_tweak": "highlight"
						}],
						"pattern": "dynastra_service",
						"rule_id": ".m.rule.contains_user_name",
						"default": true,
						"enabled": true
					}],
					"override": [{
						"conditions": [],
						"actions": ["dont_notify"],
						"rule_id": ".m.rule.master",
						"default": true,
						"enabled": false
					}, {
						"conditions": [{
							"kind": "event_match",
							"key": "content.msgtype",
							"pattern": "m.notice"
						}],
						"actions": ["dont_notify"],
						"rule_id": ".m.rule.suppress_notices",
						"default": true,
						"enabled": true
					}, {
						"conditions": [{
							"kind": "event_match",
							"key": "type",
							"pattern": "m.room.member"
						}, {
							"kind": "event_match",
							"key": "content.membership",
							"pattern": "invite"
						}, {
							"kind": "event_match",
							"key": "state_key",
							"pattern": "@dynastra_service:localhost"
						}],
						"actions": ["notify", {
							"set_tweak": "sound",
							"value": "default"
						}, {
							"set_tweak": "highlight",
							"value": false
						}],
						"rule_id": ".m.rule.invite_for_me",
						"default": true,
						"enabled": true
					}, {
						"conditions": [{
							"kind": "event_match",
							"key": "type",
							"pattern": "m.room.member"
						}],
						"actions": ["dont_notify"],
						"rule_id": ".m.rule.member_event",
						"default": true,
						"enabled": true
					}, {
						"conditions": [{
							"kind": "contains_display_name"
						}],
						"actions": ["notify", {
							"set_tweak": "sound",
							"value": "default"
						}, {
							"set_tweak": "highlight"
						}],
						"rule_id": ".m.rule.contains_display_name",
						"default": true,
						"enabled": true
					}, {
						"conditions": [{
							"kind": "event_match",
							"key": "content.body",
							"pattern": "@room"
						}, {
							"kind": "sender_notification_permission",
							"key": "room"
						}],
						"actions": ["notify", {
							"set_tweak": "highlight",
							"value": true
						}],
						"rule_id": ".m.rule.roomnotif",
						"default": true,
						"enabled": true
					}, {
						"conditions": [{
							"kind": "event_match",
							"key": "type",
							"pattern": "m.room.tombstone"
						}, {
							"kind": "event_match",
							"key": "state_key",
							"pattern": ""
						}],
						"actions": ["notify", {
							"set_tweak": "highlight",
							"value": true
						}],
						"rule_id": ".m.rule.tombstone",
						"default": true,
						"enabled": true
					}, {
						"conditions": [{
							"kind": "event_match",
							"key": "type",
							"pattern": "m.reaction"
						}],
						"actions": ["dont_notify"],
						"rule_id": ".m.rule.reaction",
						"default": true,
						"enabled": true
					}]
				},
				"device": {}
			}
		}]
	},
	"presence": {
		"events": [{
			"type": "m.presence",
			"sender": "@dynastra_service:localhost",
			"content": {
				"presence": "online",
				"last_active_ago": 47,
				"currently_active": true
			}
		}]
	},
	"device_one_time_keys_count": {
		"signed_curve25519": 0
	},
	"org.matrix.msc2732.device_unused_fallback_key_types": [],
	"device_unused_fallback_key_types": [],
	"rooms": {
		"join": {
			"!NpmoEguBBfReyhGRtq:localhost": {
				"timeline": {
					"events": [{
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"msgtype": "m.text",
							"body": "\ud83d\ude18 Testing"
						},
						"event_id": "$16502937221tjpDO:localhost",
						"origin_server_ts": 1650293722921,
						"unsigned": {
							"age": 517754929
						}
					}, {
						"type": "m.room.redaction",
						"sender": "@dynastra_service:localhost",
						"content": {},
						"redacts": "$165011388019LHQUX:localhost",
						"event_id": "$16502955232ulMrb:localhost",
						"origin_server_ts": 1650295523131,
						"unsigned": {
							"age": 515954719
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"org.matrix.msc1767.text": "\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c",
							"body": "\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c",
							"msgtype": "m.text"
						},
						"event_id": "$16502966053vFZOB:localhost",
						"origin_server_ts": 1650296605787,
						"unsigned": {
							"age": 514872063
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"org.matrix.msc1767.text": "\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c",
							"body": "\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c\ud83d\ude1c",
							"msgtype": "m.text"
						},
						"event_id": "$16502973824edUiO:localhost",
						"origin_server_ts": 1650297382849,
						"unsigned": {
							"age": 514095001
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"org.matrix.msc1767.text": "\ud83d\udeb3",
							"body": "\ud83d\udeb3",
							"msgtype": "m.text"
						},
						"event_id": "$16502983065XKClb:localhost",
						"origin_server_ts": 1650298306358,
						"unsigned": {
							"age": 513171492
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"msgtype": "m.text",
							"body": "I am talking"
						},
						"event_id": "$16503001086kgZUm:localhost",
						"origin_server_ts": 1650300108096,
						"unsigned": {
							"age": 511369754
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"msgtype": "m.text",
							"body": "\u2694\ufe0f"
						},
						"event_id": "$16503004577dGTfj:localhost",
						"origin_server_ts": 1650300457962,
						"unsigned": {
							"age": 511019888,
							"m.relations": {
								"m.annotation": {
									"chunk": [{
										"type": "m.reaction",
										"key": "\ud83d\udca6",
										"count": 1
									}]
								}
							}
						}
					}, {
						"type": "m.reaction",
						"sender": "@dynastra_service:localhost",
						"content": {},
						"event_id": "$16503005878ZHzvk:localhost",
						"origin_server_ts": 1650300587272,
						"unsigned": {
							"redacted_by": "$16503005979uEoek:localhost",
							"redacted_because": {
								"type": "m.room.redaction",
								"sender": "@dynastra_service:localhost",
								"content": {},
								"redacts": "$16503005878ZHzvk:localhost",
								"event_id": "$16503005979uEoek:localhost",
								"origin_server_ts": 1650300597043,
								"unsigned": {
									"age": 510880807
								}
							},
							"age": 510890578
						}
					}, {
						"type": "m.room.redaction",
						"sender": "@dynastra_service:localhost",
						"content": {},
						"redacts": "$16503005878ZHzvk:localhost",
						"event_id": "$16503005979uEoek:localhost",
						"origin_server_ts": 1650300597043,
						"unsigned": {
							"age": 510880807
						}
					}, {
						"type": "m.reaction",
						"sender": "@dynastra_service:localhost",
						"content": {
							"m.relates_to": {
								"rel_type": "m.annotation",
								"event_id": "$16503004577dGTfj:localhost",
								"key": "\ud83d\udca6"
							}
						},
						"event_id": "$165030060510iUjxa:localhost",
						"origin_server_ts": 1650300605103,
						"unsigned": {
							"age": 510872747
						}
					}],
					"prev_batch": "t38-349_7402_0_48_241_1_2_675_1",
					"limited": true
				},
				"state": {
					"events": [{
						"type": "m.room.power_levels",
						"sender": "@dynastra_service:localhost",
						"content": {
							"users": {
								"@dynastra_service:localhost": 100
							},
							"users_default": 0,
							"events": {
								"m.room.name": 50,
								"m.room.power_levels": 100,
								"m.room.history_visibility": 100,
								"m.room.canonical_alias": 50,
								"m.room.avatar": 50,
								"m.room.tombstone": 100,
								"m.room.server_acl": 100,
								"m.room.encryption": 100
							},
							"events_default": 0,
							"state_default": 50,
							"ban": 50,
							"kick": 50,
							"redact": 50,
							"invite": 0,
							"historical": 100
						},
						"state_key": "",
						"event_id": "$164988813413NDMEu:localhost",
						"origin_server_ts": 1649888134711,
						"unsigned": {
							"age": 923343139
						}
					}, {
						"type": "m.room.guest_access",
						"sender": "@dynastra_service:localhost",
						"content": {
							"guest_access": "can_join"
						},
						"state_key": "",
						"event_id": "$164988813517omVXZ:localhost",
						"origin_server_ts": 1649888135192,
						"unsigned": {
							"age": 923342658
						}
					}, {
						"type": "m.room.name",
						"sender": "@dynastra_service:localhost",
						"content": {
							"name": "Dynastra_be5ed214-51a8-4cbf-a697-58f5038e2aa3"
						},
						"state_key": "",
						"event_id": "$164988813518TNXbe:localhost",
						"origin_server_ts": 1649888135289,
						"unsigned": {
							"age": 923342561
						}
					}, {
						"type": "m.room.create",
						"sender": "@dynastra_service:localhost",
						"content": {
							"room_version": "1",
							"creator": "@dynastra_service:localhost"
						},
						"state_key": "",
						"event_id": "$164988813411aKHKv:localhost",
						"origin_server_ts": 1649888134453,
						"unsigned": {
							"age": 923343397
						}
					}, {
						"type": "m.room.canonical_alias",
						"sender": "@dynastra_service:localhost",
						"content": {
							"alias": "#be5ed214-51a8-4cbf-a697-58f5038e2aa3:localhost"
						},
						"state_key": "",
						"event_id": "$164988813414wkrMu:localhost",
						"origin_server_ts": 1649888134858,
						"unsigned": {
							"age": 923342992
						}
					}, {
						"type": "m.room.member",
						"sender": "@dynastra_service:localhost",
						"content": {
							"membership": "join",
							"displayname": "dynastra_service"
						},
						"state_key": "@dynastra_service:localhost",
						"event_id": "$164988813412advQE:localhost",
						"origin_server_ts": 1649888134584,
						"unsigned": {
							"age": 923343266
						}
					}, {
						"type": "m.room.join_rules",
						"sender": "@dynastra_service:localhost",
						"content": {
							"join_rule": "invite"
						},
						"state_key": "",
						"event_id": "$164988813415Yffux:localhost",
						"origin_server_ts": 1649888134982,
						"unsigned": {
							"age": 923342868
						}
					}, {
						"type": "m.room.topic",
						"sender": "@dynastra_service:localhost",
						"content": {
							"topic": "This room was automatically created by Dynastra Tabletop"
						},
						"state_key": "",
						"event_id": "$164988813519elfNe:localhost",
						"origin_server_ts": 1649888135388,
						"unsigned": {
							"age": 923342462
						}
					}, {
						"type": "m.room.history_visibility",
						"sender": "@dynastra_service:localhost",
						"content": {
							"history_visibility": "shared"
						},
						"state_key": "",
						"event_id": "$164988813516rkVJj:localhost",
						"origin_server_ts": 1649888135096,
						"unsigned": {
							"age": 923342754
						}
					}]
				},
				"account_data": {
					"events": [{
						"type": "m.fully_read",
						"content": {
							"event_id": "$165030060510iUjxa:localhost"
						}
					}]
				},
				"ephemeral": {
					"events": []
				},
				"unread_notifications": {
					"notification_count": 0,
					"highlight_count": 0
				},
				"summary": {},
				"org.matrix.msc2654.unread_count": 0
			},
			"!NtfWdLXotblItexeqj:localhost": {
				"timeline": {
					"events": [{
						"type": "m.room.member",
						"sender": "@dynastra_service:localhost",
						"content": {
							"membership": "join",
							"displayname": "dynastra_service"
						},
						"state_key": "@dynastra_service:localhost",
						"event_id": "$165030092512qLkdl:localhost",
						"origin_server_ts": 1650300925965,
						"unsigned": {
							"age": 510551885
						}
					}, {
						"type": "m.room.power_levels",
						"sender": "@dynastra_service:localhost",
						"content": {
							"users": {
								"@dynastra_service:localhost": 100
							},
							"users_default": 0,
							"events": {
								"m.room.name": 50,
								"m.room.power_levels": 100,
								"m.room.history_visibility": 100,
								"m.room.canonical_alias": 50,
								"m.room.avatar": 50,
								"m.room.tombstone": 100,
								"m.room.server_acl": 100,
								"m.room.encryption": 100
							},
							"events_default": 0,
							"state_default": 50,
							"ban": 50,
							"kick": 50,
							"redact": 50,
							"invite": 0,
							"historical": 100
						},
						"state_key": "",
						"event_id": "$165030092613hwUAx:localhost",
						"origin_server_ts": 1650300926067,
						"unsigned": {
							"age": 510551783
						}
					}, {
						"type": "m.room.canonical_alias",
						"sender": "@dynastra_service:localhost",
						"content": {
							"alias": "#85d1b2f8-fdd1-47ae-b08a-e5bdb77584ad:localhost"
						},
						"state_key": "",
						"event_id": "$165030092614VznlW:localhost",
						"origin_server_ts": 1650300926230,
						"unsigned": {
							"age": 510551620
						}
					}, {
						"type": "m.room.join_rules",
						"sender": "@dynastra_service:localhost",
						"content": {
							"join_rule": "invite"
						},
						"state_key": "",
						"event_id": "$165030092615wayuw:localhost",
						"origin_server_ts": 1650300926338,
						"unsigned": {
							"age": 510551512
						}
					}, {
						"type": "m.room.history_visibility",
						"sender": "@dynastra_service:localhost",
						"content": {
							"history_visibility": "shared"
						},
						"state_key": "",
						"event_id": "$165030092616dRQbl:localhost",
						"origin_server_ts": 1650300926456,
						"unsigned": {
							"age": 510551394
						}
					}, {
						"type": "m.room.guest_access",
						"sender": "@dynastra_service:localhost",
						"content": {
							"guest_access": "can_join"
						},
						"state_key": "",
						"event_id": "$165030092617Ttmpb:localhost",
						"origin_server_ts": 1650300926600,
						"unsigned": {
							"age": 510551250
						}
					}, {
						"type": "m.room.name",
						"sender": "@dynastra_service:localhost",
						"content": {
							"name": "Dynastra_85d1b2f8-fdd1-47ae-b08a-e5bdb77584ad"
						},
						"state_key": "",
						"event_id": "$165030092618xMqCE:localhost",
						"origin_server_ts": 1650300926697,
						"unsigned": {
							"age": 510551153
						}
					}, {
						"type": "m.room.topic",
						"sender": "@dynastra_service:localhost",
						"content": {
							"topic": "This room was automatically created by Dynastra Tabletop"
						},
						"state_key": "",
						"event_id": "$165030092619jyJJM:localhost",
						"origin_server_ts": 1650300926799,
						"unsigned": {
							"age": 510551051
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"msgtype": "m.text",
							"body": "A big fat turd"
						},
						"event_id": "$165030097221tmQiq:localhost",
						"origin_server_ts": 1650300972353,
						"unsigned": {
							"age": 510505497
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"msgtype": "m.text",
							"body": "\ud83d\ude0f"
						},
						"event_id": "$165030099722zkOtg:localhost",
						"origin_server_ts": 1650300997651,
						"unsigned": {
							"age": 510480199
						}
					}],
					"prev_batch": "t2-360_7402_0_48_241_1_2_675_1",
					"limited": true
				},
				"state": {
					"events": [{
						"type": "m.room.create",
						"sender": "@dynastra_service:localhost",
						"content": {
							"room_version": "1",
							"creator": "@dynastra_service:localhost"
						},
						"state_key": "",
						"event_id": "$165030092511DHjPV:localhost",
						"origin_server_ts": 1650300925795,
						"unsigned": {
							"age": 510552055
						}
					}]
				},
				"account_data": {
					"events": [{
						"type": "m.fully_read",
						"content": {
							"event_id": "$165030099722zkOtg:localhost"
						}
					}]
				},
				"ephemeral": {
					"events": []
				},
				"unread_notifications": {
					"notification_count": 0,
					"highlight_count": 0
				},
				"summary": {},
				"org.matrix.msc2654.unread_count": 0
			},
			"!PLMUMQcazMKMyfTmoZ:localhost": {
				"timeline": {
					"events": [{
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"org.matrix.msc1767.text": "lol",
							"body": "lol",
							"msgtype": "m.text"
						},
						"event_id": "$165030435450xnEDX:localhost",
						"origin_server_ts": 1650304354232,
						"unsigned": {
							"age": 507123618
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"org.matrix.msc1767.text": "aw unread again",
							"body": "aw unread again",
							"msgtype": "m.text"
						},
						"event_id": "$165030444051Heudq:localhost",
						"origin_server_ts": 1650304440711,
						"unsigned": {
							"age": 507037139
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"org.matrix.msc1767.text": "Un read my friend",
							"body": "Un read my friend",
							"msgtype": "m.text"
						},
						"event_id": "$165030452552UOwKs:localhost",
						"origin_server_ts": 1650304525984,
						"unsigned": {
							"age": 506951866
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"org.matrix.msc1767.text": "Another unread message",
							"body": "Another unread message",
							"msgtype": "m.text"
						},
						"event_id": "$165030456153TwfYw:localhost",
						"origin_server_ts": 1650304561629,
						"unsigned": {
							"age": 506916221
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"org.matrix.msc1767.text": "Nuevo",
							"body": "Nuevo",
							"msgtype": "m.text"
						},
						"event_id": "$165030473054mImhE:localhost",
						"origin_server_ts": 1650304730823,
						"unsigned": {
							"age": 506747027
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"msgtype": "m.text",
							"body": "\ud83d\ude05"
						},
						"event_id": "$165030479655gmwAF:localhost",
						"origin_server_ts": 1650304796231,
						"unsigned": {
							"age": 506681619,
							"m.relations": {
								"m.annotation": {
									"chunk": [{
										"type": "m.reaction",
										"key": "\u263a\ufe0f",
										"count": 1
									}]
								}
							}
						}
					}, {
						"type": "m.reaction",
						"sender": "@dynastra_service:localhost",
						"content": {
							"m.relates_to": {
								"rel_type": "m.annotation",
								"event_id": "$165030479655gmwAF:localhost",
								"key": "\u263a\ufe0f"
							}
						},
						"event_id": "$165030496756ZfjdJ:localhost",
						"origin_server_ts": 1650304967379,
						"unsigned": {
							"age": 506510471
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"msgtype": "m.text",
							"body": "test\ud83d\udc15\u200d\ud83e\uddba\ud83e\udde3\ud83e\udde3\ud83e\udde3\ud83e\udde3\ud83e\udde3\ud83e\udde3\ud83e\udde3\ud83e\udde3\ud83e\udde3\ud83e\udde3"
						},
						"event_id": "$165030498157dyRQt:localhost",
						"origin_server_ts": 1650304981638,
						"unsigned": {
							"age": 506496212
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"org.matrix.msc1767.text": "Oh lordy a dawg",
							"body": "Oh lordy a dawg",
							"msgtype": "m.text"
						},
						"event_id": "$165030527258DzUag:localhost",
						"origin_server_ts": 1650305272782,
						"unsigned": {
							"age": 506205068
						}
					}, {
						"type": "m.room.message",
						"sender": "@dynastra_service:localhost",
						"content": {
							"msgtype": "m.text",
							"body": "Yup lol"
						},
						"event_id": "$165030528659TEjiB:localhost",
						"origin_server_ts": 1650305286108,
						"unsigned": {
							"age": 506191742
						}
					}],
					"prev_batch": "t27-396_7402_0_48_241_1_2_675_1",
					"limited": true
				},
				"state": {
					"events": [{
						"type": "m.room.create",
						"sender": "@dynastra_service:localhost",
						"content": {
							"room_version": "1",
							"creator": "@dynastra_service:localhost"
						},
						"state_key": "",
						"event_id": "$165030177223iDFET:localhost",
						"origin_server_ts": 1650301772601,
						"unsigned": {
							"age": 509705249
						}
					}, {
						"type": "m.room.history_visibility",
						"sender": "@dynastra_service:localhost",
						"content": {
							"history_visibility": "shared"
						},
						"state_key": "",
						"event_id": "$165030177328zBxVP:localhost",
						"origin_server_ts": 1650301773331,
						"unsigned": {
							"age": 509704519
						}
					}, {
						"type": "m.room.topic",
						"sender": "@dynastra_service:localhost",
						"content": {
							"topic": "This room was automatically created by Dynastra Tabletop"
						},
						"state_key": "",
						"event_id": "$165030177331rSmUk:localhost",
						"origin_server_ts": 1650301773604,
						"unsigned": {
							"age": 509704246
						}
					}, {
						"type": "m.room.join_rules",
						"sender": "@dynastra_service:localhost",
						"content": {
							"join_rule": "invite"
						},
						"state_key": "",
						"event_id": "$165030177327Ljksu:localhost",
						"origin_server_ts": 1650301773236,
						"unsigned": {
							"age": 509704614
						}
					}, {
						"type": "m.room.canonical_alias",
						"sender": "@dynastra_service:localhost",
						"content": {
							"alias": "#3ef1fd98-8279-4578-bffa-06b0feb87ee6:localhost"
						},
						"state_key": "",
						"event_id": "$165030177326RBmgg:localhost",
						"origin_server_ts": 1650301773104,
						"unsigned": {
							"age": 509704746
						}
					}, {
						"type": "m.room.power_levels",
						"sender": "@dynastra_service:localhost",
						"content": {
							"users": {
								"@dynastra_service:localhost": 100
							},
							"users_default": 0,
							"events": {
								"m.room.name": 50,
								"m.room.power_levels": 100,
								"m.room.history_visibility": 100,
								"m.room.canonical_alias": 50,
								"m.room.avatar": 50,
								"m.room.tombstone": 100,
								"m.room.server_acl": 100,
								"m.room.encryption": 100
							},
							"events_default": 0,
							"state_default": 50,
							"ban": 50,
							"kick": 50,
							"redact": 50,
							"invite": 0,
							"historical": 100
						},
						"state_key": "",
						"event_id": "$165030177225mqnaL:localhost",
						"origin_server_ts": 1650301772951,
						"unsigned": {
							"age": 509704899
						}
					}, {
						"type": "m.room.member",
						"sender": "@dynastra_service:localhost",
						"content": {
							"membership": "join",
							"displayname": "dynastra_service"
						},
						"state_key": "@dynastra_service:localhost",
						"event_id": "$165030177224LNiBR:localhost",
						"origin_server_ts": 1650301772783,
						"unsigned": {
							"age": 509705067
						}
					}, {
						"type": "m.room.guest_access",
						"sender": "@dynastra_service:localhost",
						"content": {
							"guest_access": "can_join"
						},
						"state_key": "",
						"event_id": "$165030177329vRmwm:localhost",
						"origin_server_ts": 1650301773430,
						"unsigned": {
							"age": 509704420
						}
					}, {
						"type": "m.room.name",
						"sender": "@dynastra_service:localhost",
						"content": {
							"name": "Dynastra_3ef1fd98-8279-4578-bffa-06b0feb87ee6"
						},
						"state_key": "",
						"event_id": "$165030177330pUZPn:localhost",
						"origin_server_ts": 1650301773515,
						"unsigned": {
							"age": 509704335
						}
					}]
				},
				"account_data": {
					"events": [{
						"type": "m.fully_read",
						"content": {
							"event_id": "$165030528659TEjiB:localhost"
						}
					}]
				},
				"ephemeral": {
					"events": []
				},
				"unread_notifications": {
					"notification_count": 0,
					"highlight_count": 0
				},
				"summary": {},
				"org.matrix.msc2654.unread_count": 0
			}
		}
	}
}`;

const EMPTY_SYNC_RESPONSE: string = `{
	"next_batch": "s406_7402_0_48_241_1_2_675_1",
	"device_one_time_keys_count": {
		"signed_curve25519": 0
	},
	"org.matrix.msc2732.device_unused_fallback_key_types": [],
	"device_unused_fallback_key_types": []
}`;

export const FULL_SYNC_RESPONSE_OBJECT: any = JSON.parse(MOCK_FULL_CHAT_UPDATE);
export const EMPTY_SYNC_RESPONSE_OBJECT: any = JSON.parse(EMPTY_SYNC_RESPONSE);
